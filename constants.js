'use strict';

var constants = {};

constants.ROOMS = { parent1: 'room1', parent2: 'room2', driver1: 'room1', driver2: 'room2', admin: ['room1', 'room2'] };
constants.MONGO_URL = 'mongodb://localhost:27017/transportmgmt';
constants.PROFILE_IMAGE_PATH = '/otp/uploads/profile/';
constants.DEVICE_IMAGE_PATH = '/otp/uploads/device/';
constants.DAILY_CHECKLIST = 'Daily Checklist';
constants.INCIDENT_REPORT = 'Incident Report';
constants.LOST_FOUND = 'Lost and Found';
constants.ON_THE_JOB_TRAINING = 'On The Job Training';
constants.OPERATION_VISIT_REPORT = 'Operation Visit';
constants.REFRESHER_TRAINING = 'Refresher Training';
constants.SITE_VISIT = 'Site Visit';
constants.SHIFT_SUPERVISOR = 'Shift Supervisor';
constants.JOB_APPRAISAL = 'Job Appraisal';
constants.PERFORMANCE_EVAL = 'Performance Eval';
constants.FIRE_DRILL = 'Fire Drill';
constants.FIRE_INCIDENT = 'Fire Incident';
constants.LOGGER = 'Logger';
constants.OCCURRENCE_BOOK = 'Occurrence Book';
constants.LOCATION_NAME = 'Location Name';
constants.SITE_NAME = 'Site Name';
constants.DEPLOYMENT = 'Deployment';
constants.OFFICER_FEEDBACK = 'Officer Feedback';
constants.REVIEW_ACTION = 'After Action Review';
constants.CLOCKING = 'Clocking';
constants.USER = 'User';
constants.FCM_TOKEN = 'FCM Token';
constants.NOTIFICATION = 'Notification';
constants.CLIENT_SATISFACTION = 'Client Satisfaction';
constants.NOT_WORKING = 'NOT WORKING';
constants.START_WORK = 'START WORK';
constants.END_WORK = 'END WORK';
constants.WORKING = 'WORKING';
constants.BREAK = 'BREAK';
constants.QR_CODE = 'QR Code';
constants.E_LEARNING = 'e-Learning';
constants.E_LEARNING_VIEW = 'e-Learning View';
constants.E_LEARNING_RESPONSE = 'e-Learning Response';
constants.E_LEARNING_RESULT = 'e-Learning Result';
constants.E_LEARNING_FAILED = 'e-Learning Failed';

constants.COLLECTION_CLOCKING = 'Clocking';
constants.VACATION = 'Vacation';
constants.VACATION_REQUEST = 'Vacation Request';
constants.VACATION_BALANCE = 'Vacation Balance';
constants.OVER_TIME = 'OverTime';

constants.FOLDER_ELEARNING = 'e-learning';
constants.FOLDER_SITES = 'sites';
constants.FOLDER_USERS = 'users';
constants.FOLDER_CLIENT_SATISFACTION = 'client-satisfaction';

constants.DESIGNATION = 'Designation';
constants.LEAVE_TYPE = 'LeaveType';
constants.SITE = 'Site';
constants.MENTOR = 'Mentor';
constants.TRAINEE = 'Trainee';
constants.COMPLETE = 'COMPLETE';
constants.TRAINING = 'Training';
constants.ATTENDANCE = 'Attendance';
constants.SALARY_STRUCTURE = 'Salary Structure';
constants.GRADE = 'Grade';
constants.PAYSLIP = 'PaySlip';
constants.HOLIDAY = 'Holiday';

constants.FEEDBACK = 'Feedback';
constants.MANAGEMENT_FEEDBACK = 'Management Feedback';
constants.MANAGEMENT_WARNING = 'Management Warning';

constants.MULTIPART =  {
    maxBytes: 15 * 1024 * 1024, // 15 mb
    output: 'stream',
    allow: 'multipart/form-data', // important
    parse: true
}

module.exports = Object.freeze(constants);