'use strict';

const Deployment = require('../model/Deployment');
const Boom = require('boom');

async function saveDeployment(deploymentObj) {

  var result;

  await Deployment.create(deploymentObj).then((savedObj) => {
    result = savedObj;
  }).catch((err) => {
    console.log('error occurred while saving deployment.. ' + err);
    return Boom.badImplementation('Error', err);
  });

  return result;
}

async function getDeploymentBySiteName(siteName) {

  try {

    var deploymentObj = await Deployment.find({
      siteName: siteName
    });

    return deploymentObj;

  } catch (error) {

    console.log('error occurred during getDeploymentBySiteName ' + error);
  }
}

async function getDeploymentByDate(dateCreated, siteName) {
  try {

    let deploymentObj = await Deployment.findOne({
      siteName: siteName, dateCreated: dateCreated
    });

    return deploymentObj;
  } catch (error) {

    console.log('error occurred during getDeployment ' + error);
    return null;
  }
}

async function getDeployment(deploymentId) {

  try {

    let deploymentObj = await Deployment.findOne({
      _id: deploymentId
    });

    return deploymentObj;
  } catch (error) {

    console.log('error occurred during getDeployment ' + error);
    return null;
  }
}

async function validateDeploymentId(req, response) {

  let deploymentObj = await getDeployment(req.payload.deploymentId);

  if (deploymentObj == null) {

    await invalidDeploymentId(response);
  }
}

async function invalidDeploymentId(response) {
  response.success = false;
  response.message = 'Error';
  response.description = 'Invalid Deployment Id.';
}

async function populateDeploymentInfo(deployment, req) {

  deployment.siteId = req.payload.siteId;
  deployment.deploymentDate = req.payload.deploymentDate;
  deployment.amShiftDeployment = req.payload.amShiftDeployment;
  deployment.pmShiftDeployment = req.payload.pmShiftDeployment;
}

module.exports = {
  saveDeployment: saveDeployment,
  getDeploymentBySiteName: getDeploymentBySiteName,
  getDeployment: getDeployment,
  validateDeploymentId: validateDeploymentId,
  invalidDeploymentId: invalidDeploymentId,
  populateDeploymentInfo: populateDeploymentInfo,
  getDeploymentByDate: getDeploymentByDate
}