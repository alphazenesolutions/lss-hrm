'use strict';

const Joi = require('joi');

const udpateDeploymentSchema = Joi.object({
    _id: Joi.string().required(),
    siteId: Joi.string().required(),
    deploymentDate: Joi.date().required(),
    amShiftDeployment: Joi.object({
        breakTime: Joi.object().allow('', null),
        remarks: Joi.string().allow('', null),
        officers: Joi.array().allow('', null)
    }).allow('', null),
    pmShiftDeployment: Joi.object({
        breakTime: Joi.object().allow('', null),
        remarks: Joi.string().allow('', null),
        officers: Joi.array().allow('', null)
    }).allow('', null),
});

module.exports = udpateDeploymentSchema;

