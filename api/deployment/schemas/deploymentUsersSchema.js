'use strict';

const Joi = require('joi');

const deploymentUsersSchema = Joi.object({
    deploymentDate: Joi.date().required()
});

module.exports = deploymentUsersSchema;

