'use strict';

const Joi = require('joi');

const addDeploymentSchema = Joi.object({
    siteId: Joi.string().required(),
    deploymentDate: Joi.date().required(),
    amShiftDeployment: Joi.object({
        breakTime: Joi.object().allow('', null),
        remarks: Joi.string().allow('', null),
        officers: Joi.array().allow('', null)
    }).allow('', null),
    pmShiftDeployment: Joi.object({
        breakTime: Joi.object().allow('', null),
        remarks: Joi.string().allow('', null),
        officers: Joi.array().allow('', null)
    }).allow('', null),
});

module.exports = addDeploymentSchema;

