'use strict';

const Joi = require('joi');

const deploymentReportSchema = Joi.object({
    siteId: Joi.string().required(),
    deploymentDate: Joi.date().required()
});

module.exports = deploymentReportSchema;

