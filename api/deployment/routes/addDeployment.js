'use strict';

const Deployment = require('../model/Deployment');
const addDeploymentSchema = require('../schemas/addDeploymentSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const create = require('../../commons/util/collectionFunctions').create;
const populateDeploymentInfo = require('../util/deploymentFunctions').populateDeploymentInfo;
const getDeploymentByDate = require('../util/deploymentFunctions').getDeploymentByDate;
const successResponse = require('../../commons/util/commonFunctions').successResponse;
const constants = require('../../../constants');
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;

module.exports = {
  method: 'POST',
  path: '/api/v1/deployment/add',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;

      var response = {
        'success': true,
        'time': new Date()
      }

      await validateSiteId(req, response);

      if (!response.success) {
        return h.response(response).code(200);
      }

      let deploymentObj = await getDeploymentByDate(new Date(), req.payload.siteName);

      if (deploymentObj != null) {
        duplicateResponse(response);
        return h.response(response).code(200);
      }

      let deployment = new Deployment();
      deployment.userId = userObj._id;
      deployment.dateCreated = new Date();

      await populateDeploymentInfo(deployment, req);

      deployment = await create(Deployment, deployment);

      await successResponse(response, constants.DEPLOYMENT, deployment);
      return h.response(response).code(200);

    },
    tags: ['api'],
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: addDeploymentSchema
    }
  }
}

async function duplicateResponse(response) {
  response.success = false;
  response.message = 'Error';
  response.description = 'Duplicate Deployment.';
}