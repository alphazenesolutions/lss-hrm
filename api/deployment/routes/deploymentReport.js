'use strict';

const Deployment = require('../model/Deployment');
const deploymentReportSchema = require('../schemas/deploymentReportSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const successGetResponse = require('../../commons/util/commonFunctions').successGetResponse;
const getDataByFilter = require('../../commons/util/collectionFunctions').getDataByFilter;
const getStartEndTime = require('../../commons/util/dateFunctions').getStartEndTime;

module.exports = {
  method: 'POST',
  path: '/api/v1/deployment/report',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var response = {
        'success': true,
        'time': new Date()
      }

      if (req.payload.siteId != null) {

        await validateSiteId(req, response);

        if (!response.success) {
          return h.response(response).code(200);
        }
      }
      let queryFilter = await populateQueryFilter(req);
      let deploymentData = await getDataByFilter(Deployment, queryFilter);

      await successGetResponse(response, deploymentData);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: deploymentReportSchema
    }
  }
}

async function populateQueryFilter(req) {

  let timeOffset = null;
  if (req.headers.timezone != null) {

    timeOffset = parseInt(req.headers.timezone);
  }
  
  let startEndTime = await getStartEndTime(req.payload.deploymentDate, timeOffset);

  let filter = {

    siteId: req.payload.siteId,
    deploymentDate: { "$gte": startEndTime.startDate, "$lt": startEndTime.endDate }
  }

  return filter;
}