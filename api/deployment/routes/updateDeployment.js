'use strict';

const Deployment = require('../model/Deployment');
const updateDeploymentSchema = require('../schemas/updateDeploymentSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const constants = require('../../../constants');
const populateDeploymentInfo = require('../util/deploymentFunctions').populateDeploymentInfo;
const _ = require('lodash');
const update = require('../../commons/util/collectionFunctions').update;
const getById = require('../../commons/util/collectionFunctions').getById;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const successUpdateResponse = require('../../commons/util/commonFunctions').successUpdateResponse;

module.exports = {
  method: 'POST',
  path: '/api/v1/deployment/update',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      let response = {
        'success': true,
        'time': new Date()
      }

      await validateSiteId(req, response);

      if (!response.success) {

        return h.response(response).code(200);
      }

      let deployment = await getById(Deployment, req.payload._id);
      if (deployment == null || _.isEmpty(deployment)) {

        await objectNotFound(response, constants.DEPLOYMENT);
        return h.response(response).code(200);
      }

      deployment.updateUser = userObj._id;
      deployment.updatedDate = new Date();

      await populateDeploymentInfo(deployment, req);
      await update(Deployment, deployment);
      await successUpdateResponse(response, constants.DEPLOYMENT);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: updateDeploymentSchema
    }
  }
}