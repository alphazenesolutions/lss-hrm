'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const deploymentModel = new Schema({
  userId: { type: String, required: true },
  userUpdated: { type: String, required: false },
  dateCreated: { type: Date, required: true },
  dateUpdated: { type: Date, required: false },
  
  siteId: { type: String, required: true },
  deploymentDate: { type: Date, required: true },
  amShiftDeployment: {
    type: {
      breakTime: { type: Object, required: false },
      remarks: { type: String, required: false },
      officers: { type: Array, required: false },
    }, required: false
  },
  pmShiftDeployment: {
    type: {
      breakTime: { type: Object, required: false },
      remarks: { type: String, required: false },
      officers: { type: Array, required: false },
    }, required: false
  }
});

module.exports = mongoose.model('Deployment', deploymentModel);