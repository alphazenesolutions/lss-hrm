'use strict';

const Joi = require('joi');

const dailyCheckListUpdateSchema = Joi.object({
    _id: Joi.string().required(),
    dailyCheckList: Joi.object().required(),
    handOverTo: Joi.string().required(),
    siteId: Joi.string().required()
});

module.exports = dailyCheckListUpdateSchema;