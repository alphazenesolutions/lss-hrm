'use strict';

const Joi = require('joi');

const lostFoundSchema = Joi.object({
  location: Joi.string().required(),
  siteId: Joi.string().required(),
  informant: Joi.string().allow('', null),
  contactNo: Joi.string().allow('', null),
  dateTime: Joi.date().required(),
  items: Joi.object().allow('', null),
  images: Joi.any().allow('', null)
});

module.exports = lostFoundSchema;