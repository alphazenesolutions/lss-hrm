'use strict';

const Joi = require('joi');

const fireIncidentReportSchema = Joi.object({
  siteId: Joi.string().required(),
  fromDate: Joi.date().allow('', null),
  toDate: Joi.date().allow('', null)
});

module.exports = fireIncidentReportSchema;