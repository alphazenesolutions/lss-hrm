'use strict';

const Joi = require('joi');

const reviewActionUpdateSchema = Joi.object({
  _id: Joi.string().required(),
  siteId: Joi.string().required(),
  incidentType: Joi.string().allow('', null),
  assignmentName: Joi.string().allow('', null),
  location: Joi.string().allow('', null),
  dateTime: Joi.date().required(),
  reportedBy: Joi.string().allow('', null),
  incidentDescription: Joi.string().allow('', null),
  personalsInvolved: Joi.array().allow('', null),
  companyActions: Joi.string().allow('', null)
});

module.exports = reviewActionUpdateSchema;