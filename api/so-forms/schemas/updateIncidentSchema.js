'use strict';

const Joi = require('joi');

const incidentReportSchema = Joi.object({
    _id: Joi.string().required(),
    siteId: Joi.string().required(),
    reportNo: Joi.string().allow('', null),
    location: Joi.string().allow('', null),
    incidentDate: Joi.date().required(),
    subject: Joi.string().allow('', null),
    victimParticulars: Joi.object().allow('', null),
    incidentDescription: Joi.string().allow('', null),
    followUpActionTaken: Joi.string().allow('', null)
});

module.exports = incidentReportSchema;