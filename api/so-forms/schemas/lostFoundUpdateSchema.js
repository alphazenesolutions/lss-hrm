'use strict';

const Joi = require('joi');

const lostFoundUpdateSchema = Joi.object({
  _id: Joi.string().required(),
  location: Joi.string().required(),
  siteId: Joi.string().required(),
  informant: Joi.string().allow('', null),
  contactNo: Joi.string().allow('', null),
  dateTime: Joi.date().required(),
  items: Joi.object().allow('', null)
});

module.exports = lostFoundUpdateSchema;