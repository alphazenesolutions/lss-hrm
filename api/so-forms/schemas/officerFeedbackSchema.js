'use strict';

const Joi = require('joi');

const officerFeedbackSchema = Joi.object({
  siteId: Joi.string().allow('', null),
  contactNo: Joi.string().allow('', null),
  comments: Joi.string().allow('', null)
});

module.exports = officerFeedbackSchema;