'use strict';

const Joi = require('joi');

const incidentReportSchema = Joi.object({
  siteId: Joi.string().required(),
  fromDate: Joi.date().allow('', null),
  toDate: Joi.date().allow('', null)
});

module.exports = incidentReportSchema;