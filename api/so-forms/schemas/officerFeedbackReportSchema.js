'use strict';

const Joi = require('joi');

const officerFeedbackReportSchema = Joi.object({
  fromDate: Joi.date().allow('', null),
  toDate: Joi.date().allow('', null)
});

module.exports = officerFeedbackReportSchema;