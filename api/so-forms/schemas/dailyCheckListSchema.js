'use strict';

const Joi = require('joi');

const dailyCheckListSchema = Joi.object({
  dailyCheckList: Joi.object().required(),
  handOverTo: Joi.string().required(),
  siteId: Joi.string().required(),
  images: Joi.array().allow('', null)
});

module.exports = dailyCheckListSchema;