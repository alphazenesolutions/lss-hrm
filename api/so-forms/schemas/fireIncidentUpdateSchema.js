'use strict';

const Joi = require('joi');

const fireIncidentSchema = Joi.object({
  _id: Joi.string().required(),
  siteId: Joi.string().required(),
  incidentType: Joi.string().required(),
  incidentDateTime: Joi.date().required(),
  blockFloorZone: Joi.string().allow('', null),
  location: Joi.string().allow('', null),
  cause: Joi.string().allow('', null),
  involvedSOCount: Joi.number().allow('', null),
  response: Joi.object().allow('', null),
  remarks: Joi.string().allow('', null),
  fullDescription: Joi.string().allow('', null),
  drillEndTime: Joi.string().allow('', null),
  securityStaff: Joi.string().allow('', null),
  managementStaff: Joi.string().allow('', null),
});

module.exports = fireIncidentSchema;