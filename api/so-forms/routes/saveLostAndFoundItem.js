'use strict';

const FoundItems = require('../model/FoundItems');
const lostFoundSchema = require('../schemas/lostFoundSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const saveFoundItems = require('../util/itemFunctions').saveFoundItems;
const uploadBase64Images = require('../../commons/util/base64ImageFunctions').uploadBase64Images;
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const constants = require('../../../constants');
const saveOccurrenceBookWithDetails = require('../../oe-forms/util/occurrenceBookFunctions').saveOccurrenceBookWithDetails;
const successResponse = require('../../commons/util/commonFunctions').successResponse;
const populateFoundItems = require('../util/itemFunctions').populateFoundItems;

module.exports = {
  method: 'POST',
  path: '/api/v1/items/found',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      var response = {
        'success': true,
        'time': new Date()
      }

      await validateSiteId(req, response);

      if (!response.success) {
        return h.response(response).code(200);
      }

      let foundItems = new FoundItems();
      foundItems.userId = userObj._id;
      foundItems.dateCreated = new Date();

      await populateFoundItems(foundItems, req);
      foundItems = await saveFoundItems(foundItems);

      const images = req.payload['images'];
      await uploadBase64Images(images, foundItems, FoundItems);

      let occurenceDesc = constants.LOST_FOUND + ' has reported by ' + userObj.firstName + ' ' + userObj.lastName;
      await saveOccurrenceBookWithDetails(req, constants.LOST_FOUND, occurenceDesc);

      await successResponse(response, constants.LOST_FOUND, foundItems);
      return h.response(response).code(200);

    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: lostFoundSchema
    }
  }
}