'use strict';

const FoundItems = require('../model/FoundItems');
const lostFoundUpdateSchema = require('../schemas/lostFoundUpdateSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const populateFoundItems = require('../util/itemFunctions').populateFoundItems;
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const constants = require('../../../constants');
const _ = require('lodash');
const update = require('../../commons/util/collectionFunctions').update;
const getById = require('../../commons/util/collectionFunctions').getById;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const successUpdateResponse = require('../../commons/util/commonFunctions').successUpdateResponse;

module.exports = {
  method: 'POST',
  path: '/api/v1/items/found/update',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      var response = {
        'success': true,
        'time': new Date()
      }

      await validateSiteId(req, response);

      if (!response.success) {
        return h.response(response).code(200);
      }

      let foundItems = await getById(FoundItems, req.payload._id);
      if (foundItems == null || _.isEmpty(foundItems)) {

        await objectNotFound(response, constants.LOST_FOUND);
        return h.response(response).code(200);
      }

      foundItems.updateUser = userObj._id;
      foundItems.updatedDate = new Date();

      await populateFoundItems(foundItems, req);
      await update(FoundItems, foundItems);
      await successUpdateResponse(response, constants.LOST_FOUND);
      return h.response(response).code(200);

    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: lostFoundUpdateSchema
    }
  }
}

