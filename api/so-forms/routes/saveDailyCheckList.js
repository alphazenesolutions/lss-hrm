'use strict';

const DailyCheckList = require('../model/DailyCheckList');
const dailyCheckListSchema = require('../schemas/dailyCheckListSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const getUserByUserId = require('../../users/util/userFunctions').getUserByUserId;
const saveDailyCheckList = require('../util/dailyCheckListFunctions').saveDailyCheckList;
const getFcmTokenByUser = require('../../users/util/fcmFunctions').getFcmTokenByUser;
const firebaseUtils = require('../../notification/util/firebaseUtils');
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const saveOccurrenceBookWithDetails = require('../../oe-forms/util/occurrenceBookFunctions').saveOccurrenceBookWithDetails;
const _ = require('lodash');
const uploadBase64Images = require('../../commons/util/base64ImageFunctions').uploadBase64Images;
const successResponse = require('../../commons/util/commonFunctions').successResponse;
const constants = require('../../../constants');
const sendNotification = require('../../notification/util/notificationFunctions').sendNotification;
const sendNotificationToOEs = require('../../notification/util/notificationFunctions').sendNotificationToOEs;
const populateDailyCheckList = require('../util/dailyCheckListFunctions').populateDailyCheckList;

module.exports = {
  method: 'POST',
  path: '/api/v1/daily-checklist/add',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      var response = {
        'success': true,
        'time': new Date()
      }

      await validateSiteId(req, response);

      if (!response.success) {
        return h.response(response).code(200);
      }

      let handOverToUser = await getUserByUserId(req.payload.handOverTo);
      if (handOverToUser == null) {

        await invalidHandoverUser(response);
        return h.response(response).code(400);
      }

      let dailyCheckList = new DailyCheckList();
      dailyCheckList.userId = userObj._id;
      dailyCheckList.dateCreated = new Date();

      await populateDailyCheckList(dailyCheckList, req);
      dailyCheckList = await saveDailyCheckList(dailyCheckList);
      //let previousCheckList = await getPreviousHandoverChecklist(userObj._id);

      const images = req.payload['images'];
      await uploadBase64Images(images, dailyCheckList, DailyCheckList);

      //let handoverFcmTokenObj = await getFcmTokenByUser(req.payload.handOverTo);

      let notificationData = {
        "title": constants.DAILY_CHECKLIST,
        "id": dailyCheckList._id,
        "notifyUserId": req.payload.handOverTo
      };

      await sendNotification(userObj, dailyCheckList, notificationData);
      await sendNotificationToOEs(userObj, dailyCheckList, notificationData);
      
      let occurenceDesc = userObj.firstName + ' ' + userObj.lastName +
        ' handover the duty to ' + handOverToUser.firstName + ' ' + handOverToUser.lastName
      await saveOccurrenceBookWithDetails(req, constants.DAILY_CHECKLIST, occurenceDesc);

      await successResponse(response, constants.DAILY_CHECKLIST, dailyCheckList);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: dailyCheckListSchema
    }
  }
}

async function invalidHandoverUser(response) {
  response.success = false;
  response.message = 'Invalid Handover User';
  response.description = 'Invalid Handover User, Please verify.';
}

async function sendHandoverNotification(req, dailyChecklistObj, handoverFcmTokenObj) {

  let firebaseMessage = {
    "title": "Daily Checklist Handover.",
    "body": { "notification_type": constants.DAILY_CHECKLIST, "id": dailyChecklistObj._id }
  };

  if (handoverFcmTokenObj != null) {

    await firebaseUtils.sendFirebaseMessage(handoverFcmTokenObj.fcmToken, JSON.stringify(firebaseMessage));
  }

}

async function sendRemarksNotification(dailyChecklistObj, currentUser, handoverFcmTokenObj) {

  let firebaseMessage = {
    "title": constants.DAILY_CHECKLIST,
    "body": { "notification_type": constants.DAILY_CHECKLIST, "id": dailyChecklistObj._id }
  };
  let hasRemarks = false;
  let firebaseTokens = [];
  firebaseTokens.push(handoverFcmTokenObj.fcmToken);

  _.forEach(dailyChecklistObj.items, function (item) {

    if (item.remarks != null && item.remarks != '') {
      hasRemarks = true;
    }
  });


  if (hasRemarks) {
    let reportingManagerFcmObj = await getFcmTokenByUser(currentUser.reportingManager);
    if (reportingManagerFcmObj != null) {
      firebaseTokens.push(reportingManagerFcmObj.fcmToken);
    }
    if (firebaseTokens.length > 0) {
      await firebaseUtils.sendFirebaseMessage(firebaseTokens.join(), JSON.stringify(firebaseMessage));
    }
  }
}