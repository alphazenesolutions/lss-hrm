'use strict';

const FireIncident = require('../model/FireIncident');
const fireIncidentUpdateSchema = require('../schemas/fireIncidentUpdateSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const constants = require('../../../constants');
const successUpdateResponse = require('../../commons/util/commonFunctions').successUpdateResponse;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const getById = require('../../commons/util/collectionFunctions').getById;
const update = require('../../commons/util/collectionFunctions').update;
const populateFireIncident = require('../util/fireIncidentFunctions').populateFireIncident;
const _ = require('lodash');

module.exports = {
  method: 'POST',
  path: '/api/v1/fireIncident/update',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      var response = {
        'success': true,
        'time': new Date()
      }
      await validateSiteId(req, response);

      if (!response.success) {
        return h.response(response).code(200);
      }

      let fireIncident = await getById(FireIncident, req.payload._id);

      if (fireIncident == null || _.isEmpty(fireIncident)) {

        await objectNotFound(response, constants.FIRE_DRILL);
        return h.response(response).code(200);
      }

      fireIncident.userUpdated = userObj._id;

      await populateFireIncident(fireIncident, req);
      await update(FireIncident, fireIncident);

      let incidentTitle = constants.FIRE_DRILL; 
      if (req.payload.incidentType == 'Fire') {

        incidentTitle = constants.FIRE_INCIDENT;
      }

      await successUpdateResponse(response, incidentTitle, fireIncident);

      return h.response(response).code(200);

    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: fireIncidentUpdateSchema
    }
  }
}