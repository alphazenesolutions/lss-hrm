'use strict';

const Incident = require('../model/Incident');
const incidentSchema = require('../schemas/incidentSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const saveIncident = require('../util/incidentReportFunctions').saveIncident;
const populateIncidentReport = require('../util/incidentReportFunctions').populateIncidentReport;
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const uploadBase64Images = require('../../commons/util/base64ImageFunctions').uploadBase64Images;
const constants = require('../../../constants');
const saveOccurrenceBookWithDetails = require('../../oe-forms/util/occurrenceBookFunctions').saveOccurrenceBookWithDetails;
const successResponse = require('../../commons/util/commonFunctions').successResponse;
const sendNotificationToOEs = require('../../notification/util/notificationFunctions').sendNotificationToOEs;

module.exports = {
  method: 'POST',
  path: '/api/v1/incidentReport',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      let userObj = req.pre.user;
      let response = {
        'success': true,
        'time': new Date()
      }

      await validateSiteId(req, response);

      if (!response.success) {
        return h.response(response).code(200);
      }

      let incidentReport = new Incident();
      incidentReport.userId = userObj._id;
      incidentReport.dateCreated = new Date();

      await populateIncidentReport(incidentReport, req);
      incidentReport = await saveIncident(incidentReport);

      const images = req.payload['images'];
      await uploadBase64Images(images, incidentReport, Incident);

      let notificationData = {
        "title": constants.INCIDENT_REPORT,
        "id": incidentReport._id
      };
      await sendNotificationToOEs(userObj, incidentReport, notificationData);

      let occurenceDesc = constants.INCIDENT_REPORT + ' has reported by ' + userObj.firstName + ' ' + userObj.lastName;
      await saveOccurrenceBookWithDetails(req, constants.INCIDENT_REPORT, occurenceDesc);
      await successResponse(response, constants.INCIDENT_REPORT, incidentReport);

      return h.response(response).code(200);

    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: incidentSchema
    }
  }
}