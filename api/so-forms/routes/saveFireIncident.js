'use strict';

const FireIncident = require('../model/FireIncident');
const fireIncidentSchema = require('../schemas/fireIncidentSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const saveFireIncident = require('../util/fireIncidentFunctions').saveFireIncident;
const populateFireIncident = require('../util/fireIncidentFunctions').populateFireIncident;
const uploadBase64Images = require('../../commons/util/base64ImageFunctions').uploadBase64Images;
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const saveOccurrenceBookWithDetails = require('../../oe-forms/util/occurrenceBookFunctions').saveOccurrenceBookWithDetails;
const successResponse = require('../../commons/util/commonFunctions').successResponse;
const constants = require('../../../constants');
const sendNotificationToOEs = require('../../notification/util/notificationFunctions').sendNotificationToOEs;

module.exports = {
  method: 'POST',
  path: '/api/v1/fireIncident',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      var response = {
        'success': true,
        'time': new Date()
      }
      await validateSiteId(req, response);

      if (!response.success) {
        return h.response(response).code(200);
      }

      let fireIncident = new FireIncident();
      fireIncident.userId = userObj._id;
      fireIncident.dateCreated = new Date();
      await populateFireIncident(fireIncident, req);
      
      let fireIncidentObj = await saveFireIncident(fireIncident);

      const images = req.payload['images'];
      await uploadBase64Images(images, fireIncidentObj, FireIncident);

      let notificationData = {
        "title": constants.FIRE_DRILL,
        "id": fireIncidentObj._id,
        "notifyUserId": userObj.reportingManager
      };

      await sendNotificationToOEs(userObj, fireIncidentObj, notificationData);

      let incidentTitle = null;
      
      if (req.payload.incidentType == 'Fire') {

        incidentTitle = constants.FIRE_INCIDENT;
      } else {

        incidentTitle = constants.FIRE_DRILL;
      }

      let occurenceDesc = incidentTitle + ' has reported by ' + userObj.firstName + ' ' + userObj.lastName;
      await saveOccurrenceBookWithDetails(req, incidentTitle, occurenceDesc);

      await successResponse(response, incidentTitle, fireIncidentObj);

      return h.response(response).code(200);

    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: fireIncidentSchema
    }
  }
}