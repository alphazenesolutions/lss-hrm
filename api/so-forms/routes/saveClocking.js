'use strict';

const Clocking = require('../../clocking/model/Clocking');
const clockingSchema = require('../../clocking/schemas/clockingSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const saveClocking = require('../util/clockingFunctions').saveClocking;
const uploadBase64Images = require('../../commons/util/base64ImageFunctions').uploadBase64Images;
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const constants = require('../../../constants');
const successResponse = require('../../commons/util/commonFunctions').successResponse;
const getSite = require('../../sites/util/siteFunctions').getSite;
const isValidQRCode = require('../../sites/util/siteFunctions').isValidQRCode;

module.exports = {
  method: 'POST',
  path: '/api/v1/clocking/add',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;

      var response = {
        'success': true,
        'time': new Date()
      }

      await validateSiteId(req, response);

      if (!response.success) {
        return h.response(response).code(200);
      }

      let siteObj = await getSite(req.payload.siteId);
      
      if (req.payload.clockingType == 'BARCODE') {
 
        let qrCodes = siteObj.clockingDetails.qrCodes;
        let qrCode = req.payload.barCode;

        let validQR = await isValidQRCode(qrCodes, qrCode);
        if (!validQR) {

          await invalidQRCode(response);
          return h.response(response).code(200);
        }
      }

      let clocking = new Clocking();
      await populateClocking(clocking, userObj, req);
      clocking = await saveClocking(clocking);

      const images = req.payload['images'];
      await uploadBase64Images(images, clocking, Clocking);
      await successResponse(response, constants.CLOCKING, clocking);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: clockingSchema
    }
  }
}

function populateClocking(clocking, userObj, req) {

  clocking.userId = userObj._id;
  clocking.siteId = req.payload.siteId;
  clocking.clockingType = req.payload.clockingType;
  clocking.latLong = req.payload.latLong;

  if (req.payload.barCode != null) {
    clocking.barCode = req.payload.barCode.contents;
  }

  clocking.remarks = req.payload.remarks;
  clocking.dateCreated = new Date();
}

async function invalidQRCode(response) {
  response.success = false;
  response.message = 'Failure';
  response.description = 'Invalid QR Code';
}