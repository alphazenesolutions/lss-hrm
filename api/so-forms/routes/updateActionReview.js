'use strict';
const ReviewAction = require('../model/ReviewAction');
const reviewActionUpdateSchema = require('../schemas/reviewActionUpdateSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const successUpdateResponse = require('../../commons/util/commonFunctions').successUpdateResponse;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const getById = require('../../commons/util/collectionFunctions').getById;
const update = require('../../commons/util/collectionFunctions').update;
const constants = require('../../../constants');
const _ = require('lodash');

module.exports = {
  method: 'POST',
  path: '/api/v1/actionReview/update',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      let userObj = req.pre.user;
      let response = {
        'success': true,
        'time': new Date()
      }

      await validateSiteId(req, response);

      if (!response.success) {
        return h.response(response).code(200);
      }

      let reviewAction = await getById(ReviewAction, req.payload._id);

      if (reviewAction == null || _.isEmpty(reviewAction)) {

        await objectNotFound(response, constants.REVIEW_ACTION);
        return h.response(response).code(200);
      }

      reviewAction.userUpdated = userObj._id;
      await populateReviewAction(reviewAction, req);
      await update(ReviewAction, reviewAction);

      await successUpdateResponse(response, constants.REVIEW_ACTION, reviewAction);
      return h.response(response).code(200);

    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: reviewActionUpdateSchema
    }
  }
}

async function populateReviewAction(reviewAction, req) {
  reviewAction.siteId = req.payload.siteId;
  reviewAction.incidentType = req.payload.incidentType;
  reviewAction.assignmentName = req.payload.assignmentName;
  reviewAction.location = req.payload.location;
  reviewAction.dateTime = req.payload.dateTime;
  reviewAction.reportedBy = req.payload.reportedBy;
  reviewAction.incidentDescription = req.payload.incidentDescription;

  if (req.payload.personalsInvolved != null) {

    reviewAction.personalsInvolved = req.payload.personalsInvolved;
  }
  reviewAction.companyActions = req.payload.companyActions;
  reviewAction.dateUpdated = new Date();
}
