'use strict';

const Incident = require('../model/Incident');
const updateIncidentSchema = require('../schemas/updateIncidentSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const populateIncidentReport = require('../util/incidentReportFunctions').populateIncidentReport;
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const constants = require('../../../constants');
const successUpdateResponse = require('../../commons/util/commonFunctions').successUpdateResponse;
const getById = require('../../commons/util/collectionFunctions').getById;
const update = require('../../commons/util/collectionFunctions').update;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const _ = require('lodash');

module.exports = {
  method: 'POST',
  path: '/api/v1/incidentReport/update',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      let userObj = req.pre.user;
      let response = {
        'success': true,
        'time': new Date()
      }

      await validateSiteId(req, response);

      if (!response.success) {
        return h.response(response).code(200);
      }

      let incident = await getById(Incident, req.payload._id);

      if (incident == null || _.isEmpty(incident)) {

        await objectNotFound(response, constants.FIRE_DRILL);
        return h.response(response).code(200);
      }

      incident.userUpdated = userObj._id;
      incident.dateUpdated = new Date();
      
      await populateIncidentReport(incident, req);
      await update(Incident, incident);
      await successUpdateResponse(response, constants.INCIDENT_REPORT);
      return h.response(response).code(200);

    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: updateIncidentSchema
    }
  }
}