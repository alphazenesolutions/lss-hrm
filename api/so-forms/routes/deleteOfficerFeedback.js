'use strict';

const OfficerFeedback = require('../model/OfficerFeedback');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const getById = require('../../commons/util/collectionFunctions').getById;
const deleteDocument = require('../../commons/util/collectionFunctions').deleteDocument;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const successDeleteResponse = require('../../commons/util/commonFunctions').successDeleteResponse;
const constants = require('../../../constants');
const _ = require('lodash');

module.exports = {
  method: 'DELETE',
  path: '/api/v1/officerFeedback/delete/{_id}',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var response = {
        'time': new Date(),
        'data': {}
      }
      let officerFeedback = await getById(OfficerFeedback, req.params._id);

      if (officerFeedback == null || _.isEmpty(officerFeedback)) {

        await objectNotFound(response, constants.OFFICER_FEEDBACK);
        return h.response(response).code(200);
      }
      await deleteDocument(OfficerFeedback, req.params._id);

      await successDeleteResponse(response, constants.OFFICER_FEEDBACK);
      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema
    }
  }
}