'use strict';

const ReviewAction = require('../model/ReviewAction');
const Incident = require('../model/Incident');
const reviewActionSchema = require('../schemas/reviewActionSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const saveReviewAction = require('../util/reviewActionFunctions').saveReviewAction;
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const successResponse = require('../../commons/util/commonFunctions').successResponse;
const constants = require('../../../constants');
const getById = require('../../commons/util/collectionFunctions').getById;
const update = require('../../commons/util/collectionFunctions').update;

module.exports = {
  method: 'POST',
  path: '/api/v1/actionReview',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      let userObj = req.pre.user;
      let response = {
        'success': true,
        'time': new Date()
      }

      await validateSiteId(req, response);

      let incident = await getById(Incident, req.payload.incidentId);
      if (incident == null) {
        await invalidIncidentId(response);
      }

      if (!response.success) {
        return h.response(response).code(200);
      }

      let reviewAction = new ReviewAction();
      reviewAction.userId = userObj._id;

      await populateReviewAction(reviewAction, req);
      reviewAction = await saveReviewAction(reviewAction);
      incident.aarId = reviewAction._id;
      await update(Incident, incident);

      await successResponse(response, constants.REVIEW_ACTION, reviewAction);
      return h.response(response).code(200);

    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: reviewActionSchema
    }
  }
}

async function invalidIncidentId(response) {
  response.success = false;
  response.message = 'Error';
  response.description = 'Invalid Incident Id.';
}

async function populateReviewAction(reviewAction, req) {
  reviewAction.siteId = req.payload.siteId;
  reviewAction.incidentType = req.payload.incidentType;
  reviewAction.assignmentName = req.payload.assignmentName;
  reviewAction.location = req.payload.location;
  reviewAction.dateTime = req.payload.dateTime;
  reviewAction.reportedBy = req.payload.reportedBy;
  reviewAction.incidentDescription = req.payload.incidentDescription;

  if (req.payload.personalsInvolved != null) {

    reviewAction.personalsInvolved = req.payload.personalsInvolved;
  }
  reviewAction.companyActions = req.payload.companyActions;
  reviewAction.dateCreated = new Date();
}
