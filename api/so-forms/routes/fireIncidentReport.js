'use strict';

const FireIncident = require('../model/FireIncident');
const fireIncidentReportSchema = require('../schemas/fireIncidentReportSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const successGetResponse = require('../../commons/util/commonFunctions').successGetResponse;
const getDataByFilter = require('../../commons/util/collectionFunctions').getDataByFilter;
const getStartTime = require('../../commons/util/dateFunctions').getStartTime;
const getEndTime = require('../../commons/util/dateFunctions').getEndTime;

module.exports = {
  method: 'POST',
  path: '/api/v1/fireIncident/report',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var response = {
        'success': true,
        'time': new Date()
      }

      await validateSiteId(req, response);

      if (!response.success) {
        return h.response(response).code(200);
      }
      let queryFilter = await populateQueryFilter(req);
      let data = await getDataByFilter(FireIncident, queryFilter);

      await successGetResponse(response, data);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: fireIncidentReportSchema
    }
  }
}

async function populateQueryFilter(req) {

  let timeOffset = null;
  if (req.headers.timezone != null) {

    timeOffset = parseInt(req.headers.timezone);
  }

  let startDate = await getStartTime(req.payload.fromDate, timeOffset);
  let endDate = await getEndTime(req.payload.toDate, timeOffset);

  let filter = {
    siteId: req.payload.siteId,
    incidentDateTime: { "$gte": startDate, "$lt": endDate }
  }

  return filter;
}