'use strict';

const FoundItems = require('../model/FoundItems');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getById = require('../../commons/util/collectionFunctions').getById;
const getUserById = require('../../users/util/userFunctions').getUserById;
const successGetResponse = require('../../commons/util/commonFunctions').successGetResponse;

module.exports = {
  method: 'GET',
  path: '/api/v1/items/found/{id}',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var response = {
        'time': new Date(),
        'data': {}
      }

      let collectionObj = await getById(FoundItems, req.params.id);

      await successGetResponse(response, collectionObj);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema
    }
  }
}