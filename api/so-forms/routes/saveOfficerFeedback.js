'use strict';

const OfficerFeedback = require('../model/OfficerFeedback');
const officerFeedbackSchema = require('../schemas/officerFeedbackSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const saveOfficerFeedback = require('../util/officerFeedbackFunctions').saveOfficerFeedback;
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const successResponse = require('../../commons/util/commonFunctions').successResponse;
const constants = require('../../../constants');

module.exports = {
  method: 'POST',
  path: '/api/v1/officerFeedback',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      var response = {
        'success': true,
        'time': new Date()
      }

      await validateSiteId(req, response);

      if (!response.success) {
        return h.response(response).code(200);
      }

      let officerFeedback = new OfficerFeedback();
      await populateOfficerFeedback(officerFeedback, userObj, req);
      officerFeedback = await saveOfficerFeedback(officerFeedback);
      await successResponse(response, constants.OFFICER_FEEDBACK, officerFeedback);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: officerFeedbackSchema
    }
  }
}

function populateOfficerFeedback(officerFeedback, userObj, req) {
  officerFeedback.userId = userObj._id;
  officerFeedback.siteId = req.payload.siteId;
  officerFeedback.contactNo = req.payload.contactNo;
  officerFeedback.comments = req.payload.comments;
  officerFeedback.dateCreated = new Date();
}