'use strict';

const DailyCheckList = require('../model/DailyCheckList');
const dailyCheckListUpdateSchema = require('../schemas/dailyCheckListUpdateSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const getUserByUserId = require('../../users/util/userFunctions').getUserByUserId;
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const populateDailyCheckList = require('../util/dailyCheckListFunctions').populateDailyCheckList;
const constants = require('../../../constants');
const _ = require('lodash');
const update = require('../../commons/util/collectionFunctions').update;
const getById = require('../../commons/util/collectionFunctions').getById;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const successUpdateResponse = require('../../commons/util/commonFunctions').successUpdateResponse;

module.exports = {
  method: 'POST',
  path: '/api/v1/daily-checklist/update',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      var response = {
        'success': true,
        'time': new Date()
      }

      await validateSiteId(req, response);

      if (!response.success) {
        return h.response(response).code(200);
      }

      let handOverToUser = await getUserByUserId(req.payload.handOverTo);
      if (handOverToUser == null) {

        await invalidHandoverUser(response);
        return h.response(response).code(400);
      }

      let dailyCheckList = await getById(DailyCheckList, req.payload._id);

      if (dailyCheckList == null || _.isEmpty(dailyCheckList)) {

        await objectNotFound(response, constants.DAILY_CHECKLIST);
        return h.response(response).code(200);
      }

      dailyCheckList.userUpdated = userObj._id;
      dailyCheckList.dateUpdated = new Date();

      await populateDailyCheckList(dailyCheckList, req);
      await update(DailyCheckList, dailyCheckList);
      await successUpdateResponse(response, constants.DAILY_CHECKLIST, dailyCheckList);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: dailyCheckListUpdateSchema
    }
  }
}

async function invalidHandoverUser(response) {
  response.success = false;
  response.message = 'Invalid Handover User';
  response.description = 'Invalid Handover User, Please verify.';
}

