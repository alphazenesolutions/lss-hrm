'use strict';
const Boom = require('boom');

const FireIncident = require('../model/FireIncident');

async function saveFireIncident(fireIncident) {

  var result;

  await FireIncident.create(fireIncident).then((savedObj) => {
    result = savedObj;
  }).catch((err) => {
    console.log('error occurred while saving fire incident.. ' + err);
    return Boom.badImplementation('Error', err);
  });

  return result;
}

async function getFireIncident(incidnetId) {
  var results = await FireIncident.find(
    { _id: incidnetId }
  );

  return results;
}

async function updateFireIncident(fireIncidentReport) {

  await FireIncident
    .findOneAndUpdate({ _id: fireIncidentReport.id }, fireIncidentReport);
}

async function populateFireIncident(fireIncident, req) {
  fireIncident.siteId = req.payload.siteId;
  fireIncident.incidentType = req.payload.incidentType;
  fireIncident.incidentDateTime = req.payload.incidentDateTime;
  fireIncident.blockFloorZone = req.payload.blockFloorZone;
  fireIncident.location = req.payload.location;
  fireIncident.cause = req.payload.cause;
  fireIncident.involvedSOCount = req.payload.involvedSOCount;
  fireIncident.response = req.payload.response;
  fireIncident.remarks = req.payload.remarks;
  fireIncident.fullDescription = req.payload.fullDescription;
  fireIncident.drillEndTime = req.payload.drillEndTime;
  fireIncident.securityStaff = req.payload.securityStaff;
  fireIncident.managementStaff = req.payload.managementStaff;
  fireIncident.dateUpdated = new Date();
}

module.exports = {
  saveFireIncident: saveFireIncident,
  getFireIncident: getFireIncident,
  updateFireIncident: updateFireIncident,
  populateFireIncident: populateFireIncident
}