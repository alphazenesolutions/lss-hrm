'use strict';
const Boom = require('boom');

const FoundItems = require('../model/FoundItems');

async function saveFoundItems(foundItems) {
  
  var result;

  await FoundItems.create(foundItems).then((itemsObj) => {
    result = itemsObj;
  }).catch((err) => {
    console.log('error occurred while saving found items.. ' + err);
    return Boom.badImplementation('Error', err);
  });

  return result;
}

async function updateLostFound(lostFound) {

  await FoundItems
    .findOneAndUpdate({ _id: lostFound.id }, lostFound);
}

async function populateFoundItems(foundItems, req) {
  foundItems.siteId = req.payload.siteId;
  foundItems.location = req.payload.location;
  foundItems.informant = req.payload.informant;
  foundItems.contactNo = req.payload.contactNo;
  foundItems.dateTime = req.payload.dateTime;
  foundItems.items = req.payload.items;
}

module.exports = {
  saveFoundItems: saveFoundItems,
  updateLostFound: updateLostFound,
  populateFoundItems: populateFoundItems
}