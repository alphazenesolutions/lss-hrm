'use strict';
const Boom = require('boom');

const OfficerFeedback = require('../model/OfficerFeedback');

async function saveOfficerFeedback(officerFeedback) {
  
  var result;

  await OfficerFeedback.create(officerFeedback).then((savedObj) => {
    result = savedObj;
  }).catch((err) => {
    console.log('error occurred while saving officer feedback.. ' + err);
    return Boom.badImplementation('Error', err);
  });

  return result;
}

module.exports = {
  saveOfficerFeedback: saveOfficerFeedback
}