'use strict';
const Boom = require('boom');

const Clocking = require('../../clocking/model/Clocking');

async function saveClocking(clocking) {

  var result;

  await Clocking.create(clocking).then((savedObj) => {
    result = savedObj;
  }).catch((err) => {
    console.log('error occurred while saving clocking.. ' + err);
    return Boom.badImplementation('Error', err);
  });

  return result;
}

async function updateClocking(clocking) {

  await Clocking
    .findOneAndUpdate({ _id: clocking.id }, clocking);
}


async function getClockingBySite(siteId) {
  var results = await Clocking.find(
    { siteId: siteId }
  );

  return results;
}

module.exports = {
  saveClocking: saveClocking,
  updateClocking: updateClocking,
  getClockingBuSite: getClockingBySite
}