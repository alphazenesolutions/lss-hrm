'use strict';
const Boom = require('boom');

const ReviewAction = require('../model/ReviewAction');

async function saveReviewAction(reviewAction) {

  var result;

  await ReviewAction.create(reviewAction).then((savedObj) => {
    result = savedObj;
  }).catch((err) => {
    console.log('error occurred while saving Review Action.. ' + err);
    return Boom.badImplementation('Error', err);
  });

  return result;
}

module.exports = {
  saveReviewAction: saveReviewAction
}