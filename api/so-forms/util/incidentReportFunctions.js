'use strict';
const Boom = require('boom');

const Incident = require('../model/Incident');

async function saveIncident(incident) {

  var result;

  await Incident.create(incident).then((savedObj) => {
    result = savedObj;
  }).catch((err) => {
    console.log('error occurred while saving incident.. ' + err);
    return Boom.badImplementation('Error', err);
  });

  return result;
}

async function updateIncident(incident) {

  await Incident
    .findOneAndUpdate({ _id: incident.id }, incident);
}

async function populateIncidentReport(incidentReport, req) {
  incidentReport.siteId = req.payload.siteId;
  incidentReport.reportNo = req.payload.reportNo;
  incidentReport.location = req.payload.location;
  incidentReport.incidentDate = req.payload.incidentDate;
  incidentReport.subject = req.payload.subject;
  incidentReport.victimParticulars = req.payload.victimParticulars;
  incidentReport.incidentDescription = req.payload.incidentDescription;
  incidentReport.followUpActionTaken = req.payload.followUpActionTaken;
}

module.exports = {
  saveIncident: saveIncident,
  updateIncident: updateIncident,
  populateIncidentReport: populateIncidentReport
}