'use strict';
const Boom = require('boom');

const DailyCheckList = require('../model/DailyCheckList');

async function saveDailyCheckList(dailyCheckList) {
  
  var result;

  await DailyCheckList.create(dailyCheckList).then((checkListObj) => {
    result = checkListObj;
  }).catch((err) => {
    console.log('error occurred while saving daily check list.. ' + err);
    return Boom.badImplementation('Error', err);
  });

  return result;
}

async function getPreviousHandoverChecklist(userId){

  let dailyCheckList = await DailyCheckList.findOne(
    { handOverTo: userId, handOverStatus: 'open'}
  );
  return dailyCheckList;
}

async function updateCheckListStatus(dailyChecklist) {

  await DailyCheckList
    .findOneAndUpdate({ _id: dailyChecklist.id }, dailyChecklist);
}

function populateDailyCheckList(dailyCheckList, req) {
  dailyCheckList.siteId = req.payload.siteId;
  dailyCheckList.dailyCheckList = req.payload.dailyCheckList;
  dailyCheckList.handOverTo = req.payload.handOverTo;
  dailyCheckList.handOverStatus = 'open';
}

module.exports = {
  saveDailyCheckList: saveDailyCheckList,
  getPreviousHandoverChecklist: getPreviousHandoverChecklist,
  updateCheckListStatus: updateCheckListStatus,
  populateDailyCheckList: populateDailyCheckList
}