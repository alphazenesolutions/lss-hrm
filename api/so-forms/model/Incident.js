'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const incidentModel = new Schema({
    userId: { type: String, required: true },
    userUpdated: { type: String, required: false },
    siteId: { type: String, required: true },
    reportNo: { type: String, required: false },
    location: { type: String, required: false },
    incidentDate: { type: Date, required: true },
    subject: { type: String, required: false },
    victimParticulars: {
        type: {
            fullName: { type: String, required: false },
            sex: { type: String, required: false },
            nricOrFinNo: { type: String, required: false },
            address: { type: String, required: false },
        }, required: false
    },
    incidentDescription: { type: String, required: false },
    followUpActionTaken: { type: String, required: false },
    aarId: { type: String, required: false },
    images: { type: Array, required: false },
    dateCreated: { type: Date, required: true },
    dateUpdated: { type: Date, required: false }
});

module.exports = mongoose.model('Incident', incidentModel);