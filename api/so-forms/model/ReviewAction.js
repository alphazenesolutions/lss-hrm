'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const reviewActionModel = new Schema({
  userId: { type: String, required: true },
  siteId: { type: String, required: true },
  reportNo: { type: String, required: false },
  incidentId: { type: String, required: false },
  incidentType: { type: String, required: false },
  assignmentName: { type: String, required: false },
  location: { type: String, required: false },
  dateTime: { type: Date, required: false },
  reportedBy: { type: String, required: false },
  incidentDescription: { type: String, required: false },
  personalsInvolved: { type: Array, required: false },
  companyActions: { type: String, required: false },
  dateCreated: { type: Date, required: false },
  userUpdated: { type: String, required: false },
  dateUpdated: { type: Date, required: false }  
});

module.exports = mongoose.model('ReviewAction', reviewActionModel);