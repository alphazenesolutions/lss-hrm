'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const foundItemsModel = new Schema({
  userId: { type: String, required: true },
  siteId: { type: String, required: true },
  location: { type: String, required: true },
  informant: { type: String, required: false },
  contactNo: { type: String, required: false },
  dateTime: { type: Date, required: true },
  dateCreated: { type: Date, required: true },
  items: { type: Object, required: false },
  images: { type: Array, required: false },
  updatedUser: { type: String, required: false },
  updatedDate: { type: Date, required: false }

});

module.exports = mongoose.model('FoundItems', foundItemsModel);