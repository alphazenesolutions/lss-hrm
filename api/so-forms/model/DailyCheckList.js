'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const dailyCheckListModel = new Schema({
  userId: { type: String, required: true },
  siteId: { type: String, required: true },
  dailyCheckList: { type: Object, required: true },
  dateCreated: { type: Date, required: true },
  handOverTo: { type: String, required: true },
  images: { type: Array, required: false },
  handOverStatus: { type: String, required: true },
  userUpdated: { type: String, required: false },
  dateUpdated: { type: Date, required: false }
  
});

module.exports = mongoose.model('DailyCheckList', dailyCheckListModel);