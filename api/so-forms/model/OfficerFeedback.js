'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const officerFeedbackModel = new Schema({
  userId: { type: String, required: true },
  siteId: { type: String, required: true },
  contactNo: { type: String, required: true },
  comments: { type: String, required: true },
  dateCreated: { type: Date, required: true }
});

module.exports = mongoose.model('OfficerFeedback', officerFeedbackModel);