'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const fireIncidentModel = new Schema({
  userId: { type: String, required: true },
  siteId: { type: String, required: true },
  incidentType: { type: String, required: false },
  incidentDateTime: { type: Date, required: false },
  blockFloorZone: { type: String, required: false },
  location: { type: String, required: false },
  cause: { type: String, required: false },
  involvedSOCount: { type: Number, required: false },
  response: { type: Object, required: false },
  remarks: { type: String, required: false },
  fullDescription: { type: String, required: false },
  drillEndTime: { type: String, required: false },
  securityStaff: { type: String, required: false },
  managementStaff: { type: String, required: false },
  images: { type: Array, required: false },
  userUpdated: { type: String, required: false },
  dateCreated: { type: Date, required: true },
  dateUpdated: { type: Date, required: false }
});

module.exports = mongoose.model('FireIncident', fireIncidentModel);