'use strict';

const Joi = require('joi');

const generatePaySlipSchema = Joi.object({
    
    workingDays: Joi.number().required().error(new Error(
        "Working days is required."
    )),
    holidays: Joi.number().required().error(new Error(
        "Holidays is required."
    )),
    creditDate: Joi.date().required().error(new Error(
        "Credit Date is required."
    ))
});

module.exports = generatePaySlipSchema;