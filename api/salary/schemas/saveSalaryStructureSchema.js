'use strict';

const Joi = require('joi');

const saveSalaryStructureSchema = Joi.object({
    employeeId: Joi.string().required().error(new Error(
        "Employee Id is required."
    )),
    basicSalary: Joi.number().allow('', null),
    grooming: Joi.number().allow('', null),
    attendance: Joi.number().allow('', null),
    others: Joi.number().allow('', null),
    advance: Joi.number().allow('', null),
    loan: Joi.number().allow('', null),
    ph: Joi.number().allow('', null),
    phDays: Joi.number().allow('', null),
    restDaySalary: Joi.number().allow('', null),
    restDaysWorked: Joi.number().allow('', null),
    medicalReimbursement: Joi.number().allow('', null),
    overTime: Joi.number().allow('', null),
    daysWorked: Joi.number().allow('', null),
    employeeCpf: Joi.number().allow('', null),
    employerCpf: Joi.number().allow('', null),
    note: Joi.string().allow('', null)
});

module.exports = saveSalaryStructureSchema;