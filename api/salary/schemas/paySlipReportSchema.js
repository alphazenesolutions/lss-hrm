'use strict';

const Joi = require('joi');

const payslipReportSchema = Joi.object({

    payslipDate: Joi.string().required().error(new Error(
        "PaySlip Date  is required."
    ))
});

module.exports = payslipReportSchema;