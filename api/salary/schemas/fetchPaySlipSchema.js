'use strict';

const Joi = require('joi');

const fetchPaySlipSchema = Joi.object({
    employeeId: Joi.string().required().error(new Error(
        "Employee Id is required."
    )),
    payslipDate: Joi.string().required().error(new Error(
        "PaySlip Date is required."
    ))
});

module.exports = fetchPaySlipSchema;