'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const salaryStructureModel = new Schema({
  userId: { type: String, required: true },
  employeeId: { type: String, required: true },
  basicSalary: { type: Number, required: false },
  grooming: { type: Number, required: false },
  attendance: { type: Number, required: false },
  others: { type: Number, requireds: false },
  advance: { type: Number, required: false },
  loan: { type: Number, required: false },
  ph: { type: Number, required: false },
  phDays: { type: Number, required: false },
  phDays: { type: Number, required: false },
  restDaySalary: { type: Number, required: false },
  restDaysWorked: { type: Number, required: false },
  medicalReimbursement: { type: Number, required: false },
  daysWorked: { type: Number, required: false },
  overTime: { type: Number, required: false },
  employeeCpf: { type: Number, required: false },
  employerCpf: { type: Number, required: false },
  userUpdated: { type: String, required: false },
  dateCreated: { type: Date, required: false },
  dateUpdated: { type: Date, required: false },
  note: { type: String, required: false }
});

module.exports = mongoose.model('SalaryStructure', salaryStructureModel);