'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const paySlipModel = new Schema({
  userId: { type: String, required: true },
  employeeId: { type: String, required: true },
  workingDays: { type: Number, required: false },
  holidays: { type: Number, required: false },
  basicSalary: { type: Number, required: false },
  grooming: { type: Number, required: false },
  attendance: { type: Number, required: false },
  others: { type: Number, requireds: false },
  advance: { type: Number, required: false },
  loan: { type: Number, required: false },
  ph: { type: Number, required: false },
  phDays: { type: Number, required: false },
  restDaySalary: { type: Number, required: false },
  restDaysWorked: { type: Number, required: false },
  medicalReimbursement: { type: Number, required: false },
  daysWorked: { type: Number, required: false },
  overTime: { type: Number, required: false },
  vacationBalance: { type: Object, required: false },
  payslipDate: { type: String, required: false },
  creditDate: { type: Date, required: false },
  dateCreated: { type: Date, required: false },
  overTimeData: { type: Object, required: false },
  note: { type: String, required: false },
  absentDays: { type: Number, required: false }
});

module.exports = mongoose.model('PaySlip', paySlipModel);