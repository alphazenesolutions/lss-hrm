'use strict';

const SalaryStructure = require('../model/SalaryStructure');
const updateSalaryStructureSchema = require('../schemas/updateSalaryStructureSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const populateSalaryStructure = require('../util/salaryStructureFunctions').populateSalaryStructure;
const constants = require('../../../constants');
const _ = require('lodash');
const update = require('../../commons/util/collectionFunctions').update;
const getById = require('../../commons/util/collectionFunctions').getById;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const successUpdateResponse = require('../../commons/util/commonFunctions').successUpdateResponse;

module.exports = {
  method: 'POST',
  path: '/api/v1/salaryStructure/update',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      var response = {
        'success': true,
        'time': new Date()
      }

      let salaryStructure = await getById(SalaryStructure, req.payload._id);
      
      if (salaryStructure == null || _.isEmpty(salaryStructure)) {

        await objectNotFound(response, constants.SALARY_STRUCTURE);
        return h.response(response).code(200);
      }

      salaryStructure.updateUser = userObj._id;
      salaryStructure.updatedDate = new Date();

      await populateSalaryStructure(salaryStructure, req);
      await update(SalaryStructure, salaryStructure);

      await successUpdateResponse(response, constants.SALARY_STRUCTURE);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: updateSalaryStructureSchema
    }
  }
}