'use strict';

const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const fetchPaySlipSchema = require('../schemas/fetchPaySlipSchema');
const getUserById = require('../../users/util/userFunctions').getUserById;
const successGetResponse = require('../../commons/util/commonFunctions').successGetResponse;
const getPaySlip = require('../util/salaryStructureFunctions').getPaySlip;

module.exports = {
  method: 'POST',
  path: '/api/v1/payslip/fetch',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var response = {
        'time': new Date(),
        'data': {}
      }

      let collectionObj = await getPaySlip(req.payload.employeeId, req.payload.payslipDate);

      await successGetResponse(response, collectionObj);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: fetchPaySlipSchema
    }
  }
}