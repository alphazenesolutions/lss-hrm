'use strict';

const PaySlip = require('../model/PaySlip');
const OverTime = require('../../vacation/model/OverTime');
const Vacation = require('../../vacation/model/Vacation');
const VacationBalance = require('../../vacation/model/VacationBalance');
const generatePaySlipSchema = require('../schemas/generatePaySlipSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const create = require('../../commons/util/collectionFunctions').create;
const constants = require('../../../constants');
const getSalaryStructure = require('../util/salaryStructureFunctions').getSalaryStructure;
const getPaySlip = require('../util/salaryStructureFunctions').getPaySlip;
const _ = require('lodash');
const getDataByFilter = require('../../commons/util/collectionFunctions').getDataByFilter;
const getUsers = require('../../users/util/userFunctions').getUsers;
const getEmployeeAttendanceHistory = require('../../users/util/attendanceFunctions').getEmployeeAttendanceHistory;

module.exports = {
    method: 'POST',
    path: '/api/v1/payslip/generate',
    config: {
        auth: {
            strategy: 'jwt'
        },
        tags: ['api'],
        pre: [
            { method: getUserById, assign: 'user' }
        ],
        handler: async (req, h) => {

            let userObj = req.pre.user;
            let response = {
                'success': true,
                'time': new Date()
            }
            let dateObj = new Date();
            let month = dateObj.getUTCMonth(); //months from 1-12
            let year = dateObj.getUTCFullYear();
            let payslipDate = month + "-" + year;

            var users = await getUsers();

            for (const user of users) {

                let salaryStructure = await getSalaryStructure(user._id);

                if (salaryStructure != null) {

                    let paySlip = await getPaySlip(user._id, payslipDate);

                    if (paySlip == null) {

                        paySlip = new PaySlip();
                        paySlip.userId = userObj._id;
                        paySlip.dateCreated = new Date();
                    }

                    await populatePaySlip(paySlip, req);
                    await populatePaySlipSalaryStructure(paySlip, salaryStructure);

                    var currentDate = new Date();
                    var firstDay = new Date(currentDate.getFullYear(), currentDate.getMonth() - 1, 1);
                    var lastDay = new Date(currentDate.getFullYear(), currentDate.getMonth(), 0);

                    var attendance = await getEmployeeAttendanceHistory(user._id, firstDay, lastDay);

                    paySlip.daysWorked = attendance.length;

                    let overTimeQuery = await getOverTimeQueryFilter(user._id, firstDay, lastDay);
                    let vacationQuery = await getVacationQueryFilter(user._id, firstDay, lastDay);
                    let overTimeData = await getDataByFilter(OverTime, overTimeQuery);
                    let vacationData = await getDataByFilter(Vacation, vacationQuery);
                    
                    paySlip.absentDays = await getAbsentDays(req.payload.workingDays, vacationData, attendance);
                    
                    paySlip.overTimeData = overTimeData;
                    
                    let vacationBalance = await getVacationBalance(user._id);
                    
                    if (vacationBalance != null && vacationBalance.length > 0) {
                        
                        paySlip.vacationBalance = vacationBalance[0].vacationBalance;
                    }

                    paySlip.payslipDate = payslipDate;

                    paySlip = await create(PaySlip, paySlip);
                }
            }

            await successResponse(response, constants.PAYSLIP);
            return h.response(response).code(200);
        },
        // Validate the payload against the Joi schema
        validate: {
            headers: checkCommonHeadersSchema,
            payload: generatePaySlipSchema
        }
    }
}

async function getOverTimeQueryFilter(employeeId, startDate, endDate) {

    let filter = {
        dateCreated: { "$gte": startDate, "$lt": endDate },
        employeeId: employeeId
    }

    return filter;
}

async function getVacationQueryFilter(employeeId, startDate, endDate) {

    let filter = {
        fromDate: { "$gte": startDate, "$lt": endDate },
        employeeId: employeeId,
        status: "APPROVED"
    }

    return filter;
}

async function getAbsentDays(workingDays, vacationData, attendance) {

    let duration = 0;
    let absentDays = 0;

    for (const vacation of vacationData) {
        duration = duration + vacation.duration;
    }

    absentDays = workingDays - duration;
    
    if (attendance != null) {
        absentDays = absentDays - attendance.length;
    }

    return absentDays;
}

async function getVacationBalance(employeeId) {
    let filter = {
        employeeId: employeeId
    }

    let collectionObj = await getDataByFilter(VacationBalance, filter);
    return collectionObj;
}

async function populatePaySlip(paySlip, req) {

    paySlip.workingDays = req.payload.workingDays;
    paySlip.holidays = req.payload.holidays;
    paySlip.creditDate = req.payload.creditDate;
}

async function populatePaySlipSalaryStructure(paySlip, salaryStructure) {

    paySlip.employeeId = salaryStructure.employeeId;
    paySlip.basicSalary = salaryStructure.basicSalary;
    paySlip.grooming = salaryStructure.grooming;
    paySlip.attendance = salaryStructure.attendance;
    paySlip.others = salaryStructure.others;
    paySlip.advance = salaryStructure.advance;
    paySlip.loan = salaryStructure.loan;
    paySlip.ph = salaryStructure.ph;
    paySlip.phDays = salaryStructure.phDays;
    paySlip.restDaySalary = salaryStructure.restDaySalary;
    paySlip.restDaysWorked = salaryStructure.restDaysWorked;
    paySlip.medicalReimbursement = salaryStructure.medicalReimbursement;
    paySlip.overTime = salaryStructure.overTime;
    paySlip.note = salaryStructure.note;
    paySlip.employeeCpf = salaryStructure.employeeCpf;
    paySlip.employerCpf = salaryStructure.employerCpf;
}

async function successResponse(response, title) {
    response.success = true;
    response.message = 'Success';
    response.description = title + ' saved successfully.';
}
