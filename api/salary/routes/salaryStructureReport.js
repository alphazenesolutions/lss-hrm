'use strict';

const SalaryStructure = require('../model/SalaryStructure');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const successGetResponse = require('../../commons/util/commonFunctions').successGetResponse;
const getDataByFilter = require('../../commons/util/collectionFunctions').getDataByFilter;

module.exports = {
  method: 'GET',
  path: '/api/v1/salaryStructure/report',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var response = {
        'success': true,
        'time': new Date()
      }

      let data = await getDataByFilter(SalaryStructure, {});

      await successGetResponse(response, data);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema
    }
  }
}
