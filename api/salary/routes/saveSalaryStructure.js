'use strict';

const SalaryStructure = require('../model/SalaryStructure');
const salaryStructureSchema = require('../schemas/saveSalaryStructureSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const successResponse = require('../../commons/util/commonFunctions').successResponse;
const create = require('../../commons/util/collectionFunctions').create;
const constants = require('../../../constants');
const populateSalaryStructure = require('../util/salaryStructureFunctions').populateSalaryStructure;
const getSalaryStructure = require('../util/salaryStructureFunctions').getSalaryStructure;
const _ = require('lodash');

module.exports = {
  method: 'POST',
  path: '/api/v1/salaryStructure/add',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      var response = {
        'success': true,
        'time': new Date()
      }

      let salaryStructure = await getSalaryStructure(req.payload.employeeId);
      if (salaryStructure != null || !_.isEmpty(salaryStructure)) {

        await duplicateEntry(response, constants.SALARY_STRUCTURE);
        return h.response(response).code(200);
      }

      salaryStructure = new SalaryStructure();
      salaryStructure.userId = userObj._id;
      salaryStructure.dateCreated = new Date();

      await populateSalaryStructure(salaryStructure, req);
      salaryStructure = await create(SalaryStructure, salaryStructure);

      await successResponse(response, constants.SALARY_STRUCTURE, salaryStructure);
      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: salaryStructureSchema
    }
  }
}

async function duplicateEntry(response, title) {
  response.success = false;
  response.message = 'Failed';
  response.description = title + ' already existing.';
}
