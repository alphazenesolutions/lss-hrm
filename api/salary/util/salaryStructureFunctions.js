'use strict';

const PaySlip = require('../model/PaySlip');
const getUsers = require('../../users/util/userFunctions').getUsers;
const sendNotification = require('../../notification/util/notificationFunctions').sendNotification;
const constants = require('../../../constants');
const SalaryStructure = require('../model/SalaryStructure');

async function populateSalaryStructure(salaryStructure, req) {

    salaryStructure.employeeId = req.payload.employeeId;
    salaryStructure.basicSalary = req.payload.basicSalary;
    salaryStructure.grooming = req.payload.grooming;
    salaryStructure.attendance = req.payload.attendance;
    salaryStructure.others = req.payload.others;
    salaryStructure.advance = req.payload.advance;
    salaryStructure.loan = req.payload.loan;
    salaryStructure.ph = req.payload.ph;
    salaryStructure.phDays = req.payload.phDays;
    salaryStructure.restDaysWorked = req.payload.restDaysWorked;
    salaryStructure.restDaySalary = req.payload.restDaySalary;
    salaryStructure.medicalReimbursement = req.payload.medicalReimbursement;
    salaryStructure.overTime = req.payload.overTime;
    salaryStructure.daysWorked = req.payload.daysWorked;
    salaryStructure.employeeCpf = req.payload.employeeCpf;
    salaryStructure.employerCpf = req.payload.employerCpf;
    salaryStructure.note = req.payload.note;
}

async function getSalaryStructure(employeeId) {

    var salaryStructure = await SalaryStructure
        .findOne({ employeeId: employeeId });
    return salaryStructure;
}

async function getPaySlip(employeeId, payslipDate) {

    var paySlip = await PaySlip
        .findOne({ employeeId: employeeId, payslipDate: payslipDate });
    return paySlip;
}

async function sendTrainingNotification(userObj, training, empSelection, selectedEmployees) {
    if (empSelection == 'All Employees') {
        let users = await getUsers();
        for (const user of users) {

            let notificationData = {
                "title": constants.TRAINING,
                "id": training._id,
                "notifyUserId": user._id
            };
            await sendNotification(userObj, training, notificationData);
        }
    }
    else {
        for (const employeeId of selectedEmployees) {

            let notificationData = {
                "title": constants.TRAINING,
                "id": training._id,
                "notifyUserId": employeeId
            };
            await sendNotification(userObj, training, notificationData);
        }
    }
}

module.exports = {
    populateSalaryStructure: populateSalaryStructure,
    sendTrainingNotification: sendTrainingNotification,
    getSalaryStructure: getSalaryStructure,
    getPaySlip: getPaySlip
}