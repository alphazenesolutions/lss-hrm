'use strict';

const Site = require('../model/Site');
const Boom = require('boom');

async function saveSite(siteObj) {

  var result;

  await Site.create(siteObj).then((savedObj) => {
    result = savedObj;
  }).catch((err) => {
    console.log('error occurred while saving site name.. ' + err);
    return Boom.badImplementation('Error', err);
  });

  return result;
}

async function getSiteByName(siteName) {

  try {

    var siteObj = await Site.findOne({
      siteName: siteName
    });

    return siteObj;
  } catch (error) {

    console.log('error occurred during getSiteByName ' + error);
  }
}

async function getSite(siteId) {

  try {

    let siteObj = await Site.findOne({
      _id: siteId
    });

    return siteObj;
  } catch (error) {

    console.log('error occurred during getSite ' + error);
    return null;
  }
}

async function getSiteNames() {

  try {

    var siteNamesObj = await Site.find()
      .select('siteName');

    return siteNamesObj;
  } catch (error) {

    console.log('error occurred during getSiteNames ' + error);
  }
}

async function validateSiteId(req, response) {

  let siteObj = await getSite(req.payload.siteId);

  if (siteObj == null) {

    await invalidSiteId(response);
  }
}

async function invalidSiteId(response) {
  response.success = false;
  response.message = 'Error';
  response.description = 'Invalid Site Id.';
}

async function isValidQRCode(qrCodes, qrCode) {

  if (qrCodes != null && qrCodes.length > 0 && qrCode != null)

    for (const qrObj of qrCodes) {

      if (qrObj != null) {

        if (qrObj.contents == qrCode.contents) {

          return true;
        }
      }
    }

  return false;
}

async function populateSiteVisit(siteVisit, req) {

  siteVisit.visitedUserId = req.payload.visitedUserId;
  siteVisit.siteId = req.payload.siteId;
  siteVisit.timeIn = req.payload.timeIn;
  siteVisit.timeOut = req.payload.timeOut;
}

async function populateSiteInfo(site, req) {

  site.siteName = req.payload.siteName;
  site.address = req.payload.address;
  site.jobScope = req.payload.jobScope;
  site.shiftDetails = req.payload.shiftDetails;
  site.equipmentDetails = req.payload.equipmentDetails;
  site.stationaryDetails = req.payload.stationaryDetails;
  site.clockingDetails = req.payload.clockingDetails;
  site.contract = req.payload.contract;
}

module.exports = {
  saveSite: saveSite,
  getSiteByName: getSiteByName,
  getSite: getSite,
  getSiteNames: getSiteNames,
  invalidSiteId: invalidSiteId,
  validateSiteId: validateSiteId,
  isValidQRCode: isValidQRCode,
  populateSiteVisit: populateSiteVisit,
  populateSiteInfo: populateSiteInfo
}