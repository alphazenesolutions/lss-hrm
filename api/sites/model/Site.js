'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const siteModel = new Schema({
  userId: { type: String, required: true },
  userUpdated: { type: String, required: false },

  siteName: { type: String, required: true },
  jobScope: { type: Array, required: false },
  jobScopeDocId: { type: String, required: false },
  shiftDetails: { type: Array, required: false },
  address: { type: String, required: false },
  clockingDetails: {
    type: {
      noOfCheckPoints: { type: Number, required: false },
      noOfRounds: { type: Number, required: false },
      planTime: { type: Array, required: false },
    }, required: false
  },
  equipmentDetails: { type: Array, required: false },
  stationaryDetails: { type: Array, required: false },
  contract: {
    type: {
      siteNotificationDate: { type: Date, required: false },
      managementContact: { type: String, required: false },
      fccContact: { type: Array, required: false },
    }, required: false
  },
  
  dateCreated: { type: Date, required: true },
  dateUpdated: { type: Date, required: false }
});

module.exports = mongoose.model('Sites', siteModel);