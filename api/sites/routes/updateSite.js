'use strict';

const Site = require('../model/Site');
const updateSiteSchema = require('../schemas/updateSiteSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const getSite = require('../util/siteFunctions').getSite;
const populateSiteInfo = require('../util/siteFunctions').populateSiteInfo;
const successUpdateResponse = require('../../commons/util/commonFunctions').successUpdateResponse;
const uploadFile = require('../../commons/util/fileFunctions').uploadFile;
const constants = require('../../../constants');
const update = require('../../commons/util/collectionFunctions').update;
const _ = require('lodash');

module.exports = {
  method: 'POST',
  path: '/api/v1/site/update',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    payload: constants.MULTIPART,
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      var response = {
        'success': true,
        'time': new Date()
      }

      let siteObj = await getSite(req.payload.siteId);

      if (siteObj == null || _.isEmpty(siteObj)) {

        await objectNotFound(response, constants.SITE_NAME);
        return h.response(response).code(200);
      }
      siteObj.userUpdated = userObj._id;
      siteObj.dateUpdated = new Date();

      await populateSiteInfo(siteObj, req);

      let document = req.payload['jobScopeDocument']; // accept a field call document
      if (document != null) {

        let fileId = await uploadFile(document, siteObj, constants.FOLDER_SITES);
        siteObj.jobScopeDocId = fileId;
      }

      await update(Site, siteObj);

      await successUpdateResponse(response, constants.SITE);
      return h.response(response).code(200);

    },
    tags: ['api'],
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: updateSiteSchema
    }
  }
}