'use strict';

const Site = require('../model/Site');
const addSiteSchema = require('../schemas/addSiteSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const populateSiteInfo = require('../util/siteFunctions').populateSiteInfo;
const getSiteByName = require('../util/siteFunctions').getSiteByName;
const successResponse = require('../../commons/util/commonFunctions').successResponse;
const constants = require('../../../constants');
const uploadFile = require('../../commons/util/fileFunctions').uploadFile;
const create = require('../../commons/util/collectionFunctions').create;
const update = require('../../commons/util/collectionFunctions').update;

module.exports = {
  method: 'POST',
  path: '/api/v1/site/add',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    payload: constants.MULTIPART,
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      var response = {
        'success': true,
        'time': new Date()
      }

      let siteNameObj = await getSiteByName(req.payload.siteName);

      if (siteNameObj != null) {
        await duplicateResponse(response);
        return h.response(response).code(200);
      }

      let site = new Site();
      site.userId = userObj._id;
      site.dateCreated = new Date();

      await populateSiteInfo(site, req);

      site = await create(Site, site);

      const document = req.payload['jobScopeDocument']; // accept a field call document
      let fileId = await uploadFile(document, site, constants.FOLDER_SITES);
      site.jobScopeDocId = fileId;

      await update(Site, site);

      await successResponse(response, constants.SITE_NAME, site);
      return h.response(response).code(200);

    },
    tags: ['api'],
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: addSiteSchema
    }
  }
}

async function duplicateResponse(response) {
  response.success = false;
  response.message = 'Error';
  response.description = 'Duplicate site name.';
}