'use strict';

const Joi = require('joi');
const Readable = require('stream').Readable

const updateSiteSchema = Joi.object({
  siteId: Joi.string().required(),
  siteName: Joi.string().required(),
  address: Joi.string().allow('', null),
  jobScope: Joi.array().allow('', null),
  jobScopeDocument: Joi.object().type(Readable),
  shiftDetails: Joi.array().allow('', null),
  clockingDetails: Joi.object({
    noOfCheckPoints: Joi.number().allow('', null),
    noOfRounds: Joi.number().allow('', null),
    planTime: Joi.array().allow('', null)
  }).allow('', null),
  equipmentDetails: Joi.array().allow('', null),
  stationaryDetails: Joi.array().allow('', null),
  contract: Joi.object({
    siteNotificationDate: Joi.date().allow('', null),
    managementContact: Joi.string().allow('', null),
    fccContact: Joi.array().allow('', null)
  }).allow('', null)
}).options({ allowUnknown: true });

module.exports = updateSiteSchema;