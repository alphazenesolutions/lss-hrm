'use strict';
const getDataByFilter = require('../../commons/util/collectionFunctions').getDataByFilter;
const getUsers = require('../../users/util/userFunctions').getUsers;
const create = require('../../commons/util/collectionFunctions').create;
const sendNotification = require('../../notification/util/notificationFunctions').sendNotification;
const constants = require('../../../constants');

async function populateTraining(training, req) {

  training.title = req.payload.title;
  training.details = req.payload.details;
  training.location = req.payload.location;
  training.fromDate = req.payload.fromDate;
  training.toDate = req.payload.toDate;
  training.empSelection = req.payload.empSelection;
  training.selectedEmployees = req.payload.selectedEmployees;
}

async function sendTrainingNotification(userObj, training, empSelection, selectedEmployees) {
  if (empSelection == 'All Employees') {
      let users = await getUsers();
      for (const user of users) {

          let notificationData = {
              "title": constants.TRAINING,
              "id": training._id,
              "notifyUserId": user._id
          };
          await sendNotification(userObj, training, notificationData);
      }
  }
  else{
      for (const employeeId of selectedEmployees) {

          let notificationData = {
              "title": constants.TRAINING,
              "id": training._id,
              "notifyUserId": employeeId
          };
          await sendNotification(userObj, training, notificationData);
      }
  }
}

module.exports = {
  populateTraining: populateTraining,
  sendTrainingNotification: sendTrainingNotification
}