'use strict';

const importDataSchema = require('../schemas/importDataSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const importOccurrenceBook = require('../../oe-forms/util/occurrenceBookFunctions').importOccurrenceBook;
const importAttendance = require('../../users/util/attendanceFunctions').importAttendance;
const importClocking = require('../../clocking/util/clockingFunctions').importClocking;
const constants = require('../../../constants');
const csv = require("fast-csv");
const Site = require('../../sites/model/Site');
const getById = require('../../commons/util/collectionFunctions').getById;
const getClockingPoints = require('../../clocking/util/clockingFunctions').getClockingPoints;

module.exports = {
  method: 'POST',
  path: '/api/v1/import/data',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    payload: constants.MULTIPART,
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;

      var response = {
        'success': true,
        'time': new Date(),
        'data': 'imported successfully'
      }

      let importFile = req.payload['importFile'];
      let importType = req.payload.importType;
      let siteId = req.payload.siteId;
      let siteObj = null;

      if (siteId != null) {

        siteObj = await getById(Site, siteId)
      }

      await importData(importFile, importType, userObj, siteObj);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: importDataSchema
    }
  }
}

async function importData(importFile, importType, userObj, siteObj) {

  let clockingData = [];
  let noOfPoints = 0;
  let metaData = {};
  let qrId = null;
  let clockingPoints = null;

  if (siteObj != null) {
    clockingPoints = await getClockingPoints(siteObj._id);
  }

  var parser = await csv
    .fromStream(importFile, { headers: false })
    .on("data", async function (row) {

      parser.pause();
      if (!(row[0] == 'Site' || row[0] == 'S.No')) {

        if (importType == 'OCCURRENCE') {

          await importOccurrenceBook(row);
        } else if (importType == 'ATTENDANCE') {

          await importAttendance(row, userObj);
        } else if (importType == 'CLOCKING') {

          if (row[1]) {

            metaData = row;
            noOfPoints = parseInt(row[1]);
            clockingData.length = 0;
          }

          qrId = await getQRCode(clockingPoints, row[5]);
          clockingData.push({ "scanned": true, "qrId": qrId, "dateTime": new Date(row[6]) });

          if (noOfPoints == clockingData.length) {
            await importClocking(metaData, clockingData, siteObj);
          }
        }
      }
      parser.resume();
    })
    .on("end", async function () {
      console.log(importType + " exported");
    });

}

async function getQRCode(clockingPoints, qrContent) {
  let result = null;

  for (let clockingPoint of clockingPoints) {
    if (clockingPoint.qrData.contents.includes(qrContent)) {
      result = clockingPoint._id;
    }
  }
  return result;

}