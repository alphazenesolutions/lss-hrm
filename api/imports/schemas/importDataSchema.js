'use strict';

const Joi = require('joi');
const Readable = require('stream').Readable

const importDataSchema = Joi.object({

    importFile: Joi.object().type(Readable),
    importType: Joi.string().required(),
    siteId: Joi.string().allow('', null),
    timeZone: Joi.string().allow('', null)
});

module.exports = importDataSchema;