'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const base64Model = new Schema({
  fileName: { type: String, required: true },
  mimeType: { type: String, required: true },
  content: { type: String, required: true },
  dateCreated: { type: Date, required: true }
});

module.exports = mongoose.model('Base64Image', base64Model);