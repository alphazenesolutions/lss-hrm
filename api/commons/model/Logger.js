'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const loggerModel = new Schema({
  className: { type: String, required: true },
  level: { type: String, required: true },
  message: { type: String, required: true },
  dateCreated: { type: Date, required: true }
});

module.exports = mongoose.model('Logger', loggerModel);