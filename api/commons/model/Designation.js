'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const designationModel = new Schema({
  code: { type: String, required: true },
  description: { type: String, required: true }
});

module.exports = mongoose.model('Designation', designationModel);