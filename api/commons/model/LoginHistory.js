'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const loginHistoryModel = new Schema({
  userName: { type: String, required: true },
  siteId: { type: String, required: false },
  images: { type: Array, required: false },
  dateCreated: { type: Date, required: true } 
});

module.exports = mongoose.model('LoginHistory', loginHistoryModel);