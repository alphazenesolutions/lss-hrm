'use strict';

const Designation = require('../model/Designation');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const successGetResponse = require('../util/commonFunctions').successGetResponse;
const getDataByFilter = require('../../commons/util/collectionFunctions').getDataByFilter;

module.exports = {
  method: 'GET',
  path: '/api/v1/designation/lookup',
  config: {
    auth: false,
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var response = {
        'success': true,
        'time': new Date()
      }

      let result = await getDataByFilter(Designation, {});
      
      await successGetResponse(response, result);
      return h.response(response).code(200);

    },
    tags: ['api'],
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
    }
  }
}
