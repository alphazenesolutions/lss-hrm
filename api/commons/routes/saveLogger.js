'use strict';

const loggerSchema = require('../schemas/loggerSchema');
const Logger = require('../model/Logger');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const create = require('../util/collectionFunctions').create;
const successResponse = require('../util/commonFunctions').successResponse;
const constants = require('../../../constants');

module.exports = {
  method: 'POST',
  path: '/api/v1/logger',
  config: {
    tags: ['api'],
    handler: async (req, h) => {
      
      let response = {
        'success': true,
        'time': new Date()
      };
      
      console.log(req.payload.className + ' ' + req.payload.level + ' ' + req.payload.message);
      
      let logger = await populateLogger(req);
      logger = await create(Logger, logger);
      await successResponse(response, constants.LOGGER, logger);
      
      return h.response(response).code(200);
    },
    validate: {
      headers: checkCommonHeadersSchema,
      payload: loggerSchema
    },
    auth: false
  }
}

async function populateLogger(req) {

  let logger = new Logger();

  logger.className = req.payload.className;
  logger.level = req.payload.level;
  logger.message = req.payload.message;
  logger.dateCreated = new Date();

  return logger;
}
