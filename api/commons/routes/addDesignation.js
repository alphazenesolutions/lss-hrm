'use strict';

const Designation = require('../model/Designation');
const addDesignationSchema = require('../schemas/addDesignationSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const successResponse = require('../util/commonFunctions').successResponse;
const constants = require('../../../constants');
const getDataByFilter = require('../../commons/util/collectionFunctions').getDataByFilter;
const create = require('../../commons/util/collectionFunctions').create;
const _ = require('lodash');

module.exports = {
  method: 'POST',
  path: '/api/v1/designation/add',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      var response = {
        'success': true,
        'time': new Date()
      }

      let queryFilter = await populateQueryFilter(req);
      let data = await getDataByFilter(Designation, queryFilter);

      if (!_.isEmpty(data)) {
        await duplicateResponse(response);
        return h.response(response).code(200);
      }

      let designation = new Designation();
      designation.code = req.payload.code;
      designation.description = req.payload.description;
      designation = await create(Designation, designation);

      await successResponse(response, constants.DESIGNATION, designation);
      return h.response(response).code(200);

    },
    tags: ['api'],
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: addDesignationSchema
    }
  }
}

async function populateQueryFilter(req) {

  let filter = {
    code: req.payload.code
  }

  return filter;
}

async function duplicateResponse(response) {
  response.success = false;
  response.message = 'Error';
  response.description = 'Duplicate designation code.';
}