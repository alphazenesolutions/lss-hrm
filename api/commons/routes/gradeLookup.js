'use strict';

const Grade = require('../model/Grade');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const successGetResponse = require('../util/commonFunctions').successGetResponse;
const getDataByFilter = require('../util/collectionFunctions').getDataByFilter;

module.exports = {
  method: 'GET',
  path: '/api/v1/grade/lookup',
  config: {
    auth: false,
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var response = {
        'success': true,
        'time': new Date()
      }

      let result = await getDataByFilter(Grade, {});
      
      await successGetResponse(response, result);
      return h.response(response).code(200);

    },
    tags: ['api'],
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
    }
  }
}
