'use strict';

const getInprogressAttendance = require('../../users/util/attendanceFunctions').getInprogressAttendance;
const Attendance = require('../../users/model/Attendance');
const constants = require('../../../constants');
const update = require('../../commons/util/collectionFunctions').update;
const getById = require('../../commons/util/collectionFunctions').getById;
const getUserByUserId = require('../../users/util/userFunctions').getUserByUserId;
const getOEandHRUsers = require('../../users/util/userFunctions').getOEandHRUsers;
const populateNotification = require('../../notification/util/notificationFunctions').populateNotification;
const saveNotification = require('../../notification/util/notificationFunctions').saveNotification;

module.exports = {
    method: 'GET',
    path: '/api/v1/overtimeNotification',
    config: {
        handler: async (req, h) => {

            let response = {
                'success': true,
                'localtime': new Date(),
                'data': 'job successful'
            };

            try {

                var attendanceData = await getInprogressAttendance();

                await generateNotifications(attendanceData);

            } catch (error) {
                console.log('error occurred : ', error);
            }

            return h.response(response).code(200);
        },
        auth: false
    }
}

async function generateNotifications(attendanceData) {
    let oeHRUsers = await getOEandHRUsers();
    let notificationData = {
        "title": constants.OVER_TIME,
        "id": "",
        "notifyUserId": ""
    };

    for (const attendance of attendanceData) {

        var hours = getHoursWorked(attendance);

        if (hours > 12) {

            await sendNotificationToOEs(attendance, oeHRUsers, notificationData);
            await markAttendanceNotified(attendance);
        }
    }
}

function getHoursWorked(attendance) {
    var startTime = attendance.startTime;
    var currentTime = new Date();
    var hours = Math.abs(startTime - currentTime) / 36e5;
    return hours;
}

async function sendNotificationToOEs(attendance, oeHRUsers, notificationData) {
    let user = await getUserByUserId(attendance.employeeId);
    let notification = null;

    if (user.role == 'SO') {
        for (const oeHRUser of oeHRUsers) {
            notificationData.notifyUserId = oeHRUser._id;
            notificationData.id = attendance._id;
            notification = await populateNotification(user, attendance, false, notificationData);
            notification.description = user.firstName + ' ' + user.lastName + ' have crossed 12 hours in the duty';
            notification.metaData = { id: attendance._id, siteId: attendance.siteId };

            await saveNotification(notification);
        }

    }
}

async function markAttendanceNotified(attendance) {
    let attendanceObj = await getById(Attendance, attendance._id);
    
    if (attendanceObj != null) {

        attendanceObj.notified = true;
        await update(Attendance, attendanceObj);
    }
}