'use strict';
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getBase64Image = require('../util/base64ImageFunctions').getBase64Image;
const successGetResponse = require('../../commons/util/commonFunctions').successGetResponse;

module.exports = {
  method: 'GET',
  path: '/api/v1/image/{id}',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    handler: async (req, h) => {

      var response = {
        'success': true,
        'time': new Date()
      };

      let image = await getBase64Image(req.params.id);

      await successGetResponse(response, image);

      return h.response(response).code(200);
    },
    validate: {
      headers: checkCommonHeadersSchema
    },
  }
}