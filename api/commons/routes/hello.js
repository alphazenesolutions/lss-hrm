'use strict';

const sendEmail = require('../util/emailFunctions').sendEmail;
var moment = require('moment-timezone');

module.exports = {
    method: 'GET',
    path: '/api/v1/hello',
    config: {
        tags: ['api'],
        handler: async (req, h) => {

            let response = {
                'success': true,
                'localtime': new Date(),
                'data': 'hello world'
            };

            return h.response(response).code(200);
        },
        auth: false
    }
}