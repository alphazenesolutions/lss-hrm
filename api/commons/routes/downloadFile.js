'use strict';
const FileMetaData = require('../model/FileMetaData');
const getById = require('../../commons/util/collectionFunctions').getById;

module.exports = {
  method: 'GET',
  path: '/api/v1/download/{id}',
  config: {
    auth: false,
    tags: ['api'],
    handler: async (req, h) => {

      var fileMetadata = await getById(FileMetaData, req.params.id);

      return h.file(fileMetadata.filePath, {
        confine: false, filename: fileMetadata.fileName
      })
        .header('Content-Disposition: attachment')
        .header('filename: ' + fileMetadata.fileName);
    }
  }
}