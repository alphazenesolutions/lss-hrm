'use strict';

const Grade = require('../model/Grade');
const addGradeSchema = require('../schemas/addGradeSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const constants = require('../../../constants');
const create = require('../util/collectionFunctions').create;
const update = require('../../commons/util/collectionFunctions').update;
const deleteDocument = require('../../commons/util/collectionFunctions').deleteDocument;
const _ = require('lodash');
const getById = require('../../commons/util/collectionFunctions').getById;
const getDataByFilter = require('../util/collectionFunctions').getDataByFilter;

module.exports = {
  method: 'POST',
  path: '/api/v1/grade/add',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var response = {
        'success': true,
        'time': new Date()
      }

      let grades = await getDataByFilter(Grade, {});

      let isExistingGrade = false;
      for (const grade of grades) {
        for (const payloadGrade of req.payload.gradeData) {
          if (payloadGrade._id!=null && grade._id == payloadGrade._id) {
            isExistingGrade = true;
          }
        }
        if (!isExistingGrade) {

          await deleteDocument(Grade, grade._id);
        }
        isExistingGrade = false;
      }

      for (const grade of req.payload.gradeData) {
        let gradeCollection = null;

        if (grade._id != null) {

          gradeCollection = await getById(Grade, grade._id);

          if (gradeCollection == null || _.isEmpty(gradeCollection)) {

            await objectNotFound(response, constants.GRADE);
            return h.response(response).code(200);
          }

          await populateGrade(gradeCollection, grade);
          await update(Grade, gradeCollection);

        }
        else {
          gradeCollection = new Grade();
          await populateGrade(gradeCollection, grade);
          await create(Grade, gradeCollection);
        }
      }

      await successResponse(response, constants.GRADE);
      return h.response(response).code(200);

    },
    tags: ['api'],
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: addGradeSchema
    }
  }
}

async function successResponse(response, title) {
  response.success = true;
  response.message = 'Success';
  response.description = title + ' saved successfully.';
}

function populateGrade(gradeCollection, grade) {
  gradeCollection.grade = grade.grade;
  gradeCollection.jobDescription = grade.jobDescription;
}