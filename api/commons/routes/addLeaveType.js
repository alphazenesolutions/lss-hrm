'use strict';

const LeaveType = require('../model/LeaveType');
const addLeaveTypeSchema = require('../schemas/addLeaveTypeSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const successResponse = require('../util/commonFunctions').successResponse;
const constants = require('../../../constants');
const getDataByFilter = require('../util/collectionFunctions').getDataByFilter;
const create = require('../util/collectionFunctions').create;
const _ = require('lodash');

module.exports = {
  method: 'POST',
  path: '/api/v1/leaveType/add',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      var response = {
        'success': true,
        'time': new Date()
      }

      let queryFilter = await populateQueryFilter(req);
      let data = await getDataByFilter(LeaveType, queryFilter);

      if (!_.isEmpty(data)) {
        await duplicateResponse(response);
        return h.response(response).code(200);
      }

      let leaveType = new LeaveType();
      leaveType.code = req.payload.code;
      leaveType.description = req.payload.description;
      leaveType = await create(LeaveType, leaveType);

      await successResponse(response, constants.LEAVE_TYPE, leaveType);
      return h.response(response).code(200);

    },
    tags: ['api'],
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: addLeaveTypeSchema
    }
  }
}

async function populateQueryFilter(req) {

  let filter = {
    code: req.payload.code
  }

  return filter;
}

async function duplicateResponse(response) {
  response.success = false;
  response.message = 'Error';
  response.description = 'Duplicate leaveType  code.';
}