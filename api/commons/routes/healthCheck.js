'use strict';

const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const sendEmail = require('../../commons/util/emailFunctions').sendEmail;
var moment = require('moment-timezone');

module.exports = {
    method: 'GET',
    path: '/api/v1/health',
    config: {
        tags: ['api'],
        handler: async (req, h) => {
            let datetime = moment().tz("Asia/Singapore").format();

            let response = {
                'success': true,
                'time': datetime,
                'localtime': new Date()
            };

            console.log('sending email .. ');
            await sendEmail('achchayya@gmail.com','dumzuservices@gmail.com', 'Test Email', 'Health Check');
            console.log('sent email .. ');

            return h.response(response).code(200);
        },
        validate: {
            headers: checkCommonHeadersSchema
        },
        auth: false
    }
}