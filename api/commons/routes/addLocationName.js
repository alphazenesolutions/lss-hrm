'use strict';

const Location = require('../model/Location');
const locationNameSchema = require('../schemas/locationNameSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const saveLocationName = require('../util/locationFunctions').saveLocationName;
const getLocationByName = require('../util/locationFunctions').getLocationByName;
const successResponse = require('../../commons/util/commonFunctions').successResponse;
const constants = require('../../../constants');

module.exports = {
  method: 'POST',
  path: '/api/v1/location/add',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      var response = {
        'success': true,
        'time': new Date()
      }
      
      let locationNameObj = await getLocationByName(req.payload.locationName);
      
      if(locationNameObj!=null){
        duplicateResponse(response);
        return h.response(response).code(200);
      }
      
      let location = new Location();
      await populateLocationInfo(location, userObj, req);

      location = await saveLocationName(location);
      
      await successResponse(response, constants.LOCATION_NAME, location);
      return h.response(response).code(200);

    },
    tags: ['api'],
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: locationNameSchema
    }
  }
}

async function populateLocationInfo(location, userObj, req) {
  location.userId = userObj._id;
  location.dateCreated = new Date();
  location.locationName = req.payload.locationName;
}

async function duplicateResponse(response) {
  response.success = false;
  response.message = 'Error';
  response.description = 'Duplicate location name.';
}