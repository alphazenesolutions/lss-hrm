'use strict';

var moment = require('moment-timezone');

function currentDate() {

  return moment().tz("Asia/Singapore").format();
}

async function getStartTime(dateTime, offSetMins) {

  if (offSetMins != null) {

    let isoDate = await getIsoDate(dateTime, offSetMins);
    return isoDate;
  }

  return dateTime;
}

async function getEndTime(dateTime, offSetMins) {

  if (offSetMins != null) {

    let isoDate = await getIsoDate(dateTime, offSetMins);
    return isoDate;
  }

  return dateTime;
}

async function getIsoDate(dateTime, offSetMins) {

  let isoDate = null;
  var MS_PER_MINUTE = 60000;

  if (parseInt(offSetMins) < parseInt(0)) {

    isoDate = new Date(dateTime.getTime() - Math.abs(offSetMins) * MS_PER_MINUTE);
  } else {

    isoDate = new Date(dateTime.getTime() + Math.abs(offSetMins) * MS_PER_MINUTE);
  }

  return isoDate;
}

async function getStartEndTime(dateTime, offSetMins) {

  let startDate = dateTime;
  startDate.setHours(0, 0, 0, 0);

  let isoStartDate = await getIsoDate(startDate, offSetMins);

  let endDate = new Date(dateTime);
  endDate.setHours(23, 59, 59, 999);

  let isoEndDate = await getIsoDate(endDate, offSetMins);

  let result = {
    startDate: startDate,
    endDate: endDate
  };

  if (offSetMins != null) {
    result.startDate = isoStartDate;
    result.endDate = isoEndDate;
  }
  return result;
}

module.exports = {
  currentDate: currentDate,
  getStartEndTime: getStartEndTime,
  getStartTime: getStartTime,
  getEndTime: getEndTime,
  getIsoDate: getIsoDate
}