'use strict';

const updateUser = require('../../users/util/userFunctions').updateUser;
const update = require('../../commons/util/collectionFunctions').update;
const fileTypeNotSupported = require('../../users/util/userFunctions').fileTypeNotSupported;
const mongoose = require('mongoose');
const Thurston = require('thurston');
const options = { whitelist: ['image/png', 'image/jpeg'] };
const _ = require('lodash');

// upload to mongodb
async function uploadProfileImage(profileImage, user) {

  if (profileImage) {

    try {
      //instantiate mongoose-gridfs
      var gridfs = require('mongoose-gridfs')({
        collection: 'profile',
        model: 'Profile',
        mongooseConnection: mongoose.connection
      });

      //create or save a file
      var fileId = null;

      await gridfs.write({
        filename: profileImage.hapi.filename,
        contentType: profileImage.hapi.headers['content-type']
      },
        profileImage,
        async function (error, createdFile) {
          if (error) {

            console.log('error : ', error);
          }

          if (createdFile != null) {

            fileId = createdFile['_id'];
            user.profileImage = fileId;
            await updateUser(user);
          }
        });
      return fileId;
    } catch (error) {

      console.log('error occurred during writing the file : ', error);
    }
  }
}

// upload to mongodb
async function uploadDocument(document, Collection, collection) {

  if (document) {

    try {
      //instantiate mongoose-gridfs
      var gridfs = require('mongoose-gridfs')({
        collection: 'document',
        model: 'Document',
        mongooseConnection: mongoose.connection
      });

      //create or save a file
      var fileId = null;

      await gridfs.write({
        filename: document.hapi.filename,
        contentType: document.hapi.headers['content-type']
      },
        document,
        async function (error, createdFile) {
          if (error) {

            console.log('error : ', error);
          }

          if (createdFile != null) {

            fileId = createdFile['_id'];
            collection.fileId = fileId;
            await update(Collection, collection);
          }
        });
      return fileId;
    } catch (error) {

      console.log('error occurred during writing the file : ', error);
    }
  }
}

async function uploadImages(req, collectionObj, Collection) {

  const images = req.payload['images'];
  
  if (images != null) {

    let fileIds = [];

    try {
      //instantiate mongoose-gridfs
      let gridfs = require('mongoose-gridfs')({
        collection: 'image',
        model: 'Image',
        mongooseConnection: mongoose.connection
      });

      //create or save a file

      if (Array.isArray(images)) {

        let promises = [];

        _.forEach(images, async function (image) {
          promises.push(uploadFile(gridfs, image, collectionObj, Collection));
        });

        await Promise.all(promises);
      } else {

        uploadFile(gridfs, images, collectionObj, Collection);
      }

    } catch (error) {
      console.log('error occurred during writing the file : ', error);
    }
    return fileIds;
  }
}

async function uploadFile(gridfs, image, collectionObj, Collection) {

  await gridfs.write({
    filename: image.hapi.filename,
    contentType: image.hapi.headers['content-type']
  }, image, async function (error, createdFile) {
    if (error) {
      console.log('error : ', error);
    }
    if (createdFile != null) {
      let fileId = createdFile['_id'];
      if (collectionObj.images) {
        collectionObj.images.push(fileId);
      } else {
        let images = [];
        images.push(fileId);

        collectionObj.images = images;
      }
      updateCollection(Collection, collectionObj);
    }
  });
}

async function validateImages(req, response) {

  const images = req.payload['images'];

  if (images != null) {

    let validFiles = await validImages(req);

    if (!validFiles) {
      console.log('fileTypeNotSupported called ');

      await fileTypeNotSupported(response);
    }
  }
}

async function validImages(req) {
  const images = req.payload['images'];
  let validImages = true;

  try {

    if (Array.isArray(images)) {

      let promises = [];

      _.forEach(images, async function (image) {
        promises.push(Thurston.validate({ file: image }, options));
      });

      try {
        await Promise.all(promises);
      } catch (error) {
        validImages = false;

        return validImages;
      }

    } else {
      await Thurston.validate({ file: images }, options);
    }
  } catch (error) {
    validImages = false;

    return validImages;
  }

  return validImages;
}

// deprecated
async function updateCollection(Collection, collectionObj) {

  await Collection
    .findOneAndUpdate({ _id: collectionObj.id }, collectionObj);
}

module.exports = {
  uploadProfileImage: uploadProfileImage,
  validImages: validImages,
  validateImages: validateImages,
  uploadImages: uploadImages,
  updateCollection: updateCollection,
  uploadDocument: uploadDocument
}