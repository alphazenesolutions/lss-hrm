'use strict';

const _ = require('lodash');
const Boom = require('boom');

async function createDocument(Collection, collectionObj) {

  var result;

  await Collection.create(collectionObj).then((savedObj) => {
    result = savedObj;
  }).catch((err) => {
    console.log('Exception occurred during the create operation: ' + err);
    return Boom.badImplementation('Exception occurred during the create operation: ', err);
  });

  return result;
}

async function updateDocument(Collection, collectionObj) {

  await Collection
    .findOneAndUpdate({ _id: collectionObj._id }, collectionObj);
}

async function deleteDocument(Collection, id) {
  await Collection.remove({_id: id});
}

async function getDocumentById(Collection, id) {

  try {

    let collectionObj = await Collection.findOne({
      _id: id
    });

    return collectionObj;
  } catch (error) {

    console.log('error occurred during getById ' + error);
    return null;
  }
}

async function getDataByFilter(Collection, query) {

  try {

    let collectionObj = await Collection.find(query).select('-__v');

    return collectionObj;
  } catch (error) {

    console.log('error occurred during getDataByFilter ' + error);
    return null;
  }
}

module.exports = {
  create: createDocument,
  getById: getDocumentById,
  update: updateDocument,
  deleteDocument: deleteDocument,
  getDataByFilter: getDataByFilter
}