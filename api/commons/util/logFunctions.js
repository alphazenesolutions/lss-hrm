'use strict';

const Logger = require('../model/Logger');
const Boom = require('boom');

async function saveLog(logObj) {

  var result;

  await Logger.create(logObj).then((savedObj) => {
    result = savedObj;
  }).catch((err) => {
    console.log('error occurred while saving Logger.. ' + err);
    return Boom.badImplementation('Error', err);
  });

  return result;
}

module.exports = {
  saveLog: saveLog
}