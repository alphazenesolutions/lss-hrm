"use strict";

const fs = require('fs');
const { google } = require('googleapis');
const util = require('util');
// Convert fs.readFile into Promise version of same    
const readFile = util.promisify(fs.readFile);

// If modifying these scopes, delete token.json.
var SCOPES = [
    'https://mail.google.com/',
    'https://www.googleapis.com/auth/gmail.modify',
    'https://www.googleapis.com/auth/gmail.compose',
    'https://www.googleapis.com/auth/gmail.send'
];
// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.
const TOKEN_PATH = 'token.json';

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
async function authorize(credentials) {
    const { client_secret, client_id, redirect_uris } = credentials.installed;
    const oAuth2Client = new google.auth.OAuth2(
        client_id, client_secret, redirect_uris[0]);

    let tokenContent = await readFileAsync(TOKEN_PATH);

    if (tokenContent != null) {
        oAuth2Client.setCredentials(JSON.parse(tokenContent));
        return oAuth2Client;
    } else {
        return null;
    }
}

async function makeBody(to, from, subject, message) {
    var str = ["Content-Type: text/plain; charset=\"UTF-8\"\n",
        "MIME-Version: 1.0\n",
        "Content-Transfer-Encoding: 7bit\n",
        "to: ", to, "\n",
        "from: ", from, "\n",
        "subject: ", subject, "\n\n",
        message
    ].join('');

    var encodedMail = new Buffer(str).toString("base64").replace(/\+/g, '-').replace(/\//g, '_');
    return encodedMail;
}

async function readFileAsync(filePath) {
    let content = null;
    try {
        content = await readFile(filePath);
    } catch (error) {

        console.log('error occurred during readFile : ', error);
    }
    return content;
}

async function sendEmail(to, from, subject, message) {

    // Load client secrets from a local file.
    let content = await readFileAsync('/opt/lss/livesensorsecurity.app/credentials.json');
    if (content != null) {

        let auth = await authorize(JSON.parse(content));

        if (auth != null) {

            const gmail = await google.gmail({ version: 'v1', auth });
            var raw = await makeBody(to, subject, message);

            await gmail.users.messages.send({
                auth: auth,
                userId: 'me',
                resource: {
                    raw: raw
                }
            }, function (err, response) {

                if (err) {

                    console.log('err : ', err);
                }
                else {
                    console.log('email sent successfully.');
                }
            });
        }
    }
}

module.exports = {
    sendEmail: sendEmail
};
