'use strict';

const Location = require('../model/Location');
const Boom = require('boom');

async function saveLocationName(locationObj) {

  var result;

  await Location.create(locationObj).then((savedObj) => {
    result = savedObj;
  }).catch((err) => {
    console.log('error occurred while saving location name.. ' + err);
    return Boom.badImplementation('Error', err);
  });

  return result;
}

async function getLocation(locationId) {

  try {

    var locationObj = await Location.findOne({
      _id: locationId
    });

    return locationObj;
  } catch (error) {

    console.log('error occurred during getLocation ' + error);
  }
}

async function getLocationByName(locationName) {

  try {

    var locationObj = await Location.findOne({
      locationName: locationName
    });

    return locationObj;
  } catch (error) {

    console.log('error occurred during getLocationByName ' + error);
  }
}

async function getLocationNames() {

  try {

    var locationNamesObj = await Location.find()
      .select('locationName');

    return locationNamesObj;
  } catch (error) {

    console.log('error occurred during getLocationNames ' + error);
  }
}

module.exports = {
  saveLocationName: saveLocationName,
  getLocation: getLocation,
  getLocationByName: getLocationByName,
  getLocationNames: getLocationNames
}