'use strict';

const admin = require("firebase-admin");

function sendFirebaseMessage(registrationToken, message) {

  // See the "Defining the message payload" section below for details
  // on how to define a message payload.
  var payload = {
    data: {
      message: message
    }
  };
  
  console.log('firebaseToken: '+registrationToken);
  console.log('payload: '+JSON.stringify(payload));
  
  // Send a message to the device corresponding to the provided
  // registration token.
  admin.messaging().sendToDevice(registrationToken, payload)
    .then(function (response) {
      // See the MessagingDevicesResponse reference documentation for
      // the contents of response.
    })
    .catch(function (error) {
      console.log("Error sending message:", error);
    });
}

module.exports = {
  sendFirebaseMessage: sendFirebaseMessage
}
