'use strict';

const updateUser = require('../../users/util/userFunctions').updateUser;
const mongoose = require('mongoose');
const LoginHistory = require('../model/LoginHistory');
const Boom = require('boom');

// upload to mongodb
async function loginHistoryEntry(user, location) {

  var loginHistory = new LoginHistory();
  loginHistory.userName = user.userName;
  loginHistory.dateCreated = new Date();
  loginHistory.location = location;
  loginHistory = await saveLoginHistory(loginHistory);
  return loginHistory;
}

async function saveLoginHistory(loginHistory) {

  var userResult;
  await LoginHistory.create(loginHistory).then((savedObj) => {
    userResult = savedObj;
  }).catch((err) => {
    console.log('error occurred while saving saveLoginHistory.. ' + err);
    return Boom.badImplementation('Error', err);
  });

  return userResult;
}

async function updateLoginHistory(loginHistory) {

  await LoginHistory
    .findOneAndUpdate({ _id: loginHistory.id }, loginHistory);
}

module.exports = {
  loginHistoryEntry: loginHistoryEntry
}