'use strict';

const fs = require('fs');
const FileMetaData = require('../model/FileMetaData');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const create = require('../../commons/util/collectionFunctions').create;

async function uploadFile(file, collectionObj, folder) {
  if (file != null) {
    var path = '/opt/uploads/' + folder + '/';
    //var path = 'd://uploads//' + folder + '//';
    var fileName = collectionObj._id + '_' + file.hapi.filename;
    var filePath = path + fileName;

    // save the file
    await uploader(file, filePath);

    let fileMetaData = new FileMetaData();
    fileMetaData.dateCreated = new Date();
    fileMetaData.fileName = file.hapi.filename;
    fileMetaData.filePath = filePath;

    fileMetaData = await create(FileMetaData, fileMetaData);

    return  fileMetaData._id;
  }
}

async function uploader(image, fileName) {
  if (image) {

    await image.pipe(fs.createWriteStream(fileName));
  }
}

async function getFile(fileId) {

  var gridfs = require('mongoose-gridfs')({
    collection: 'document',
    model: 'Document',
    mongooseConnection: mongoose.connection
  });

  var file = await gridfs.readById(fileId);
  return file;
}

async function getFileMetadata(fileId) {

  var metadataSchema = new Schema({}, { strict: false });
  var Document = mongoose.model("Document", metadataSchema, "document.files");

  let metadata = await Document.findOne({ _id: fileId });
  return metadata;
}

module.exports = {
  uploadFile: uploadFile,
  getFile: getFile,
  getFileMetadata: getFileMetadata
}