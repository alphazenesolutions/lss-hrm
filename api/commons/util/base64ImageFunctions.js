'use strict';

const Base64Image = require('../model/Base64Image');
const updateCollection = require('./uploadFunctions').updateCollection;
const Boom = require('boom');

async function saveBase64Image(base64ImageObj) {

  var result;

  await Base64Image.create(base64ImageObj).then((savedObj) => {
    result = savedObj;
  }).catch((err) => {
    console.log('error occurred while saving Base64Image.. ' + err);
    return Boom.badImplementation('Error', err);
  });

  return result;
}

async function getBase64Image(base64ImageId) {

  try {

    let base64ImageObj = await Base64Image.findOne({
      _id: base64ImageId
    });

    return base64ImageObj;
  } catch (error) {

    console.log('error occurred during getBase64Image ' + error);
    return null;
  }
}

async function getStream(imageData) {

  var imageBase64 = imageData.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");

  var fileBuffer = new Buffer(imageBase64, "base64");

  fs.writeFile("./sweet.jpg", fileBuffer, function (error) {
    if (error) {
      console.log(error);
    } else {
      console.log('WOOHOOOO!!!');
    }
  });
}

async function uploadBase64Images(images, collectionObj, Collection) {

  if (images != null && Array.isArray(images)) {

    for (const image of images) {

      let base64Image = await populateBase64Image(image);
      let savedObj = await saveBase64Image(base64Image);
      let imageMetaData = {
        fileName: image.fileName,
        mimeType: image.mimeType,
        id: savedObj._id
      };

      if (collectionObj.images) {

        collectionObj.images.push(imageMetaData);
      } else {

        let images = [];
        images.push(imageMetaData);
        collectionObj.images = images;
      }

      await updateCollection(Collection, collectionObj);
    }
  }
}

async function uploadBase64Image(image) {

  let base64Image = await populateBase64Image(image);
  let savedObj = await saveBase64Image(base64Image);

  let imageMetaData = {
    fileName: image.fileName,
    mimeType: image.mimeType,
    id: savedObj._id
  };

  return imageMetaData;
}

async function populateBase64Image(image) {

  let base64Image = new Base64Image();

  base64Image.fileName = image.fileName;
  base64Image.mimeType = image.mimeType;
  base64Image.content = image.content;
  base64Image.dateCreated = new Date();

  return base64Image;
}

module.exports = {
  saveBase64Image: saveBase64Image,
  getBase64Image: getBase64Image,
  uploadBase64Image: uploadBase64Image,
  uploadBase64Images: uploadBase64Images
}