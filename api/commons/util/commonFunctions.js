'use strict';

async function getCollectionDataById(collectionName, id) {

  try {

    let collectionObj = await collectionName.findOne({
      _id: id
    });

    return collectionObj;
  } catch (error) {

    console.log('error occurred during getSite ' + error);
    return null;
  }
}


async function updateCollection(Collection, collectionObj) {
  
  await Collection
  .findOneAndUpdate({ _id: collectionObj.id }, collectionObj);
}

async function successResponse(response, title, collectionObj) {
  response.id = collectionObj._id;
  response.success = true;
  response.message = 'Success';
  response.description = title + ' saved successfully.';
}

async function successGetResponse(response, data) {
  response.success = true;
  response.message = 'Success';
  response.data = data;
}

async function successUpdateResponse(response, title) {
  response.success = true;
  response.message = 'Success';
  response.description = title + ' updated successfully.';
}

async function successDeleteResponse(response, title) {
  response.success = true;
  response.message = 'Success';
  response.description = title + ' deleted successfully.';
}

async function objectNotFound(response, title) {
  response.success = false;
  response.message = 'Failed';
  response.description = title + ' not found.';
}

module.exports = {
  getCollectionDataById: getCollectionDataById,
  updateCollection: updateCollection,
  objectNotFound: objectNotFound, 
  successResponse: successResponse,
  successGetResponse: successGetResponse,
  successDeleteResponse: successDeleteResponse,
  successUpdateResponse: successUpdateResponse
}