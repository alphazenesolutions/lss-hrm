'use strict';

const Joi = require('joi');

const addDesignationSchema = Joi.object({
  code: Joi.string().required(),
  description: Joi.string().allow('', null)
});

module.exports = addDesignationSchema;