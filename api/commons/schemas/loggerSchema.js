'use strict';

const Joi = require('joi');

const siteNameSchema = Joi.object({
  className: Joi.string().required(),
  level: Joi.string().required(),
  message: Joi.string().required()
});

module.exports = siteNameSchema;