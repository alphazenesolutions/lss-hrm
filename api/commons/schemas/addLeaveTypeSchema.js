'use strict';

const Joi = require('joi');

const addLeaveTypeSchema = Joi.object({
  code: Joi.string().required(),
  description: Joi.string().allow('', null)
});

module.exports = addLeaveTypeSchema;