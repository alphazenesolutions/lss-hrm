'use strict';

const Joi = require('joi');


const gradeSchema = Joi.object({
    _id: Joi.string().allow('', null),
    grade: Joi.string().required(),
    jobDescription: Joi.string().required()
});

const addGradeSchema = Joi.object({
    gradeData: Joi.array().items(gradeSchema).required(),
});

module.exports = addGradeSchema;