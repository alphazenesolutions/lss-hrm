'use strict';

const Joi = require('joi');

const locationSchema = Joi.object({
  locationName: Joi.string().required()
});

module.exports = locationSchema;