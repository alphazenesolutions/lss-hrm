'use strict';

const Joi = require('joi');

const saveFeedbackSchema = Joi.object({
  feedback: Joi.string().required().error(new Error(
    "Feedback is required."
)),
  isWarning: Joi.boolean().required().error(new Error(
    "IsWarning is required."
)),
  employeeId: Joi.string().required().error(new Error(
    "Employee Id is required."
)),
});

module.exports = saveFeedbackSchema;