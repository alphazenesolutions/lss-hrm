'use strict';

const Feedback = require('../model/Feedback');
const getUserById = require('../../users/util/userFunctions').getUserById;
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getById = require('../../commons/util/collectionFunctions').getById;

module.exports = {
  method: 'GET',
  path: '/api/v1/feedback/{id}',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    // Before the route handler runs, verify that the user is unique
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      try {

        var user = req.pre.user;
        var response = {
          time: new Date(),
          'data': {}
        }

        var feedback = await getById(Feedback, req.params.id);
        console.log(feedback);
        
        response.success = true;
        response.data = feedback;

        return h.response(response).code(200);

      } catch (err) {
        console.log('Exception occurred in getNotifications' + err);
      }
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema
    }
  }
}