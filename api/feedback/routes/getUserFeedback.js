'use strict';

const getUserById = require('../../users/util/userFunctions').getUserById;
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getDataByFilter = require('../../commons/util/collectionFunctions').getDataByFilter;
const successGetResponse = require('../../commons/util/commonFunctions').successGetResponse;
const Feedback = require('../model/Feedback');

module.exports = {
  method: 'GET',
  path: '/api/v1/user/feedback/{id}',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    // Before the route handler runs, verify that the user is unique
    pre: [
      { method: getUserById, assign: 'user' }
    ],  
    handler: async (req, h) => {

      try {
        
        var response = {
          time: new Date()
        }

        let queryFilter = await populateQueryFilter(req);
        let data = await getDataByFilter(Feedback, queryFilter);
  
        await successGetResponse(response, data);
  
        return h.response(response).code(200);
   
      } catch (err) {
        console.log('Exception occurred in get user Feedback' + err);
      }
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema
    }
  }
}

async function populateQueryFilter(req) {

  let filter = {
    employeeId:  req.params.id  
  };

  return filter;
}