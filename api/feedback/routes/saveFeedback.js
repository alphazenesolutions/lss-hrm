'use strict';

const Feedback = require('../model/Feedback');
const getUserById = require('../../users/util/userFunctions').getUserById;
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const saveFeedbackSchema = require('../schemas/saveFeedbackSchema');
const successResponse = require('../../commons/util/commonFunctions').successResponse;
const constants = require('../../../constants');
const create = require('../../commons/util/collectionFunctions').create;
const sendNotification = require('../../notification/util/notificationFunctions').sendNotification;

module.exports = {
  method: 'POST',
  path: '/api/v1/feedback/save',
  config: {
    auth: {
      strategy: 'jwt'
    },
    // Before the route handler runs, verify that the user is unique
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      try {

        var userObj = req.pre.user;
        var response = {
          time: new Date()
        }

        let feedback = new Feedback();
        await populateFeedback(req, feedback);

        feedback = await create(Feedback, feedback);

        await successResponse(response, constants.FEEDBACK, feedback);

        let title = null;
        if (req.payload.isWarning) {
          title = constants.MANAGEMENT_WARNING;
        } else {
          title = constants.MANAGEMENT_FEEDBACK
        }

        let notificationData = {
          "title": title,
          "id": feedback._id,
          "notifyUserId": feedback.employeeId
        };

        await sendNotification(userObj, feedback, notificationData);

        return h.response(response).code(200);

      } catch (err) {
        console.log('Exception occurred in saveFeedback' + err);
      }
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: saveFeedbackSchema
    }
  }
}

async function populateFeedback(req, feedback) {
  feedback.feedback = req.payload.feedback;
  feedback.isWarning = req.payload.isWarning;
  feedback.employeeId = req.payload.employeeId;
  feedback.userId = req.pre.user._id;
  feedback.dateCreated = new Date();
}