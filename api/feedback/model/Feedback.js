'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const feedbackModel = new Schema({
  userId: { type: String, required: true },
  employeeId: { type: String, required: true },
  feedback: { type: String, required: true },
  isWarning: { type: Boolean, required: false },
  dateCreated: { type: Date, required: true },
  dateUpdated: { type: Date, required: false }
});

module.exports = mongoose.model('Feedback', feedbackModel);