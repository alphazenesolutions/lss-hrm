'use strict';

var express = require('express'),
	app = express(),
	http = require('http'),
	server = http.createServer(app),
	io = require('socket.io').listen(server),
	usernames = {},
	rooms = ['room1', 'room2', 'room3'],
	_ = require('lodash'),
	util = require('util'),
	constants = require('./constants'),
	transService = require('./service/index');

io.sockets.on('connection', function (socket) {

	// when the client emits 'adduser', this listens and executes
	socket.on('adduser', function (data) {
		console.log('add user data : ', data);
		var dataArr = data.split(',');

		var userName = dataArr[0],
			organisation = dataArr[1];

		// store the username in the socket session for this client
		socket.username = userName;

		// add the client's username to the global list
		usernames[userName] = userName;
		// send client to room 1

		/*if (_.get(constants.ROOMS, username) instanceof Array) {
			_.forEach(_.get(constants.ROOMS, username), function (value) {
				socket.join(value);
			});
		} else {
			socket.join(_.get(constants.ROOMS, username));
		}*/

		transService.getVehicleRoom(userName, organisation).then(function (vehicleRoom) {

			console.log('user joined to ' + vehicleRoom);
			// store the room name in the socket session for this client
			socket.room = vehicleRoom;

			socket.join(vehicleRoom);

			// echo to client they've connected
			socket.emit('updatechat', 'SERVER', 'you have connected to ' + vehicleRoom);
			// echo to room 1 that a person has connected to their room
			socket.broadcast.to(vehicleRoom).emit('updatechat', 'SERVER', userName + ' has connected to this room');
			socket.emit('updaterooms', rooms, vehicleRoom);

		});

	});

	// when the client emits 'sendchat', this listens and executes
	socket.on('sendchat', function (data) {
		// we tell the client to execute 'updatechat' with 2 parameters
		util.log(' user ' + socket.username + ' data : ' + data);
		io.sockets.in(socket.room).emit('updatechat', socket.username, data);
	});

	socket.on('switchRoom', function (newroom) {
		socket.leave(socket.room);
		socket.join(newroom);
		socket.emit('updatechat', 'SERVER', 'you have connected to ' + newroom);
		// sent message to OLD room
		socket.broadcast.to(socket.room).emit('updatechat', 'SERVER', socket.username + ' has left this room');
		// update socket session room title
		socket.room = newroom;
		socket.broadcast.to(newroom).emit('updatechat', 'SERVER', socket.username + ' has joined this room');
		socket.emit('updaterooms', rooms, newroom);
	});


	// when the user disconnects.. perform this
	socket.on('disconnect', function () {
		// remove the username from global usernames list
		delete usernames[socket.username];
		// update list of users in chat, client-side
		io.sockets.emit('updateusers', usernames);
		// echo globally that this client has left
		socket.broadcast.emit('updatechat', 'SERVER', socket.username + ' has disconnected');
		socket.leave(socket.room);
	});
});

module.exports = io;
