'use strict';

const rn = require('random-number');

async function generate() {
  var options = {
    min: 100000,
    max: 999999,
    integer: true
  };
  var randomNum = await rn(options);
  return randomNum;
}

module.exports = {
  generate: generate
}
