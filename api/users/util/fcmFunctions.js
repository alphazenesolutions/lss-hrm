'use strict';

const Boom = require('boom');
const FCMToken = require('../model/FCMToken');

async function saveFcmToken(fcmToken) {

  var result;

  await FCMToken.create(fcmToken).then((fcmObj) => {
    result = fcmObj;
  }).catch((err) => {
    console.log('error occurred while saving fcm token.. ' + err);
    return Boom.badImplementation('Error', err);
  });

  return result;
}

async function getFcmToken(fcmTokenId) {

  var fcmToken = await FCMToken.findOne({
    fcmToken: fcmTokenId
  });

  return fcmToken;
}

async function getFcmTokenByUser(userId) {

  let fcmToken = {};

  fcmToken = await FCMToken.findOne({
    userId: userId
  }).select('fcmToken');

  return fcmToken;
}

async function deleteFcmTokensByUser(userId) {

  await FCMToken.remove({
    userId: userId
  });
}

async function updateFcmToken(fcmToken) {
  await FCMToken
    .findOneAndUpdate({ _id: fcmToken.id }, fcmToken);
}

async function duplicateToken(response) {
  response.success = false;
  response.message = 'Duplicate Token';
  response.description = 'The FCM token registered already.';
}

module.exports = {
  saveFcmToken: saveFcmToken,
  getFcmToken: getFcmToken,
  updateFcmToken: updateFcmToken,
  duplicateToken: duplicateToken,
  getFcmTokenByUser: getFcmTokenByUser,
  deleteFcmTokensByUser: deleteFcmTokensByUser
}