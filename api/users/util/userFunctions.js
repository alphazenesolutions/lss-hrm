'use strict';

const Boom = require('boom');
const User = require('../model/User');
const WorkLog = require('../model/WorkLog');
const Attendance = require('../model/Attendance');
const BPromise = require('bluebird');
const _ = require('lodash');
const constants = require('../../../constants');
const bcrypt = require('bcryptjs');

async function verifyUniqueUser(req, h) {
  // Find an entry from the database that
  // matches username
  var user = await User.findOne({ userName: req.payload.userName });
  // Check whether the username
  // is already taken and error out if so
  if (user) {
    if (user.userName && user.userName === req.payload.userName) {
      return Boom.badRequest('User Name is in use');
    }
  }
  // If everything checks out, send the payload through
  // to the route handler
  return h.response(req.payload);
}

async function getUserByUserName(req) {
  var user = await User.findOne(
    { userName: req.payload.userName }
  );
  return user;
}

async function getUserFromUserName(userName) {
  var user = await User.findOne(
    { userName: userName }
  );
  return user;
}

async function getUserById(req) {

  let user = await getUserByUserId(req.params.userId);
  return user;
}

async function getUserByUserId(userId) {

  let user = await User.findOne({ _id: userId })
    .select('-password -__v -attempts');
  return user;
}

async function getUsers() {

  var users = await User
    .find()
    // Deselect the password and version fields
    .select('-password -__v -attempts -token');
  return users;
}

async function getOEandSOs() {

  var users = await User
    .find({ role: { '$in': ['SO', 'OE'] } })
    // Deselect the password and version fields
    .select('-password -__v -attempts -token');
  return users;
}

async function getOEUsers() {

  var users = await User
    .find({ role: 'OE' })
    // Deselect the password and version fields
    .select('-password -__v -attempts -token');
  return users;
}

async function getHRUsers() {

  var users = await User
    .find({ role: 'HR' })
    // Deselect the password and version fields
    .select('-password -__v -attempts -token');
  return users;
}

async function getOEandHRUsers() {

  var users = await User
    .find({ role: { '$in': ['OE', 'HR'] } })
    // Deselect the password and version fields
    .select('-password -__v -attempts -token');
  return users;
}

async function getUserByRefCode(refCode) {

  var user = await User.findOne({ refCode: refCode });
  return user;
}

async function userNotFound(response) {
  response.success = false;
  response.message = 'User Not Found';
  response.description = 'Invalid Token.';
}

async function internalError(response) {
  response.success = false;
  response.message = 'Internal Error';
  response.description = 'Something is not right here.';
}

async function verifyUserExistance(req, h) {
  // Find an entry from the database that
  var user = await User.findOne({
    phone: req.payload.phone
  });
  // Check whether the username,email or phone
  // is already taken and error out if so
  if (!user) {
    return Boom.badRequest('Username is in use');
  }
  // If everything checks out, send the payload through
  // to the route handler
  h.response(req.payload);
}

async function updateUser(user) {

  await User
    .findOneAndUpdate({ _id: user.id }, user);
}

function getFriendsPhoneNos(phone) {
  return new BPromise(function (resolve, reject) {

    User.findOne({
      phone: phone
    }, (err, user) => {

      if (err) {
        console.error('error occurred ', err);
        reject(err);
      }
      else if (!user) {
        console.error('User Not found ' + err);
        resolve(Boom.badRequest('User Not found'));
      }
      else {
        var result = new Array();
        if (user && user.friends) {
          _.forEach(user.friends, function (value, key) {
            if (value) {
              result.push(value.phone);
            }
          });
        }
        resolve(result);
      }
    });
  });
}

async function saveUser(req, user) {

  var userResult;

  await User.create(user).then((userObj) => {
    userResult = userObj;
  }).catch((err) => {
    console.log('error occurred while saving user.. ' + err);
    return Boom.badImplementation('Error', err);
  });

  return userResult;
}

async function fileTypeNotSupported(response) {
  response.success = false;
  response.message = 'File type not supported';
  response.description = 'File type is not supported. Please use jpeg/png';
}

async function populateUser(user, req) {
  user.userName = req.payload.userName;
  user.role = req.payload.role;
  user.designation = req.payload.designation;
  user.staffId = req.payload.staffId;
  user.firstName = req.payload.firstName;
  user.lastName = req.payload.lastName;
  user.dateCreated = new Date();
  user.admin = false;
  user.status = "Active";
  user.phone = req.payload.phone;

  // fields existing in current route but not used in web-app 
  // TODO : need to remove fields which are of no use in mobile app as well as web app
  user.reportingManager = req.payload.reportingManager;
  user.workStatus = '';
  user.attempts = 0;

  // fields not available in current route but used in web-app. Hence, added.
  user.address = req.payload.address;
  user.dateOfBirth = req.payload.dateOfBirth;
  user.dateOfJoining = req.payload.dateOfJoining;
  user.dateOfConfirmation = req.payload.dateOfConfirmation;
  user.pwmGrade = req.payload.pwmGrade;
  user.deploymentGrade = req.payload.deploymentGrade;
  user.courses = req.payload.courses;
  user.gender = req.payload.gender;
  user.wpIssuedDate = req.payload.wpIssuedDate;
  user.wpExpiryDate = req.payload.wpExpiryDate;
  user.notificationAddDate = req.payload.notificationAddDate;
  user.notificationCancelDate = req.payload.notificationCancelDate;
  user.maritalStatus = req.payload.maritalStatus;
  user.age = req.payload.age;
  user.bankAccount = req.payload.bankAccount;
  user.nationality = req.payload.nationality;
  user.nok = req.payload.nok;
  user.resignationDate = req.payload.resignationDate;
  user.terminationDate = req.payload.terminationDate;
  user.uniformIssueStatus = req.payload.uniformIssueStatus;
  user.employeeManagement = req.payload.employeeManagement;
}

async function hashPassword(password) {
  // Generate a salt at level 10 strength
  const salt = await bcrypt.genSalt(10);
  const hash = await bcrypt.hash(password, salt);
  return hash;
}

module.exports = {
  verifyUniqueUser: verifyUniqueUser,
  getUserById: getUserById,
  getUserByUserName: getUserByUserName,
  getUserFromUserName: getUserFromUserName,
  updateUser: updateUser,
  verifyUserExistance: verifyUserExistance,
  getFriendsPhoneNos: getFriendsPhoneNos,
  userNotFound: userNotFound,
  internalError: internalError,
  getUserByRefCode: getUserByRefCode,
  getUserByUserId: getUserByUserId,
  getUsers: getUsers,
  getHRUsers: getHRUsers,
  fileTypeNotSupported: fileTypeNotSupported,
  saveUser: saveUser,
  getOEandSOs: getOEandSOs,
  populateUser: populateUser,
  getOEUsers: getOEUsers,
  getOEandHRUsers: getOEandHRUsers,
  hashPassword: hashPassword
}
