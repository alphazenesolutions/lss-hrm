'use strict';

const Attendance = require('../model/Attendance');
const getUserFromUserName = require('../../users/util/userFunctions').getUserFromUserName;
const getSiteByName = require('../../sites/util/siteFunctions').getSiteByName;
const create = require('../../commons/util/collectionFunctions').create;

async function getCurrentAttendance(employeeId) {

    var attendance = await Attendance.findOne({
        employeeId: employeeId,
        status: 'INPROGRESS'
    });
    return attendance;
}

async function getInprogressAttendance() {

    var attendance = await Attendance.find({
        status: 'INPROGRESS',
        notified: false
    });
    return attendance;
}

async function getEmployeeAttendanceHistory(employeeId, fromDate, toDate) {

    var attendance = await Attendance.find({
        employeeId: employeeId,
        status: 'COMPLETED',
        dateCreated: { "$gte": fromDate, "$lt": toDate }
    });

    return attendance;
}

async function getAttendance(siteId, fromDate, toDate) {

    let query = { dateCreated: { '$gte': fromDate, '$lte': toDate } };

    if (siteId != null) {
        query.siteId = siteId;
    }

    let attendance = await Attendance
        .find(query);

    return attendance;
}


async function importAttendance(row) {
    
    if (row[0] != null && row[1].trim() != null) {

        let siteObj = await getSiteByName(row[0].trim());

        let userId = row[1].trim();
        userId = userId.toString().replace(/\"/g, "");
    
        let employeeObj = await getUserFromUserName(userId.trim());
    
        if (employeeObj != null) {

            let attendance = await populateAttendance(row, employeeObj, siteObj);
            await create(Attendance, attendance);
        }
    }
}

async function populateAttendance(row, employeeObj, siteObj) {

    let attendance = new Attendance();

    attendance.userId = employeeObj._id;
    attendance.employeeId = employeeObj._id;
    attendance.dateCreated = new Date(row[2]);
    attendance.startTime = new Date(row[2]);
    attendance.siteId = siteObj._id;
    attendance.endTime = new Date(row[3]);
    if (row.length > 4 && row[4] != null) {

        attendance.breakTime = new Date(row[4]);
    }
    if (row.length > 5 && row[5] != null) {
        attendance.resumeTime = new Date(row[5]);
    }
    attendance.status = "COMPLETED";

    return attendance;
}

module.exports = {
    getAttendance: getAttendance,
    getCurrentAttendance: getCurrentAttendance,
    getEmployeeAttendanceHistory: getEmployeeAttendanceHistory,
    importAttendance: importAttendance,
    getInprogressAttendance: getInprogressAttendance
}
