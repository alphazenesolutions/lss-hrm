'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userModel = new Schema({
  userId: { type: String, required: false },
  dateCreated: { type: Date, required: false },
  modifiedUserId: { type: String, required: false },
  modifiedDate: { type: Date, required: false },

  userName: { type: String, required: false, index: { unique: true } },
  password: { type: String, required: true },
  role: { type: String, required: true },
  staffId: { type: String, required: true },
  designation: { type: String, required: true },
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  phone: { type: String, required: false },
  profileImage: { type: String, required: false },
  token: { type: String, required: false },

  // fields existing in current model but not used in web-app 
  // TODO : need to remove fields which are of no use in mobile app as well as web app
  nricNo: { type: String, required: false },
  workStatus: { type: String, required: false },
  attempts: { type: Number, required: false },
  admin: { type: Boolean, required: false },
  active: { type: Boolean, required: false },
  address1: { type: String, required: false },
  address2: { type: String, required: false },
  city: { type: String, required: false },
  region: { type: String, required: false },
  zip: { type: String, required: false },
  country: { type: String, required: false },
  reportingManager: { type: String, required: false },
  fileId: { type: String, required: false },

  // fields not available in current model but used in web-app. Hence, added.
  address: { type: String, required: false },
  dateOfBirth: { type: Date, required: false },
  dateOfJoining: { type: Date, required: false },
  dateOfConfirmation: { type: Date, required: false },
  pwmGrade: { type: String, required: false },
  deploymentGrade: { type: String, required: false },
  courses: { type: Array, required: false },
  gender: { type: String, required: false },
  wpIssuedDate: { type: Date, required: false },
  wpExpiryDate: { type: Date, required: false },
  notificationAddDate: { type: Date, required: false },
  notificationCancelDate: { type: Date, required: false },
  maritalStatus: { type: String, required: false },
  age: { type: String, required: false },
  bankAccount: { type: String, required: false },
  nationality: { type: String, required: false },
  nok: { type: String, required: false },
  resignationDate: { type: Date, required: false },
  terminationDate: { type: Date, required: false },
  contractOfEmployment: { type: String, required: false },
  uniformIssueStatus: { type: String, required: false },
  ojtStatus: { type: String, required: false },
  status: { type: String, required: false },
  trainings: { type: Array, required: false },
  employeeManagement: {
    type: {
      warningHistory: { type: Array, required: false },
      lastWarning: { type: String, required: false }
    }, required: false
  }

});

module.exports = mongoose.model('User', userModel);