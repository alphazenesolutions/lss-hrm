'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const workLogModel = new Schema({
  userId: { type: String, required: true },
  siteId: { type: String, required: false },
  workStatus: { type: String, required: true },
  images: { type: Array, required: false },
  dateCreated: { type: Date, required: true }
});

module.exports = mongoose.model('WorkLog', workLogModel);