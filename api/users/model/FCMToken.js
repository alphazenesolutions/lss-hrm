'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const fcmTokenModel = new Schema({
  fcmToken: { type: String, required: true },
  userId: { type: String, required: true },
  dateCreated: { type: Date, required: true }
});

module.exports = mongoose.model('FCMToken', fcmTokenModel);