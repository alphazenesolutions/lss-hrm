'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const attendanceModel = new Schema({
  userId: { type: String, required: true },
  employeeId: { type: String, required: true },
  siteId: { type: String, required: true },
  startTime: { type: Date, required: false },
  startImage: { type: Object, required: false },
  breakTime: { type: Date, required: false },
  resumeTime: { type: Date, required: false },
  endTime: { type: Date, required: false },
  endImage: { type: Object, required: false },
  status: { type: String, required: false },
  notified: { type:  Boolean, required: false, default: false },
  dateCreated: { type: Date, required: true }
});

module.exports = mongoose.model('Attendance', attendanceModel);