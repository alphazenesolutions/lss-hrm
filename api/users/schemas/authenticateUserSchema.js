'use strict';

const Joi = require('joi');

const authenticateUserSchema =  Joi.object({
  userName: Joi.string().required(),
  password: Joi.string().required()

}).options({ allowUnknown: true });

module.exports = authenticateUserSchema;