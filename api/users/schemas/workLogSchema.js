'use strict';

const Joi = require('joi');

const workLogSchema = Joi.object({
  siteId: Joi.string().allow('', null),
  employeeId: Joi.string().allow('', null),
  workStatus: Joi.string().required(),
  startTime: Joi.date().allow('', null),
  breakTime: Joi.date().allow('', null),
  resumeTime: Joi.date().allow('', null),
  endTime: Joi.date().allow('', null),
  images: Joi.array().allow('', null)
});

module.exports = workLogSchema;