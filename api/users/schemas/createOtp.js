'use strict';

const Joi = require('joi');

const createOtpSchema = Joi.object({
  email: Joi.string().email().required(),
  otpCode: Joi.string().required(),
  timestamp: Joi.date().required(),
  status: Joi.string().required()
});

module.exports = createOtpSchema;