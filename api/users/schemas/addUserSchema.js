'use strict';

const Joi = require('joi');
const Readable = require('stream').Readable

const createUserSchema = Joi.object({
  userName: Joi.string().required(),
  password: Joi.string().required(),
  firstName: Joi.string().required(),
  lastName: Joi.string().required(),
  role: Joi.string().required(),
  designation: Joi.string().required(),
  staffId: Joi.string().required(),
  phone: Joi.string().allow('', null),
  profileImage: Joi.object().type(Readable),
  contractOfEmployment: Joi.object().type(Readable),
  
  // fields existing in current model but not used in web-app 
  // TODO : need to remove fields which are of no use in mobile app as well as web app 
  address1: Joi.string().allow('', null),
  address2: Joi.string().allow('', null),
  city: Joi.string().allow('', null),
  region: Joi.string().allow('', null),
  zip: Joi.string().allow('', null),
  reportingManager: Joi.string().allow('', null),

  // fields not available in current model but used in web-app. Hence, added.
  address: Joi.string().allow('', null),
  dateOfBirth: Joi.date().allow('', null),
  dateOfJoining: Joi.date().allow('', null),
  dateOfConfirmation: Joi.date().allow('', null),
  pwmGrade: Joi.string().allow('', null),
  deploymentGrade: Joi.string().allow('', null),
  courses: Joi.array().allow('', null),
  gender: Joi.string().allow('', null),
  wpIssuedDate: Joi.date().allow('', null),
  wpExpiryDate: Joi.date().allow('', null),
  notificationAddDate: Joi.date().allow('', null),
  notificationCancelDate: Joi.date().allow('', null),
  maritalStatus: Joi.string().allow('', null),
  age: Joi.string().allow('', null),
  bankAccount: Joi.string().allow('', null),
  nationality: Joi.string().allow('', null),
  nok: Joi.string().allow('', null),
  resignationDate: Joi.date().allow('', null),
  terminationdate: Joi.date().allow('', null),
  uniformIssueStatus: Joi.string().allow('', null), 
});

module.exports = createUserSchema;