'use strict';

const Joi = require('joi');

const checkCommonHeadersSchema = Joi.object({
  'source-system': Joi.string().only(['iPhone', 'Android', 'WebApp']).required(),
  'channel': Joi.string().equal('LssApp').required(),
  'timeZone': Joi.string().allow('', null),
}).options({ allowUnknown: true });

module.exports = checkCommonHeadersSchema;