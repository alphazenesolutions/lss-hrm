'use strict';

const Joi = require('joi');

const attendanceHistorySchema = Joi.object({
  employeeId: Joi.string().required()
});

module.exports = attendanceHistorySchema;