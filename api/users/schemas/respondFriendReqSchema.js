'use strict';

const Joi = require('joi');

const sendRequestSchema = Joi.object({
  userId: Joi.string().required(),
  status: Joi.string().only(['ACCEPTED','REJECTED']).required(),
});

module.exports = sendRequestSchema;