'use strict';

const Joi = require('joi');

const firebaseRegKeySchema = Joi.object({
  username: Joi.string().alphanum().min(2).max(30),
  firebaseregkey: Joi.string().required()
});

module.exports = {
  firebaseRegKeySchema: firebaseRegKeySchema
}