'use strict';

const Joi = require('joi');

const updateFCMTokenSchema = Joi.object({
  'fcmToken': Joi.string().required()
}).options({ allowUnknown: true });

module.exports = updateFCMTokenSchema;