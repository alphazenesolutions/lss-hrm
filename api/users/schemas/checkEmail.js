'use strict';

const Joi = require('joi');

const checkUserNameSchema = Joi.object({
  userName: Joi.string().required()
}).options({ allowUnknown: true });

module.exports = checkUserNameSchema;