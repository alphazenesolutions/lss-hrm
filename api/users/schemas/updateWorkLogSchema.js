'use strict';

const Joi = require('joi');

const updateWorkLogSchema = Joi.object({
    _id: Joi.string().required().error(new Error(
        "Attendance Id is required."
    )),
    siteId: Joi.string().allow('', null),
    employeeId: Joi.string().allow('', null),
    startTime: Joi.date().allow('', null),
    breakTime: Joi.date().allow('', null),
    resumeTime: Joi.date().allow('', null),
    endTime: Joi.date().allow('', null)
});

module.exports = updateWorkLogSchema;