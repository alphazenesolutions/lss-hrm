'use strict';

const Joi = require('joi');

const attendanceReportSchema = Joi.object({
  siteId: Joi.string().allow('', null),
  fromDate: Joi.date().required(),
  toDate: Joi.date().required()
});

module.exports = attendanceReportSchema;