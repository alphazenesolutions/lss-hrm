'use strict';

const Joi = require('joi');

const registerFCMTokenSchema = Joi.object({
  'fcmToken': Joi.string().required()
}).options({ allowUnknown: true });

module.exports = registerFCMTokenSchema;