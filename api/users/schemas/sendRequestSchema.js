'use strict';

const Joi = require('joi');

const sendRequestSchema = Joi.object({
  refCode: Joi.string().required()
});

module.exports = sendRequestSchema;