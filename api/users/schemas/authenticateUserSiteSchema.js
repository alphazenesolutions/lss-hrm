'use strict';

const Joi = require('joi');

const authenticateUserLocationSchema =  Joi.object({
  userName: Joi.string().required(),
  password: Joi.string().required(),
  siteId: Joi.string().allow('', null),
  image: Joi.object().allow('', null)

});

module.exports = authenticateUserLocationSchema;