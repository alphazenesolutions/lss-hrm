'use strict';

const User = require('../model/User');
const updateUserSchema = require('../schemas/updateUserSchema');
const getUserById = require('../util/userFunctions').getUserById;
const hashPassword = require('../util/userFunctions').hashPassword;
const getUserByUserId = require('../util/userFunctions').getUserByUserId;
const fileTypeNotSupported = require('../util/userFunctions').fileTypeNotSupported;
const checkCommonHeadersSchema = require('../schemas/checkCommonHeaders');
const Thurston = require('thurston');
const options = { whitelist: ['image/png', 'image/jpeg'] };
const constants = require('../../../constants');
const successUpdateResponse = require('../../commons/util/commonFunctions').successUpdateResponse;
const populateUser = require('../util/userFunctions').populateUser;
const update = require('../../commons/util/collectionFunctions').update;
const uploadFile = require('../../commons/util/fileFunctions').uploadFile;
const _ = require('lodash');

module.exports = {
  method: 'POST',
  path: '/api/v1/user/update',
  config: {
    auth: {
      strategy: 'jwt'
    },
    payload: constants.MULTIPART,
    // Before the route handler runs, verify that the user is unique
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = await getUserByUserId(req.payload._id);

      var response = {
        'time': new Date()
      }

      if (userObj == null || _.isEmpty(userObj)) {

        await objectNotFound(response, constants.USER);
        return h.response(response).code(200);
      }

      let profileImage = req.payload['profileImage']; // accept a field call profileImage
      if (profileImage != null) {

        try {

          await Thurston.validate({ file: profileImage }, options);
        } catch (error) {
          await fileTypeNotSupported(response);
          return h.response(response).code(200);
        }
      }

      if (profileImage != null) {

        let fileId = await uploadFile(profileImage, userObj, constants.FOLDER_USERS);
        userObj.fileId = fileId;
      }

      const employeeContractDocument = req.payload['contractOfEmployment']; // accept a field call profileImage

      if (employeeContractDocument != null) {

        let contractFileId = await uploadFile(employeeContractDocument, userObj, constants.FOLDER_USERS);
        userObj.contractOfEmployment = contractFileId;
      }

      await populateUser(userObj, req);

      if (req.payload.password != null) {

        userObj.password = await hashPassword(req.payload.password);
      }

      await update(User, userObj);
      await successUpdateResponse(response, constants.USER);
      return h.response(response).code(200);
    },
    tags: ['api'],
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: updateUserSchema
    }
  }
}