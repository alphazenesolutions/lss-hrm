'use strict';

const Boom = require('boom');
const User = require('../model/User');
const firebaseRegKeySchema = require('../schemas/updateUserFirebaseRegKey');

module.exports = {
  method: 'POST',
  path: '/api/users/regkey',
  config: {
    handler: (req, res) => {
      User.findOne({
        username: req.payload.username
      }, (err, user) => {

        if (user) {
          console.log('firebase reg key ', req.payload.firebaseregkey);
          user.firebaseregkey = req.payload.firebaseregkey;

          User
            .findOneAndUpdate({ _id: user._id }, user, (err, user) => {
              if (err) {
                throw Boom.badRequest(err);
              }
              if (!user) {
                throw Boom.notFound('User not found!');
              }
              console.log('Firebase RegKey updated successfully.');
              res({ status: 'Firebase RegKey updated successfully.' }).code(200);
            });
        }
        else {
          res(Boom.badRequest('User Not Found!'));
        }
      });
    },
    validate: {
      payload: firebaseRegKeySchema.firebaseRegKeySchema
    },
    auth: {
      strategy: 'jwt'
    }
  }  
}