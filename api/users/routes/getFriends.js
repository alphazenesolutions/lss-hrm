'use strict';

const userFunctions = require('../util/userFunctions');

module.exports = {
  method: 'GET',
  path: '/api/get/friends/{phone}',
  config: {
    handler: (req, res) => {
        userFunctions.getFriendsPhoneNos(req.params.phone).then(function (data) {
            res(data);
        });
    },
    // Add authentication to this route
    // The user must have a scope of `admin`
    auth: {
      strategy: 'jwt'
    }
  }
}