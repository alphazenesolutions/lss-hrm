'use strict';

const checkEmailSchema = require('../schemas/checkEmail');
const checkCommonHeadersSchema = require('../schemas/checkCommonHeaders');
const getUserByUserName = require('../util/userFunctions').getUserByUserName;

module.exports = {
  method: 'POST',
  path: '/api/v1/validateEmail',
  config: {
    auth: false,
    tags: ['api'],
    pre: [
      { method: getUserByUserName, assign: 'user' }
    ],
    handler: async (req, h) => {
      var user = req.pre.user;
      // Check whether the username,email or phone
      // is already taken and error out if so
      
      var response = {
        'time': new Date(),
        'success': true
      }
      if (user != null && user.userName == req.payload.userName) {
        response.messageKey = 'USER_EXIST';
      }
      else {
        response.messageKey = 'USER_NOT_EXIST';
      }
      return h.response(response);

    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: checkEmailSchema
    }
  }
}
