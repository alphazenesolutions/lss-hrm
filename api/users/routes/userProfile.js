'use strict';

const checkCommonHeadersSchema = require('../schemas/checkCommonHeaders');
const getUserById = require('../util/userFunctions').getUserById;
const _ = require('lodash');
const successGetResponse = require('../../commons/util/commonFunctions').successGetResponse;

module.exports = {
  method: 'GET',
  path: '/api/v1/user/profile',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var user = req.pre.user;

      var response = {
        time: new Date(),
        data: {}
      }

      if (user == null) {

        userNotFound(response);
        return h.response(response);
      }
      else {

        await successGetResponse(response, user);
        return h.response(response);
      }
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema
    }
  }
}

function userNotFound(response) {
  response.success = false;
  response.message = 'Something went wrong.';
  response.description = 'Looks like something went wrong. please try again.';
}