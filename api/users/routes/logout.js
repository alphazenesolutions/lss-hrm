'use strict';

const checkCommonHeadersSchema = require('../schemas/checkCommonHeaders');
const getUserById = require('../util/userFunctions').getUserById;
const createToken = require('../util/token');
const updateUser = require('../util/userFunctions').updateUser;

module.exports = {
  method: 'POST',
  path: '/api/v1/logout',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var response = {
        'time': new Date(),
        'success': true,
        'message': 'Logout Successfully'
      }

      let user = req.pre.user;

      user.token = await createToken(user);
      await updateUser(user);

      return h.response(response);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema
    }
  }
}