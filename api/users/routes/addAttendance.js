'use strict';

const Attendance = require('../model/Attendance');
const workLogSchema = require('../schemas/workLogSchema');
const getUserById = require('../util/userFunctions').getUserById;
const checkCommonHeadersSchema = require('../schemas/checkCommonHeaders');
const constants = require('../../../constants');
const successResponse = require('../../commons/util/commonFunctions').successResponse;
const create = require('../../commons/util/collectionFunctions').create;
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const _ = require('lodash');

module.exports = {
  method: 'POST',
  path: '/api/v1/attendance/add',
  config: {
    auth: {
      strategy: 'jwt'
    },
    // Before the route handler runs, verify that the user is unique
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      var response = {
        'success': true,
        'time': new Date()
      }

      if (req.payload.siteId != null) {

        await validateSiteId(req, response);

        if (!response.success) {
          return h.response(response).code(200);
        }
      }

      let attendance = await populateAttendance(req, userObj);
      attendance = await create(Attendance, attendance);

      await successResponse(response, constants.ATTENDANCE, attendance);
      return h.response(response).code(200);

    },
    tags: ['api'],
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: workLogSchema
    }
  }
}

async function populateAttendance(req, userObj) {

  let attendance = new Attendance();

  attendance.userId = userObj._id;
  attendance.dateCreated = req.payload.startTime;
  attendance.startTime = req.payload.startTime;
  attendance.employeeId = userObj._id;
  attendance.siteId = req.payload.siteId;
  attendance.breakTime = req.payload.breakTime;
  attendance.resumeTime = req.payload.resumeTime;
  attendance.endTime = req.payload.endTime;
  attendance.status = "COMPLETED";

  return attendance;
}