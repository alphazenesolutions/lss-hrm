'use strict';

const getHRUsers = require('../util/userFunctions').getHRUsers;

module.exports = {
  method: 'GET',
  path: '/api/v1/hrProfile',
  config: {
    tags: ['api'],
    auth: {
      strategy: 'jwt'
    },

    handler: async (req, h) => {

      let hrUser = await getHRUsers();

      var response = {
        'time': new Date(),
        'success': true,
        'data': hrUser
      }

      return h.response(response).code(200);
    }
  }
}