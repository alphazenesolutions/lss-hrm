'use strict';

const Boom = require('boom');
const User = require('../model/User');
const getUserById = require('../util/userFunctions').getUserById;
const populateUser = require('../util/userFunctions').populateUser;
const hashPassword = require('../util/userFunctions').hashPassword;
const fileTypeNotSupported = require('../util/userFunctions').fileTypeNotSupported;
const addUserSchema = require('../schemas/addUserSchema');
const getUserByUserName = require('../util/userFunctions').getUserByUserName;
const checkCommonHeadersSchema = require('../schemas/checkCommonHeaders');
const createToken = require('../util/token');
const constants = require('../../../constants');
const successResponse = require('../../commons/util/commonFunctions').successResponse;
const uploadFile = require('../../commons/util/fileFunctions').uploadFile;
const create = require('../../commons/util/collectionFunctions').create;
const update = require('../../commons/util/collectionFunctions').update;
const createVacationBalance = require('../../vacation/util/vacationFunctions').createVacationBalance;
const Thurston = require('thurston');
const options = { whitelist: ['image/png', 'image/jpeg'] };

module.exports = {
  method: 'POST',
  path: '/api/v1/user/add',
  config: {
    auth: false,
    payload: constants.MULTIPART,
    // Before the route handler runs, verify that the user is unique
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var loggedInUser = req.pre.user;
      var userObj = await getUserByUserName(req);
      var response = {
        'success': true,
        'time': new Date()
      }
      let profileImage = null;

      if (userObj != null && userObj.status == 'Active') {

        await duplicateUser(response);
        return h.response(response).code(200);
      }
      else {
        profileImage = req.payload['profileImage']; // accept a field call profileImage
        if (profileImage != null) {

          try {

            await Thurston.validate({ file: profileImage }, options);
          } catch (error) {
            await fileTypeNotSupported(response);
            return h.response(response).code(200);
          }
        }

        let user = new User();
        //user.userId = loggedinUser._id;
        user.password = await hashPassword(req.payload.password);
        user.token = await createToken(user);

        await populateUser(user, req);
        userObj = await create(User, user);

        if (profileImage != null) {

          let fileId = await uploadFile(profileImage, userObj, constants.FOLDER_USERS);
          userObj.fileId = fileId;

          await update(User, userObj);
        }

        const employeeContractDocument = req.payload['contractOfEmployment']; // accept a field call profileImage

        if (employeeContractDocument != null) {

          let contractFileId = await uploadFile(employeeContractDocument, userObj, constants.FOLDER_USERS);
          userObj.contractOfEmployment = contractFileId;

          await update(User, userObj);
        }

        await createVacationBalance(loggedInUser, userObj);

        if (userObj) {

          await successResponse(response, constants.USER, userObj);
          return h.response(response).code(201);

        } else {

          return Boom.badImplementation('Error', 'Something is not right here.');
        }
      }
    },
    tags: ['api'],
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: addUserSchema
    }
  }
}

async function duplicateUser(response) {
  response.success = false;
  response.message = 'Already registered';
  response.description = 'Seems like you are just registered from another app. Please login';
}