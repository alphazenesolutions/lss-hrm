'use strict';

module.exports = {
  method: 'GET',
  path: '/api/v1/user/profile/{profileImage}',
  config: {
    auth: false,
    tags: ['api'],
    handler: async (req, h) => {

      var path = '/opt/uploads/profile/';
      //var path = 'e://uploads//profile//';

      return h.file(path + req.params.profileImage, {
        confine: false
      })
        .header('Content-Disposition: attachment')
        .header('filename: ' + req.params.profileImage);
    },
  }
}