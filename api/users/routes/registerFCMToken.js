'use strict';

const FCMToken = require('../model/FCMToken');
const createFcmTokenSchema = require('../schemas/createFcmTokenSchema');
const checkCommonHeadersSchema = require('../schemas/checkCommonHeaders');
const saveFcmToken = require('../util/fcmFunctions').saveFcmToken;
const getFcmToken = require('../util/fcmFunctions').getFcmToken;
const duplicateToken = require('../util/fcmFunctions').duplicateToken;
const getUserById = require('../util/userFunctions').getUserById;
const deleteFcmTokensByUser = require('../util/fcmFunctions').deleteFcmTokensByUser;
const successResponse = require('../../commons/util/commonFunctions').successResponse;
const constants = require('../../../constants');

module.exports = {
  method: 'POST',
  path: '/api/v1/user/registerFCMToken',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {
      var user = req.pre.user;
      var response = {
        time: new Date()
      }

      var fcmTokenObj = await getFcmToken(req.payload.fcmToken);

      if (fcmTokenObj != null) {

        await duplicateToken(response);
      } else {

        let fcmToken = new FCMToken();
        await populateFcmToken(req, fcmToken);
        await deleteFcmTokensByUser(user._id);
        fcmToken = await saveFcmToken(fcmToken)
        await successResponse(response, constants.FCM_TOKEN, fcmToken);
      }

      return h.response(response);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: createFcmTokenSchema
    }
  }
}

async function populateFcmToken(req, fCMToken) {
  fCMToken.fcmToken = req.payload.fcmToken;
  fCMToken.userId = req.pre.user._id;
  fCMToken.dateCreated = new Date();
}