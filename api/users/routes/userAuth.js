'use strict';

module.exports = {
  method: 'GET',
  path: '/api/userauth',
  config: {
    handler: (req, res) => {
      console.log('authentication success..');
      res('success ');
    },
    // Add authentication to this route
    // The user must have a scope of `admin`
    auth: {
      strategy: 'jwt'
    }
  }
}