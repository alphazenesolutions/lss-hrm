'use strict';

const Attendance = require('../model/Attendance');
const workLogSchema = require('../schemas/workLogSchema');
const getUserById = require('../util/userFunctions').getUserById;
const updateUser = require('../util/userFunctions').updateUser;
const checkCommonHeadersSchema = require('../schemas/checkCommonHeaders');
const constants = require('../../../constants');
const successResponse = require('../../commons/util/commonFunctions').successResponse;
const create = require('../../commons/util/collectionFunctions').create;
const update = require('../../commons/util/collectionFunctions').update;
const uploadBase64Image = require('../../commons/util/base64ImageFunctions').uploadBase64Image;
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const getCurrentAttendance = require('../util/attendanceFunctions').getCurrentAttendance;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const _ = require('lodash');

module.exports = {
  method: 'POST',
  path: '/api/v2/workLog',
  config: {
    auth: {
      strategy: 'jwt'
    },
    // Before the route handler runs, verify that the user is unique
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      var response = {
        'success': true,
        'time': new Date()
      }

      if (req.payload.siteId != null) {

        await validateSiteId(req, response);

        if (!response.success) {
          return h.response(response).code(200);
        }
      }

      const images = req.payload['images'];
      let imageMetadata = null;

      if (images != null && Array.isArray(images)) {
        imageMetadata = await uploadBase64Image(images[0]);
      }

      let attendance = null;

      if (req.payload.workStatus == 'START WORK') {

        attendance = await populateAttendance(req, userObj);
        attendance.startImage = imageMetadata;
        attendance = await create(Attendance, attendance);

      } else {

        attendance = await getCurrentAttendance(req.pre.user._id);
        
        if (attendance == null || _.isEmpty(attendance)) {

          await objectNotFound(response, constants.ATTENDANCE);
          return h.response(response).code(200);
        }

        if (req.payload.workStatus == 'START BREAK') {
          attendance.breakTime = req.payload.breakTime;
        } else if (req.payload.workStatus == 'RESUME WORK') {
          attendance.resumeTime = req.payload.resumeTime;
        } else if (req.payload.workStatus == 'END WORK') {
          attendance.endTime = req.payload.endTime;
          attendance.status = "COMPLETED";
          attendance.endImage = imageMetadata;
        }
        await update(Attendance, attendance);
      }

      userObj.workStatus = req.payload.workStatus;
      await updateUser(userObj);

      await successResponse(response, constants.ATTENDANCE, attendance);
      return h.response(response).code(200);

    },
    tags: ['api'],
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: workLogSchema
    }
  }
}

async function populateAttendance(req, userObj) {

  let attendance = new Attendance();

  attendance.userId = userObj._id;
  attendance.dateCreated = req.payload.startTime;
  attendance.startTime = req.payload.startTime;
  attendance.employeeId = userObj._id;
  attendance.siteId = req.payload.siteId;
  attendance.status = 'INPROGRESS';

  return attendance;
}