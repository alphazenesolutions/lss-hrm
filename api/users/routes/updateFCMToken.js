'use strict';

const updateFcmTokenSchema = require('../schemas/updateFcmTokenSchema');
const checkCommonHeadersSchema = require('../schemas/checkCommonHeaders');
const getFcmToken = require('../util/fcmFunctions').getFcmToken;
const updateFcmToken = require('../util/fcmFunctions').updateFcmToken;
const duplicateToken = require('../util/fcmFunctions').duplicateToken;
const getFcmTokenByUser = require('../util/fcmFunctions').getFcmTokenByUser;
const getUserById = require('../util/userFunctions').getUserById;
const constants = require('../../../constants');
const successUpdateResponse = require('../../commons/util/commonFunctions').successUpdateResponse;

module.exports = {
  method: 'PUT',
  path: '/api/v1/user/updateFCMToken',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {
      var user = req.pre.user;
      var response = {
        time: new Date()
      }

      if (user == null) {

        userNotFound(response);
        return h.response(response);
      }
      else {

        var fcmTokenObj = await getFcmToken(req.payload.fcmToken);
        if (fcmTokenObj != null) {

          await duplicateToken(response);
        } else {
          
          fcmTokenObj = await getFcmTokenByUser(user._id);
          
          if (fcmTokenObj == null) {

            await invalidToken(response);
          } else {

            await populateFcmToken(req, fcmTokenObj);
            await updateFcmToken(fcmTokenObj)
            await successUpdateResponse(response, constants.FCM_TOKEN);
          }
        }

        return h.response(response);
      }
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: updateFcmTokenSchema
    }
  }
}

function userNotFound(response) {
  response.success = false;
  response.message = 'Something went wrong.';
  response.description = 'Looks like something went wrong. please try again.';
}

async function populateFcmToken(req, fCMToken) {
  fCMToken.fcmToken = req.payload.fcmToken;
}

async function invalidToken(response) {
  response.success = false;
  response.message = 'FCM not registered.';
  response.description = 'Please regiser FCM token.';
}