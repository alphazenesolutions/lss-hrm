'use strict';

const attendanceReportSchema = require('../schemas/attendanceReportSchema');
const checkCommonHeadersSchema = require('../schemas/checkCommonHeaders');
const getAttendance = require('../util/attendanceFunctions').getAttendance;
const successGetResponse = require('../../commons/util/commonFunctions').successGetResponse;
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const getStartTime = require('../../commons/util/dateFunctions').getStartTime;
const getEndTime = require('../../commons/util/dateFunctions').getEndTime;

module.exports = {
  method: 'POST',
  path: '/api/v1/attendanceReport',
  config: {
    tags: ['api'],
    auth: {
      strategy: 'jwt'
    },

    handler: async (req, h) => {

      var response = {
        'success': true,
        'time': new Date()
      }

      if (req.payload.siteId != null) {

        await validateSiteId(req, response);

        if (!response.success) {
          return h.response(response).code(200);
        }
      }

      let timeOffset = null;
      if (req.headers.timezone != null) {

        timeOffset = parseInt(req.headers.timezone);
      }

      let startDate = await getStartTime(req.payload.fromDate, timeOffset);
      let endDate = await getEndTime(req.payload.toDate, timeOffset);
    
      var data = await getAttendance(req.payload.siteId, startDate, endDate);
      await successGetResponse(response, data);
      return h.response(response).code(200);

    },
    validate: {
      headers: checkCommonHeadersSchema,
      payload: attendanceReportSchema
    }
  }
}