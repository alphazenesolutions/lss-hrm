'use strict';

const User = require('../model/User');
const checkCommonHeadersSchema = require('../schemas/checkCommonHeaders');
const getUserById = require('../util/userFunctions').getUserById;
const getById = require('../../commons/util/collectionFunctions').getById;
const update = require('../../commons/util/collectionFunctions').update;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const successDeleteResponse = require('../../commons/util/commonFunctions').successDeleteResponse;
const constants = require('../../../constants');
const _ = require('lodash');

module.exports = {
  method: 'DELETE',
  path: '/api/v1/user/delete/{_id}',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var response = {
        'time': new Date(),
        'data': {}
      }

      let userObj = await getById(User, req.params._id);

      if (userObj == null || _.isEmpty(userObj)) {

        await objectNotFound(response, constants.USER);
        return h.response(response).code(200);
      }

      userObj.status = "InActive";
      userObj.userName = userObj.userName + "_Deleted"

      await update(User, userObj);

      await successDeleteResponse(response, constants.USER);
      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema
    }
  }
}