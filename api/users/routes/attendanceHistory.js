'use strict';

const attendanceHistorySchema = require('../schemas/attendanceHistorySchema');
const checkCommonHeadersSchema = require('../schemas/checkCommonHeaders');
const getEmployeeAttendanceHistory = require('../util/attendanceFunctions').getEmployeeAttendanceHistory;
const successGetResponse = require('../../commons/util/commonFunctions').successGetResponse;

module.exports = {
    method: 'POST',
    path: '/api/v1/attendanceHistory',
    config: {
        tags: ['api'],
        auth: {
            strategy: 'jwt'
        },

        handler: async (req, h) => {

            var response = {
                'success': true,
                'time': new Date()
            }

            var currentDate = new Date();
            var firstDay = new Date(currentDate.getFullYear(), currentDate.getMonth() - 1, 1);
            var lastDay = new Date(currentDate.getFullYear(), currentDate.getMonth(), 0);

            var attendanceHistory = await getEmployeeAttendanceHistory(req.payload.employeeId, firstDay, lastDay);
            await successGetResponse(response, attendanceHistory);
            return h.response(response).code(200);
        },
        validate: {
            headers: checkCommonHeadersSchema,
            payload: attendanceHistorySchema
        }
    }
}