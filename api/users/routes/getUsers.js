'use strict';

const getUsers = require('../util/userFunctions').getUsers;

module.exports = {
  method: 'GET',
  path: '/api/v1/users',
  config: {
    tags: ['api'],
    auth: {
      strategy: 'jwt'
    },

    handler: async (req, h) => {

      var users = await getUsers();

      var response = {
        'time': new Date(),
        'success': true,
        'data': users
      }

      return h.response(response).code(200);
    }
  }
}