'use strict';

const authenticateUserSchema = require('../schemas/authenticateUserSchema');
const getUserByUserName = require('../util/userFunctions').getUserByUserName;
const createToken = require('../util/token');
const bcrypt = require('bcryptjs');
const checkCommonHeadersSchema = require('../schemas/checkCommonHeaders');
const updateUser = require('../util/userFunctions').updateUser;
const successGetResponse = require('../../commons/util/commonFunctions').successGetResponse;

module.exports = {
  method: 'POST',
  path: '/api/v1/authenticateUser',
  config: {
    auth: false,
    // Check the user's password against the DB
    pre: [
      { method: getUserByUserName, assign: 'user' }
    ],
    handler: async (req, h) => {

      var user = req.pre.user;
      var password = req.payload.password;

      var response = {
        time: new Date(),
        success: false
      }

      if (user == null) {
        await invalidCredentials(response);
        return h.response(response);
      }

      var isValid = await bcrypt.compare(password, user.password);

      if (isValid === true) {

        if (user.terminationDate != null || user.status == 'InActive') {

          await invalidCredentials(response);
          return h.response(response);
        }

        if (req.pre.user.token == null) {

          let token = await createToken(req.pre.user);
          user.attempts = 0;
          user.token = token;
          await updateUser(user);
        }

        user.password = undefined;
        user.__v = undefined;
        await successGetResponse(response, user);

      } else {

        await invalidCredentials(response);
      }

      return h.response(response).code(200);
    },
    tags: ['api'],
    validate: {
      headers: checkCommonHeadersSchema,
      payload: authenticateUserSchema
    }
  }
}

async function invalidCredentials(response) {
  response.errorCode = 'INVALID_CREDENTIALS';
  response.message = 'Invalid Credentials';
  response.description = 'The username or password you entered is incorrect. Please try again.';
}