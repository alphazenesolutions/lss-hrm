'use strict';

const checkEmailSchema = require('../schemas/checkEmail');
const checkCommonHeadersSchema = require('../schemas/checkCommonHeaders');
const getUserByUserName = require('../util/userFunctions').getUserByUserName;

module.exports = {
  method: 'POST',
  path: '/api/v1/resetPassword',
  config: {
    auth: false,
    tags: ['api'],
    pre: [
      { method: getUserByUserName, assign: 'user' }
    ],
    handler: async (req, h) => {
      var user = req.pre.user;
      var response = {
        'time': new Date()
      };

      if (user && user.emailAddress && user.emailAddress === req.payload.emailAddress) {
        //TODO dispatch reset email link 
        response.success = true;
        response.message = 'Email has sent.';
        response.description = 'The reset password link is sent to your email address. Please proceed to reset.'
      } else {
        response.success = false;
        response.message = 'Email Not exist';
        response.description = 'Please enter the valid email you used to register with Zingo before'
      }

      return h.response(response);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: checkEmailSchema
    }
  }
}
