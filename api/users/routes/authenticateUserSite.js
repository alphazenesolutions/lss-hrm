'use strict';

const authenticateUserSiteSchema = require('../schemas/authenticateUserSiteSchema');
const getUserByUserName = require('../util/userFunctions').getUserByUserName;
const updateUser = require('../util/userFunctions').updateUser;
const LoginHistory = require('../../commons/model/LoginHistory');
const createToken = require('../util/token');
const bcrypt = require('bcryptjs');
const checkCommonHeadersSchema = require('../schemas/checkCommonHeaders');
const uploadBase64Images = require('../../commons/util/base64ImageFunctions').uploadBase64Images;
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const create = require('../../commons/util/collectionFunctions').create;
const successGetResponse = require('../../commons/util/commonFunctions').successGetResponse;
const _ = require('lodash');

module.exports = {
  method: 'POST',
  path: '/api/v1/authenticate',
  config: {
    auth: false,
    // Check the user's password against the DB
    pre: [
      { method: getUserByUserName, assign: 'user' }
    ],
    handler: async (req, h) => {

      var user = req.pre.user;
      var password = req.payload.password;

      var response = {
        time: new Date(),
        success: false
      }

      if (user == null) {
        await invalidCredentials(response);
        return h.response(response);
      }

      var isValid = await bcrypt.compare(password, user.password);

      if (isValid === true && user.status == 'Active') {

        await validateSiteId(req, response);

        let loginHistory = await populateLoginHistory(req);
        loginHistory = await create(LoginHistory, loginHistory);
        await uploadLoginImage(req, loginHistory);

        //TODO remove this code before production 

        if (req.pre.user.token == null) {
          
          let token = await createToken(req.pre.user);
          user.attempts = 0;
          user.token = token;
          await updateUser(user);
        }

        user.password = undefined;
        user.__v = undefined;
        await successGetResponse(response, user);

        return h.response(response).code(200);

      } else {

        var diffMins = await accessedAgo(user);

        if (user.attempts > 3 && diffMins > 1) {
          user.attempts = 1;
          user.timestamp = new Date();
          await updateUser(user);

          await invalidCredentials(response);
          return h.response(response);
        }
        else if (user.attempts < 3) {
          user.attempts = user.attempts + 1;
          user.timestamp = new Date();
          await updateUser(user);

          await invalidCredentials(response);
          return h.response(response);

        } else {
          await accountLocked(response);
          return h.response(response);
        }
      }
    },
    tags: ['api'],
    validate: {
      headers: checkCommonHeadersSchema,
      payload: authenticateUserSiteSchema
    }
  }
}

async function populateLoginHistory(req) {

  let loginHistory = new LoginHistory();
  loginHistory.userName = req.pre.user.userName;
  loginHistory.dateCreated = new Date();
  loginHistory.siteId = req.payload.siteId;

  return loginHistory;

}

async function uploadLoginImage(req, loginHistory) {

  let image = req.payload['image']; // accept a field call image
  if (image != null) {

    let images = [];
    images.push(image);

    await uploadBase64Images(images, loginHistory, LoginHistory);
  }
}

async function accessedAgo(user) {
  var today = new Date();
  var userTimestamp = user.timestamp;
  var diffMs = (userTimestamp - today); // milliseconds between now & Christmas
  var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
  return diffMins;
}

async function accountLocked(response) {
  response.errorCode = 'ACCOUNT_LOCKED';
  response.message = 'Account Locked';
  response.description = 'Your account is locked. Please wait for 20 min and try again.';
}

async function invalidCredentials(response) {
  response.errorCode = 'INVALID_CREDENTIALS';
  response.message = 'Invalid Credentials';
  response.description = 'The username or password you entered is incorrect. Please try again.';
}

async function invalidLocation(response) {
  response.errorCode = 'INVALID_LOCATION';
  response.message = 'Invalid Location';
  response.description = 'Invalid location code. Please check the location code.';
}
