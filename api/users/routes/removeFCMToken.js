'use strict';

const createFcmTokenSchema = require('../schemas/createFcmTokenSchema');
const checkCommonHeadersSchema = require('../schemas/checkCommonHeaders');
const getUserById = require('../util/userFunctions').getUserById;
const deleteFcmTokensByUser = require('../util/fcmFunctions').deleteFcmTokensByUser;
const successDeleteResponse = require('../../commons/util/commonFunctions').successDeleteResponse;
const constants = require('../../../constants');

module.exports = {
  method: 'POST',
  path: '/api/v1/user/removeFCMToken',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {
      var user = req.pre.user;
      var response = {
        time: new Date()
      }

      await deleteFcmTokensByUser(user._id);
      await successDeleteResponse(response, constants.FCM_TOKEN);

      return h.response(response);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: createFcmTokenSchema
    }
  }
}