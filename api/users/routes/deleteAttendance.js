'use strict';

const Attendance = require('../model/Attendance');
const checkCommonHeadersSchema = require('../schemas/checkCommonHeaders');
const getUserById = require('../util/userFunctions').getUserById;
const getById = require('../../commons/util/collectionFunctions').getById;
const deleteDocument = require('../../commons/util/collectionFunctions').deleteDocument;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const successDeleteResponse = require('../../commons/util/commonFunctions').successDeleteResponse;
const constants = require('../../../constants');
const _ = require('lodash');

module.exports = {
  method: 'DELETE',
  path: '/api/v1/attendance/delete/{_id}',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var response = {
        'time': new Date(),
        'data': {}
      }
      let attendance = await getById(Attendance, req.params._id);

      if (attendance == null || _.isEmpty(attendance)) {

        await objectNotFound(response, constants.ATTENDANCE);
        return h.response(response).code(200);
      }
      await deleteDocument(Attendance, req.params._id);

      await successDeleteResponse(response, constants.ATTENDANCE);
      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema
    }
  }
}