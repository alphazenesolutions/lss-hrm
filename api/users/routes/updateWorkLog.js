'use strict';

const Attendance = require('../model/Attendance');
const updateWorkLogSchema = require('../schemas/updateWorkLogSchema');
const getUserById = require('../util/userFunctions').getUserById;
const checkCommonHeadersSchema = require('../schemas/checkCommonHeaders');
const constants = require('../../../constants');
const successResponse = require('../../commons/util/commonFunctions').successResponse;
const update = require('../../commons/util/collectionFunctions').update;
const _ = require('lodash');
const getById = require('../../commons/util/collectionFunctions').getById;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;

module.exports = {
    method: 'POST',
    path: '/api/v1/workLog/update',
    config: {
        auth: {
            strategy: 'jwt'
        },
        // Before the route handler runs, verify that the user is unique
        pre: [
            { method: getUserById, assign: 'user' }
        ],
        handler: async (req, h) => {

            var response = {
                'success': true,
                'time': new Date()
            }

            let attendance = await getById(Attendance, req.payload._id);
            if (attendance == null || _.isEmpty(attendance)) {

                await objectNotFound(response, constants.ATTENDANCE);
                return h.response(response).code(200);
            }

            attendance = await populateAttendance(attendance, req);
            await update(Attendance, attendance);

            await successResponse(response, constants.ATTENDANCE, attendance);
            return h.response(response).code(200);

        },
        tags: ['api'],
        // Validate the payload against the Joi schema
        validate: {
            headers: checkCommonHeadersSchema,
            payload: updateWorkLogSchema
        }
    }
}

async function populateAttendance(attendance, req) {

    attendance.employeeId = req.payload.employeeId;
    attendance.siteId = req.payload.siteId;

    if (req.payload.startTime) {
        attendance.startTime = req.payload.startTime;
        attendance.dateCreated = req.payload.startTime;
    }
    if (req.payload.breakTime) {
        attendance.breakTime = req.payload.breakTime;
    }
    if (req.payload.resumeTime) {
        attendance.resumeTime = req.payload.resumeTime;
    }
    if (req.payload.endTime) {
        attendance.endTime = req.payload.endTime;
    }

    return attendance;
}