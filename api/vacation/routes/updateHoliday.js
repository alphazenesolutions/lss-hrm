'use strict';

const Holiday = require('../model/Holiday');
const updateHolidaySchema = require('../schemas/updateHoliday');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const successUpdateResponse = require('../../commons/util/commonFunctions').successUpdateResponse;
const constants = require('../../../constants');
const update = require('../../commons/util/collectionFunctions').update;
const _ = require('lodash');
const getCollectionDataById = require('../../commons/util/commonFunctions').getCollectionDataById;

module.exports = {
  method: 'POST',
  path: '/api/v1/holiday/update',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      var response = {
        'success': true,
        'time': new Date()
      }

      let holidayObj = await getCollectionDataById(Holiday, req.payload._id);

      if (holidayObj == null || _.isEmpty(holidayObj)) {

        await objectNotFound(response, constants.HOLIDAY);
        return h.response(response).code(200);
      }

      holidayObj.holidayName = req.payload.holidayName;
      holidayObj.holidayDate = req.payload.holidayDate;

      await update(Holiday, holidayObj);

      await successUpdateResponse(response, constants.HOLIDAY);
      return h.response(response).code(200);

    },
    tags: ['api'],
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: updateHolidaySchema
    }
  }
}