'use strict';

const VacationBalance = require('../model/VacationBalance');
const updateVacationBalanceSchema = require('../schemas/updateVacationBalancesSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const constants = require('../../../constants');
const _ = require('lodash');
const update = require('../../commons/util/collectionFunctions').update;
const getById = require('../../commons/util/collectionFunctions').getById;
const successUpdateResponse = require('../../commons/util/commonFunctions').successUpdateResponse;

module.exports = {
  method: 'POST',
  path: '/api/v1/vacationBalance/update',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;

      var response = {
        'success': true,
        'time': new Date()
      }

      let vacationBalanceData = req.payload.vacationBalanceData;

      for (const vacationBalanceJsonObj of vacationBalanceData) {

        let vacationBalance = await getById(VacationBalance, vacationBalanceJsonObj._id); 

        if (vacationBalance != null && !_.isEmpty(vacationBalance)) {

          vacationBalance.vacationBalance = vacationBalanceJsonObj.vacationBalance;
          await update(VacationBalance, vacationBalance);
        }
      }

      await successUpdateResponse(response, constants.VACATION_BALANCE);
      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: updateVacationBalanceSchema
    }
  }
}