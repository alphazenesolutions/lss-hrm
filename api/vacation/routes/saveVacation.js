'use strict';

const Vacation = require('../model/Vacation');
const VacationBalance = require('../model/VacationBalance');
const saveVacationSchema = require('../schemas/saveVacationSchema');
const populateVacation = require('../util/vacationFunctions').populateVacation;
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const constants = require('../../../constants');
const successResponse = require('../../commons/util/commonFunctions').successResponse;
const create = require('../../commons/util/collectionFunctions').create;
const update = require('../../commons/util/collectionFunctions').update;
const uploadDocument = require('../../commons/util/uploadFunctions').uploadDocument;
const getDataByFilter = require('../../commons/util/collectionFunctions').getDataByFilter;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const _ = require('lodash');

module.exports = {
  method: 'POST',
  path: '/api/v1/vacation',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    payload: {
      maxBytes: 15 * 1024 * 1024, // 15 mb
      output: 'stream',
      allow: 'multipart/form-data', // important
      parse: true
    },
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;

      var response = {
        'success': true,
        'time': new Date()
      }

      let vacation = new Vacation();
      vacation.dateCreated = new Date();
      let filter = {

        employeeId: req.payload.employeeId
      }

      let duration = req.payload.duration;
      let vacationBalanceData = await getDataByFilter(VacationBalance, filter);
      let vacationBalanceObj = null;
      if (vacationBalanceData != null) {
        vacationBalanceObj = vacationBalanceData[0];
      }
      if (vacationBalanceObj == null || _.isEmpty(vacationBalanceObj)) {

        await objectNotFound(response, constants.VACATION_BALANCE);
        return h.response(response).code(200);
      }
      let leaveTypeCode = req.payload.leaveTypeCode;

      if ((vacationBalanceObj.vacationBalance[leaveTypeCode].assignedBalance - vacationBalanceObj.vacationBalance[leaveTypeCode].usedBalance) < duration) {
        await insufficientBalance(response);
        return h.response(response).code(200);
      }

      await populateVacation(vacation, userObj, req);
      vacation.status = 'APPROVED';
      vacation.approverId = userObj._id;
      vacation = await create(Vacation, vacation);

      vacationBalanceObj.vacationBalance[leaveTypeCode].usedBalance = vacationBalanceObj.vacationBalance[leaveTypeCode].usedBalance + duration;
      await update(VacationBalance, vacationBalanceObj);

      const document = req.payload['document']; // accept a field call document
      await uploadDocument(document, Vacation, vacation);

      await successResponse(response, constants.VACATION, vacation);
      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: saveVacationSchema
    }
  }
}

async function insufficientBalance(response) {
  response.success = false;
  response.message = 'Failed';
  response.description = 'Insufficient Leave Balance.';
}
