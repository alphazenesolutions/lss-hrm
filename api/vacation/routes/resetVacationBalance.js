'use strict';

const VacationBalance = require('../model/VacationBalance');
const LeaveType = require('../../commons/model/LeaveType');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const resetVacationBalanceSchema = require('../schemas/resetVacationBalanceSchema');
const getUserById = require('../../users/util/userFunctions').getUserById;
const constants = require('../../../constants');
const _ = require('lodash');
const update = require('../../commons/util/collectionFunctions').update;
const getById = require('../../commons/util/collectionFunctions').getById;
const successUpdateResponse = require('../../commons/util/commonFunctions').successUpdateResponse;
const getDataByFilter = require('../../commons/util/collectionFunctions').getDataByFilter;

module.exports = {
  method: 'POST',
  path: '/api/v1/vacationBalance/reset',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;

      var response = {
        'success': true,
        'time': new Date()
      }
      let filter = {};
      let leaveTypes = await getDataByFilter(LeaveType, {});

      if (req.payload.employeeSelection == 'Selected') {

        filter = { 'employeeId': { '$in': req.payload.employeeIds } };
      }

      let vacationBalanceData = await getDataByFilter(VacationBalance, filter);

      for (const vacationBalanceJsonObj of vacationBalanceData) {
        let vacationBalance = await getById(VacationBalance, vacationBalanceJsonObj._id);
        if (vacationBalance != null && !_.isEmpty(vacationBalance)) {

          await populateBalance(vacationBalance, userObj, leaveTypes);
          await update(VacationBalance, vacationBalance);
        }
      }

      await successUpdateResponse(response, constants.VACATION_BALANCE);
      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: resetVacationBalanceSchema
    }
  }
}

async function populateBalance(vacationBalanceObj, userObj, leaveTypes) {

  let vacationBalance = vacationBalanceObj.vacationBalance;

  for (const leaveType of leaveTypes) {
    let leaveTypeCode = leaveType.code;
    leaveTypeCode = leaveTypeCode.trim();

    if (vacationBalance[leaveTypeCode] != null && vacationBalance[leaveTypeCode].assignedBalance != null) {

      vacationBalance[leaveTypeCode].assignedBalance = 0;
    }

    if (vacationBalance[leaveTypeCode] != null && vacationBalance[leaveTypeCode].usedBalance != null) {

      vacationBalance[leaveTypeCode].usedBalance = 0;
    }
  }

  vacationBalance.updateUser = userObj._id;
  vacationBalance.updatedDate = new Date();
}