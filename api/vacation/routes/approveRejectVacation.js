'use strict';

const Vacation = require('../model/Vacation');
const approveRejectVacationSchema = require('../schemas/approveRejectVacationSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const constants = require('../../../constants');
const _ = require('lodash');
const update = require('../../commons/util/collectionFunctions').update;
const getById = require('../../commons/util/collectionFunctions').getById;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const successUpdateResponse = require('../../commons/util/commonFunctions').successUpdateResponse;
const sendNotification = require('../../notification/util/notificationFunctions').sendNotification;
const VacationBalance = require('../model/VacationBalance');
const getVacationBalanceByEmployee = require('../util/vacationFunctions').getVacationBalanceByEmployee;
const populateNotification = require('../../notification/util/notificationFunctions').populateNotification;
const saveNotification = require('../../notification/util/notificationFunctions').saveNotification;

module.exports = {
  method: 'POST',
  path: '/api/v1/vacation/approveReject',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      var response = {
        'success': true,
        'time': new Date()
      }

      let vacation = await getById(Vacation, req.payload._id);
      if (vacation == null || _.isEmpty(vacation)) {

        await objectNotFound(response, constants.VACATION);
        return h.response(response).code(200);
      }

      vacation.updateUser = userObj._id;
      vacation.dateUpdated = new Date();

      vacation.status = req.payload.status;
      vacation.comments = req.payload.comments;
      vacation.approverId = userObj._id;

      await update(Vacation, vacation);

      if (req.payload.status == 'REJECTED') {
        let vacationBalance = await getVacationBalanceByEmployee(VacationBalance, vacation.employeeId);

        if (vacationBalance != null
          && vacationBalance.vacationBalance != null
          && vacationBalance.vacationBalance[vacation.leaveTypeCode] != null) {

          vacationBalance.vacationBalance[vacation.leaveTypeCode].usedBalance = vacationBalance.vacationBalance[vacation.leaveTypeCode].usedBalance - vacation.duration;
          await update(VacationBalance, vacationBalance);
        }
      }

      let notificationData = {
        "title": constants.VACATION_REQUEST,
        "id": vacation._id,
        "notifyUserId": vacation.employeeId
      };

      let notification = await populateNotification(userObj, vacation, false, notificationData);
      notification.description = 'Your vacation request is ' + vacation.status + ' by ' + userObj.firstName + ' ' + userObj.lastName;
      await saveNotification(notification);

      await successUpdateResponse(response, constants.VACATION);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: approveRejectVacationSchema
    }
  }
}