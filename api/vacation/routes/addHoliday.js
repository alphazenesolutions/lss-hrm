'use strict';

const Holiday = require('../model/Holiday');
const addHolidaySchema = require('../schemas/addHoliday');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const successResponse = require('../../commons/util/commonFunctions').successResponse;
const constants = require('../../../constants');
const getDataByFilter = require('../../commons/util/collectionFunctions').getDataByFilter;
const create = require('../../commons/util/collectionFunctions').create;
const _ = require('lodash');

module.exports = {
  method: 'POST',
  path: '/api/v1/holiday/add',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var response = {
        'success': true,
        'time': new Date()
      }

      let queryFilter = await populateQueryFilter(req);
      let data = await getDataByFilter(Holiday, queryFilter);

      if (!_.isEmpty(data)) {
        await duplicateResponse(response);
        return h.response(response).code(200);
      }

      let holiday = new Holiday();
      holiday.holidayName = req.payload.holidayName;
      holiday.holidayDate = req.payload.holidayDate;
      holiday = await create(Holiday, holiday);

      await successResponse(response, constants.HOLIDAY, holiday);
      return h.response(response).code(200);

    },
    tags: ['api'],
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: addHolidaySchema
    }
  }
}

async function populateQueryFilter(req) {

  let filter = {

    holidayName: req.payload.holidayName,
    holidayDate: req.payload.holidayDate
  }

  return filter;
}

async function duplicateResponse(response) {
  response.success = false;
  response.message = 'Error';
  response.description = 'Duplicate holiday.';
}