'use strict';

const Holiday = require('../model/Holiday');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const holidaysReportSchema = require('../schemas/holidaysReportSchema');
const getUserById = require('../../users/util/userFunctions').getUserById;
const successGetResponse = require('../../commons/util/commonFunctions').successGetResponse;
const getDataByFilter = require('../../commons/util/collectionFunctions').getDataByFilter;

module.exports = {
  method: 'POST',
  path: '/api/v1/holidays',
  config: {
    auth: false,
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var response = {
        'success': true,
        'time': new Date()
      }

      let filter = {

        holidayDate: { "$gte": req.payload.fromDate, "$lte": req.payload.toDate }
      }
    
      let result = await getDataByFilter(Holiday, filter);

      await successGetResponse(response, result);
      return h.response(response).code(200);

    },
    tags: ['api'],
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: holidaysReportSchema
    }
  }
}