'use strict';

const populateVacationBalance = require('../util/vacationFunctions').populateVacationBalance;
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;

module.exports = {
  method: 'POST',
  path: '/api/v1/populateVacationBalance',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;

      var response = {
        'success': true,
        'time': new Date()
      }

      await populateVacationBalance(userObj);

      response.success = true;
      response.message = 'Success';
      response.description = 'Vacation Balance populated successfully.';

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
    }
  }
}