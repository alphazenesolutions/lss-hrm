'use strict';

const Vacation = require('../model/Vacation');
const updateVacationSchema = require('../schemas/updateVacationSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const populateVacation = require('../util/vacationFunctions').populateVacation;
const constants = require('../../../constants');
const _ = require('lodash');
const update = require('../../commons/util/collectionFunctions').update;
const getById = require('../../commons/util/collectionFunctions').getById;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const successUpdateResponse = require('../../commons/util/commonFunctions').successUpdateResponse;
const uploadDocument = require('../../commons/util/uploadFunctions').uploadDocument;

module.exports = {
  method: 'POST',
  path: '/api/v1/vacation/update',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    payload: {
      maxBytes: 15 * 1024 * 1024, // 15 mb
      output: 'stream',
      allow: 'multipart/form-data', // important
      parse: true
    },
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      var response = {
        'success': true,
        'time': new Date()
      }

      let vacation = await getById(Vacation, req.payload._id);
      if (vacation == null || _.isEmpty(vacation)) {

        await objectNotFound(response, constants.VACATION);
        return h.response(response).code(200);
      }

      vacation.updateUser = userObj._id;
      vacation.updatedDate = new Date();

      await populateVacation(vacation, userObj, req);
      await update(Vacation, vacation);

      const document = req.payload['document']; // accept a field call document
      await uploadDocument(document, Vacation, vacation);

      await successUpdateResponse(response, constants.VACATION);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: updateVacationSchema
    }
  }
}