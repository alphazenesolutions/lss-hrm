'use strict';

const OverTime = require('../model/OverTime');
const overtimeReportSchema = require('../schemas/overtimeReportSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const successGetResponse = require('../../commons/util/commonFunctions').successGetResponse;
const getDataByFilter = require('../../commons/util/collectionFunctions').getDataByFilter;
const getStartTime = require('../../commons/util/dateFunctions').getStartTime;
const getEndTime = require('../../commons/util/dateFunctions').getEndTime;

module.exports = {
  method: 'POST',
  path: '/api/v1/overtime/report',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var response = {
        'success': true,
        'time': new Date()
      }

      let queryFilter = await populateQueryFilter(req);
      let data = await getDataByFilter(OverTime, queryFilter);

      await successGetResponse(response, data);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: overtimeReportSchema
    }
  }
}

async function populateQueryFilter(req) {

  let timeOffset = null;
  if (req.headers.timezone != null) {

    timeOffset = parseInt(req.headers.timezone);
  }

  let startDate = await getStartTime(req.payload.fromDate, timeOffset);
  let endDate = await getEndTime(req.payload.toDate, timeOffset);

  let filter = {
    dateCreated: { "$gte": startDate, "$lt": endDate }
  }

  if (req.payload.employeeId != null) {

    filter.employeeId = req.payload.employeeId;
  }

  return filter;
}