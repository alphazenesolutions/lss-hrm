'use strict';

const applyVacationSchema = require('../schemas/addVacationBalanceSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const getUserByUserId = require('../../users/util/userFunctions').getUserByUserId;
const constants = require('../../../constants');
const successResponse = require('../../commons/util/commonFunctions').successResponse;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const _ = require('lodash');
const createVacationBalance = require('../../vacation/util/vacationFunctions').createVacationBalance;

module.exports = {
  method: 'POST',
  path: '/api/v1/vacationBalance/add',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      let loggedInUser = req.pre.user;
      let userObj = await getUserByUserId(req.payload.employeeId);
      
      let response = {
        'success': true,
        'time': new Date()
      }

      if (userObj == null || _.isEmpty(userObj)) {

        await objectNotFound(response, constants.USER);
        return h.response(response).code(200);
      }

      let vacationBalance = await createVacationBalance(loggedInUser, userObj);
      await successResponse(response, constants.VACATION_BALANCE, vacationBalance);
      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: applyVacationSchema
    }
  }
}