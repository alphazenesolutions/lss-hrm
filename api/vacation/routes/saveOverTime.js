'use strict';

const VacationBalance = require('../model/VacationBalance');
const OverTime = require('../model/OverTime');
const saveOverTimeSchema = require('../schemas/saveOverTimeSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const constants = require('../../../constants');
const create = require('../../commons/util/collectionFunctions').create;
const update = require('../../commons/util/collectionFunctions').update;
const successResponse = require('../../commons/util/commonFunctions').successResponse;
const getDataByFilter = require('../../commons/util/collectionFunctions').getDataByFilter;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const _ = require('lodash');

module.exports = {
  method: 'POST',
  path: '/api/v1/overTime',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;

      var response = {
        'success': true,
        'time': new Date()
      }

      let overTime = new OverTime();
      overTime.dateCreated = new Date();
      overTime.userId = userObj._id;
      
      let filter = {

        employeeId: req.payload.employeeId
      }

      let vacationBalanceData = await getDataByFilter(VacationBalance, filter);
      let vacationBalance = null;
      if (vacationBalanceData != null) {
        vacationBalance = vacationBalanceData[0];
      }
      if (vacationBalance == null || _.isEmpty(vacationBalance)) {

        await objectNotFound(response, constants.VACATION_BALANCE);
        return h.response(response).code(200);
      }

      await populateBalance(overTime, req);
      overTime = await create(OverTime, overTime);

      if (vacationBalance != null && !_.isEmpty(vacationBalance)) {
        vacationBalance.overTimeBalance = vacationBalance.overTimeBalance + req.payload.overTimeDuration; 
        await update(VacationBalance, vacationBalance);
      }

      await successResponse(response, constants.OVER_TIME, overTime);
      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: saveOverTimeSchema
    }
  }
}

async function populateBalance(overTime, req) {

  overTime.employeeId = req.payload.employeeId;
  overTime.overTimeDuration = req.payload.overTimeDuration;
  overTime.dateTime = req.payload.dateTime;
  overTime.comments = req.payload.comments;
}