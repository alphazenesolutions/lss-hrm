'use strict';
const getDataByFilter = require('../../commons/util/collectionFunctions').getDataByFilter;
const getUsers = require('../../users/util/userFunctions').getUsers;
const VacationBalance = require('../model/VacationBalance');
const LeaveType = require('../../commons/model/LeaveType');
const create = require('../../commons/util/collectionFunctions').create;
const update = require('../../commons/util/collectionFunctions').update;

function populateVacation(vacation, userObj, req) {

  vacation.userId = userObj._id;
  vacation.employeeId = req.payload.employeeId;
  vacation.leaveTypeCode = req.payload.leaveTypeCode;
  vacation.fromDate = req.payload.fromDate;
  vacation.toDate = req.payload.toDate;
  vacation.duration = req.payload.duration;
  vacation.comments = req.payload.comments;
}

async function populateVacationBalance(userObject) {

  if (userObject != null && userObj._id != null) {

    let users = await getUsers();

    for (const user of users) {
      if (user != null)
        await createVacationBalance(userObject, user);
    }
  }
}

async function createVacationBalance(loggedInUser, userObj) {

  let leaveTypes = await getDataByFilter(LeaveType, {});

  let vacationBalance = await getVacationBalanceByEmployee(VacationBalance, userObj._id);

  if (vacationBalance != null) {

    await updateVacationBalance(vacationBalance, leaveTypes);
    await update(VacationBalance, vacationBalance);
    return vacationBalance;
  } else {

    vacationBalance = new VacationBalance();
    vacationBalance.dateCreated = new Date();

    await populateBalance(vacationBalance, userObj, loggedInUser, leaveTypes);
    vacationBalance = await create(VacationBalance, vacationBalance);
    return vacationBalance;
  }

}

async function updateVacationBalance(vacationBalance, leaveTypes) {

  for (const leaveType of leaveTypes) {

    if (vacationBalance.vacationBalance != null && vacationBalance.vacationBalance[leaveType.code] == null) {

      vacationBalance.vacationBalance[leaveType.code] = { assignedBalance: 0, usedBalance: 0 };
    }
  }
}

async function populateBalance(vacationBalance, user, userObject, leaveTypes) {

  if (userObject != null) {

    vacationBalance.userId = userObject._id;
  }
  vacationBalance.employeeId = user._id;
  vacationBalance.vacationBalance = {};

  for (const leaveType of leaveTypes) {
    vacationBalance.vacationBalance[leaveType.code] = { assignedBalance: 0, usedBalance: 0 };
  }
  vacationBalance.overTimeBalance = 0;

}

async function getVacationBalanceByEmployee(Collection, employeeId) {

  try {

    let collectionObj = await Collection.findOne({
      employeeId: employeeId
    });

    return collectionObj;
  } catch (error) {

    console.log('error occurred during getVacationBalanceByEmployee ' + error);
    return null;
  }
}

module.exports = {
  populateVacation: populateVacation,
  populateVacationBalance: populateVacationBalance,
  createVacationBalance: createVacationBalance,
  getVacationBalanceByEmployee: getVacationBalanceByEmployee
}