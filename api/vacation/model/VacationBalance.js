'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const vacationBalanceModel = new Schema({
    userId: { type: String, required: false },
    userUpdated: { type: String, required: false },
    employeeId: { type: String, required: true },
    vacationBalance: { type: Object, required: true },
    overTimeBalance: { type: Number, required: true },
    dateCreated: { type: Date, required: true },
    dateUpdated: { type: Date, required: false }
});

module.exports = mongoose.model('VacationBalance', vacationBalanceModel);