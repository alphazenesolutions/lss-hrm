'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const overTimeModel = new Schema({
    userId: { type: String, required: true },
    userUpdated: { type: String, required: false },
    employeeId: { type: String, required: true },
    overTimeDuration: { type: Number, required: true },
    dateTime: { type: Date, required: true },
    dateCreated: { type: Date, required: true },
    dateUpdated: { type: Date, required: false }
});

module.exports = mongoose.model('OverTime', overTimeModel);