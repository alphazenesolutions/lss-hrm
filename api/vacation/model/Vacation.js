'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const clockingModel = new Schema({
  userId: { type: String, required: true },
  employeeId: { type: String, required: true },
  approverId: { type: String, required: false },
  leaveTypeCode: { type: String, required: true },
  fromDate: { type: Date, required: true },
  toDate: { type: Date, required: true },
  duration: { type: Number, required: true },
  comments: { type: String, required: false },
  fileId: { type: String, required: false },
  status: { type: String, required: false },
  images: { type: Array, required: false },
  dateCreated: { type: Date, required: true },
  dateUpdated: { type: Date, required: false }
});

module.exports = mongoose.model('Vacation', clockingModel);