'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const holidayModel = new Schema({
  holidayName: { type: String, required: true },
  holidayDate: { type: Date, required: true }
});

module.exports = mongoose.model('Holiday', holidayModel);