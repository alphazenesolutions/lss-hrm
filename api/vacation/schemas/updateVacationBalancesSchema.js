'use strict';

const Joi = require('joi');

const updateVacationBalancesSchema = Joi.object({
    vacationBalanceData: Joi.array().required().error(new Error(
        "Vacation Balance Data is required."
    ))    
});

module.exports = updateVacationBalancesSchema;