'use strict';

const Joi = require('joi');

const saveOverTimeSchema = Joi.object({
    employeeId: Joi.string().required().error(new Error(
        "Employee Id is required."
    )),
    overTimeDuration: Joi.number().required().error(new Error(
        "OverTime Duration is required."
    )),
    dateTime: Joi.date().required().error(new Error(
        "OverTime Date is required."
    )),
    comments: Joi.string().allow('', null)
});

module.exports = saveOverTimeSchema;