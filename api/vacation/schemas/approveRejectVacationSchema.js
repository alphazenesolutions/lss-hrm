'use strict';

const Joi = require('joi');

const approveRejectVacationSchema = Joi.object({
    _id: Joi.string().required().error(new Error(
        "Vacation Id is required."
    )),
    status: Joi.string().required().error(new Error(
        "Status is required."
    )),
    comments: Joi.string().allow('', null)
});

module.exports = approveRejectVacationSchema;