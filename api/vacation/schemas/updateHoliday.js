'use strict';

const Joi = require('joi');

const updateHolidaySchema = Joi.object({
  _id: Joi.string().required(),
  holidayName: Joi.string().required(),
  holidayDate: Joi.date().required()
});

module.exports = updateHolidaySchema;