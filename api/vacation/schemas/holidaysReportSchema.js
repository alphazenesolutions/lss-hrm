'use strict';

const Joi = require('joi');

const holidaysReportSchema = Joi.object({

    fromDate: Joi.date().required().error(new Error(
        "From Date is required."
    )),
    toDate: Joi.date().required().error(new Error(
        "To Date is required."
    ))
});

module.exports = holidaysReportSchema;