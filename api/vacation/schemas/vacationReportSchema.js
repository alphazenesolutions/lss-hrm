'use strict';

const Joi = require('joi');

const vacationReportSchema = Joi.object({

    employeeId: Joi.string().allow('', null),
    fromDate: Joi.date().required().error(new Error(
        "From Date is required."
    )),
    toDate: Joi.date().required().error(new Error(
        "To Date is required."
    ))
});

module.exports = vacationReportSchema;