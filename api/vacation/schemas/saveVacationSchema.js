'use strict';

const Joi = require('joi');
const Readable = require('stream').Readable

const saveVacationSchema = Joi.object({
    employeeId: Joi.string().required().error(new Error(
        "Employee Id is required."
    )),
    fromDate: Joi.date().required().error(new Error(
        "From Date is required."
    )),
    leaveTypeCode: Joi.string().required().error(new Error(
        "Leave Type is required."
    )),
    toDate: Joi.date().required().error(new Error(
        "To Date is required."
    )),
    duration: Joi.number().required().error(new Error(
        "Duration is required."
    )),
    comments: Joi.string().allow('', null),
    document: Joi.object().type(Readable)    
});

module.exports = saveVacationSchema;