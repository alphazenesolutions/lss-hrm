'use strict';

const Joi = require('joi');

const saveVacationSchema = Joi.object({
    employeeId: Joi.string().required().error(new Error(
        "Employee Id is required."
    ))
});

module.exports = saveVacationSchema;