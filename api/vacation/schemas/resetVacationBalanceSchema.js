'use strict';

const Joi = require('joi');

const resetVacationBalanceSchema = Joi.object({
    employeeSelection: Joi.string().only(['All', 'Selected']).required(),
    employeeIds: Joi.array().allow('', null)
});

module.exports = resetVacationBalanceSchema;