'use strict';

const Joi = require('joi');

const overtimeReportSchema = Joi.object({

    employeeId: Joi.string().required().error(new Error(
        "Employee Id is required."
    )),
    fromDate: Joi.date().required().error(new Error(
        "From Date is required."
    )),
    toDate: Joi.date().required().error(new Error(
        "To Date is required."
    ))
});

module.exports = overtimeReportSchema;