'use strict';

const Joi = require('joi');

const addHolidaySchema = Joi.object({
  holidayName: Joi.string().required(),
  holidayDate: Joi.date().required()
});

module.exports = addHolidaySchema;