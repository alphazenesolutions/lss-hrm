'use strict';

const Joi = require('joi');

const saveOverTimeSchema = Joi.object({
    _id: Joi.string().required().error(new Error(
        "Id is required."
    )),
    overTimeDuration: Joi.number().required().error(new Error(
        "OverTime Duration is required."
    )),
    comments: Joi.number().allow('', null)
});

module.exports = saveOverTimeSchema;