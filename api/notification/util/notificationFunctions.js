'use strict';

const Boom = require('boom');
const Notification = require('../model/Notification');
const firebaseUtils = require('./firebaseUtils');
const getUserByUserId = require('../../users/util/userFunctions').getUserByUserId;
const getFcmTokenByUser = require('../../users/util/fcmFunctions').getFcmTokenByUser;
const getOEUsers = require('../../users/util/userFunctions').getOEUsers;
const _ = require('lodash');
const create = require('../../commons/util/collectionFunctions').create;

async function getNotifications(userId) {

  var results = await Notification.find(
    { "notifyUserId": userId, "acknowledgement": false }
  );

  return results;
}

async function getOpenOverTimeNotifications(userId, attendanceId) {

  var results = await Notification.find(
    { "userId": userId, "metaData.id": attendanceId, "acknowledgement": false }
  );

  return results;
}

async function getNotification(notificationId) {

  var results = await Notification.find(
    { "_id": notificationId }
  );

  return results;
}

async function saveNotification(notification) {

  var result;

  await Notification.create(notification).then((notificationObj) => {
    result = notificationObj;
  }).catch((err) => {
    console.log('error occurred while saving notification.. ' + err);
    return Boom.badImplementation('Error', err);
  });

  return result;
}

async function notifyDeviceAdded(deviceObj) {

  let firebaseTokens = [];
  let userIds = [];

  if (deviceObj.userId != null) {
    let user = await getUserByUserId(deviceObj.userId);

    await _.forOwn(user.friends, async function (userObj, userId) {

      if (_.isEqual('ACCEPTED', userObj.status)) {
        userIds.push(userId);
      }
    });

    firebaseTokens = await getFcmTokens(userIds);

    let messageBody = user.fullName + ' has added a new ' + deviceObj.deviceType + ' with name ' + deviceObj.deviceName;

    var firebaseMessage = {
      "title": "New Device Added.",
      "body": messageBody
    };

    for (const fcmToken of firebaseTokens) {

      firebaseUtils.sendFirebaseMessage(fcmToken, JSON.stringify(firebaseMessage));
    }
  }

  return firebaseTokens;
}

async function getFcmTokens(userIds) {

  let firebaseTokens = [];
  let fcmToken = {};

  for (const userId of userIds) {
    fcmToken = await getFcmTokenByUser(userId);
    firebaseTokens.push(fcmToken.fcmToken);
  }

  return firebaseTokens;
}

async function updateNotification(notification) {

  await Notification
    .findOneAndUpdate({ _id: notification._id }, notification);
}

async function sendNotificationToOEs(userObj, collectionObj, notificationData) {

  let oeUsers = await getOEUsers();

  for (const oeUser of oeUsers) {
    notificationData.notifyUserId = oeUser._id;
    await sendNotification(userObj, collectionObj, notificationData);
  }
}

async function sendNotification(userObj, collectionObj, notificationData) {

  let fcmTokenObj = await getFcmTokenByUser(notificationData.notifyUserId);
  let firebaseSuccess = null;
  if (fcmTokenObj != null && collectionObj != null) {

    let firebaseMessage = {
      "title": notificationData.title,
      "body": { "notification_type": notificationData.title, "id": collectionObj._id }
    };
    firebaseMessage.body.description = userObj.firstName + ' ' + userObj.lastName + ' has created the ' + notificationData.title;

    firebaseSuccess = await firebaseUtils.sendFirebaseMessage(fcmTokenObj.fcmToken, JSON.stringify(firebaseMessage));
  }

  if (fcmTokenObj == null) {
    firebaseSuccess = false;
  }

  if (notificationData.notifyUserId != null) {

    let notification = await populateNotification(userObj, collectionObj, firebaseSuccess, notificationData);
    await create(Notification, notification);
  }
}

async function sendCustomNotification(userObj, notificationData) {
  let notification = await populateCustomNotification(userObj, notificationData);
  await create(Notification, notification);
}

async function populateCustomNotification(userObj, notificationData) {
  let notification = new Notification();
  notification.userId = userObj._id;
  notification.notifyUserId = notificationData.notifyUserId;
  notification.notificationType = notificationData.title;
  notification.id = collectionObj._id;
  notification.title = notificationData.title;
  notification.description = userObj.firstName + ' ' + userObj.lastName + ' has created the ' + notificationData.title;
  notification.androidSync = false;
  notification.acknowledgement = false;
  notification.dateCreated = new Date();
  return notification;
}

async function populateNotification(userObj, collectionObj, firebaseSuccess, notificationData) {
  let notification = new Notification();
  notification.userId = userObj._id;
  notification.notifyUserId = notificationData.notifyUserId;
  notification.notificationType = notificationData.title;
  notification.id = collectionObj._id;
  notification.title = notificationData.title;
  notification.description = userObj.firstName + ' ' + userObj.lastName + ' has created the ' + notificationData.title;
  notification.androidSync = false;
  notification.acknowledgement = false;
  notification.firebaseSuccess = firebaseSuccess;
  notification.dateCreated = new Date();
  return notification;
}


module.exports = {
  getNotifications: getNotifications,
  getNotification: getNotification,
  saveNotification: saveNotification,
  notifyDeviceAdded: notifyDeviceAdded,
  updateNotification: updateNotification,
  sendNotification: sendNotification,
  sendCustomNotification: sendCustomNotification,
  sendNotificationToOEs: sendNotificationToOEs,
  populateNotification: populateNotification,
  getOpenOverTimeNotifications: getOpenOverTimeNotifications
}