'use strict';

const admin = require("firebase-admin");
const _ = require('lodash');

async function sendFirebaseMessage(registrationToken, message) {

  // See the "Defining the message payload" section below for details
  // on how to define a message payload.
  var payload = {
    data: {
      message: message
    }
  };

  console.log('firebaseToken: ' + registrationToken);
  console.log('payload: ' + JSON.stringify(payload));
  let firebaseDelivered = true;
  // Send a message to the device corresponding to the provided
  // registration token.

  const promises = [];
  promises.push(sendMessage(registrationToken, payload));
  const promiseResults = await Promise.all(promises);

  _.forEach(promiseResults, async function (resultsObj) {
    firebaseDelivered = resultsObj;
    console.log('resultsObj ', resultsObj);
  });

  return firebaseDelivered;
}

module.exports = {
  sendFirebaseMessage: sendFirebaseMessage
}

async function sendMessage(registrationToken, payload) {
  let deliveredStatus = null;
  await admin.messaging().sendToDevice(registrationToken, payload)
    .then(async function (response) {
      // See the MessagingDevicesResponse reference documentation for
      // the contents of response.
      console.log("Firebase response: ", JSON.stringify(response));

      if (response != null && response.failureCount > 0) {

        deliveredStatus = false;
      } else {
        deliveredStatus = true;
      }
    })
    .catch(async function (error) {
      console.log("Error sending message:", error);

      deliveredStatus = false;
    });

  return deliveredStatus;
}

