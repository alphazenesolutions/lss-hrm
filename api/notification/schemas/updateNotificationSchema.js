'use strict';

const Joi = require('joi');

const updateNotificationSchema = Joi.object({
  notificationIds: Joi.array().required(),
  acknowledgement: Joi.boolean().required()
});

module.exports = updateNotificationSchema;