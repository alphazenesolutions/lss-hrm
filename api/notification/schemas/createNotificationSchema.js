'use strict';

const Joi = require('joi');

const createNotificationSchema = Joi.object({
  title: Joi.string().required(),
  description: Joi.string().required(),
  trigger: Joi.object().required()
});

module.exports = createNotificationSchema;