'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const notificationModel = new Schema({
  userId: { type: String, required: true },
  notifyUserId: { type: String, required: true },
  notificationType: { type: String, required: true },
  id: { type: String, required: true },
  title: { type: String, required: true },
  description: { type: String, required: true },
  androidSync: { type: Boolean, required: false },
  firebaseSuccess: { type: Boolean, required: false },
  acknowledgement: { type: Boolean, required: false },
  metaData: { type: Object, required: false },
  dateCreated: { type: Date, required: true },
  dateUpdated: { type: Date, required: false }
});

module.exports = mongoose.model('Notification', notificationModel);