'use strict';

const Notification = require('../model/Notification');
const userNotFound = require('../../users/util/userFunctions').userNotFound;
const getUserById = require('../../users/util/userFunctions').getUserById;
const saveNotification = require('../util/notificationFunctions').saveNotification;
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const createNotificationSchema = require('../schemas/createNotificationSchema');

module.exports = {
  method: 'POST',
  path: '/api/v1/notification/save',
  config: {
    auth: {
      strategy: 'jwt'
    },
    // Before the route handler runs, verify that the user is unique
    pre: [
      { method: getUserById, assign: 'user' }
    ],  
    handler: async (req, h) => {

      try {

        var user = req.pre.user;
        var response = {
          time: new Date()
        }
  
        if (user == null) {

          await userNotFound(response);
          return h.response(response).code(400);
        }

        let notification = new Notification();
        await populateNotification(req, notification);

        notification = await saveNotification(notification);
        response.success = true;
        response.notificationId = notification._id;

        return h.response(response).code(200);
  
      } catch (err) {
        console.log('Exception occurred in saveNotification' + err);
      }
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: createNotificationSchema
    }
  }
}

async function populateNotification(req, notification){
    notification.title = req.payload.title;
    notification.description = req.payload.description;
    notification.timestamp = new Date();
    notification.userId = req.pre.user._id;
    notification.acknowledgement = false;
}