'use strict';

const Notification = require('../model/Notification');
const overTimeNotificationReportSchema = require('../schemas/overTimeNotificationReportSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const getDataByFilter = require('../../commons/util/collectionFunctions').getDataByFilter;
const successGetResponse = require('../../commons/util/commonFunctions').successGetResponse;
const getStartTime = require('../../commons/util/dateFunctions').getStartTime;
const getEndTime = require('../../commons/util/dateFunctions').getEndTime;

module.exports = {
  method: 'POST',
  path: '/api/v1/user/overtime/notification/report',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    // Before the route handler runs, verify that the user is unique
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var response = {
        'success': true,
        'time': new Date()
      }

      let queryFilter = await populateQueryFilter(req);
      let data = await getDataByFilter(Notification, queryFilter);

      await successGetResponse(response, data);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: overTimeNotificationReportSchema
    }
  }
}

async function populateQueryFilter(req) {

  let timeOffset = null;
  if (req.headers.timezone != null) {

    timeOffset = parseInt(req.headers.timezone);
  }

  let startDate = await getStartTime(req.payload.fromDate, timeOffset);
  let endDate = await getEndTime(req.payload.toDate, timeOffset);

  let filter = {
    dateCreated: { "$gte": startDate, "$lt": endDate },
    notificationType: 'OverTime', 
    acknowledgement: true
  }

  if (req.payload.siteId != null) {
   
    filter['metaData.siteId'] = req.payload.siteId;
  }

  return filter;
}