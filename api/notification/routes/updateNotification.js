'use strict';

const Notification = require('../model/Notification');
const userNotFound = require('../../users/util/userFunctions').userNotFound;
const getUserById = require('../../users/util/userFunctions').getUserById;
const updateNotification = require('../util/notificationFunctions').updateNotification;
const getNotification = require('../util/notificationFunctions').getNotification;
const getOpenOverTimeNotifications = require('../util/notificationFunctions').getOpenOverTimeNotifications;
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const updateNotificationSchema = require('../schemas/updateNotificationSchema');
const successUpdateResponse = require('../../commons/util/commonFunctions').successUpdateResponse;
const deleteDocument = require('../../commons/util/collectionFunctions').deleteDocument;
const constants = require('../../../constants');
const _ = require('lodash');

module.exports = {
  method: 'POST',
  path: '/api/v1/notification/update',
  config: {
    tags: ['api'],
    auth: {
      strategy: 'jwt'
    },
    // Before the route handler runs, verify that the user is unique
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      try {

        var user = req.pre.user;
        var response = {
          time: new Date()
        }

        if (user == null) {

          await userNotFound(response);
          return h.response(response).code(400);
        }

        let notifications = await getNotifications(req);

        if (notifications.length > 0)
          for (const notification of notifications) {

            if (notification != null) {

              let notificationObj = notification;

              if (Array.isArray(notification) && notification[0] != null) {

                notificationObj = notification[0];
              }

              notificationObj.androidSync = true; // TODO address this 
              notificationObj.dateUpdated = new Date();
              if (req.payload.acknowledgement != null) {
                notificationObj.acknowledgement = req.payload.acknowledgement;
              }
              await updateNotification(notificationObj);

              await removeOverTimeNotifications(notificationObj);

            }
          }
        await successUpdateResponse(response, constants.NOTIFICATION);

        return h.response(response).code(200);

      } catch (err) {
        console.log('Exception occurred in updateNotification' + err);
      }
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: updateNotificationSchema
    }
  }
}

async function getNotifications(req) {

  const notificationIds = req.payload['notificationIds'];

  let notifications = [];

  for (const notificationId of notificationIds) {
    let notification = await getNotification(notificationId);
    notifications.push(notification);
  }

  return notifications;
}

async function removeOverTimeNotifications(overTimeNotificationObj) {

  if (overTimeNotificationObj.notificationType == constants.OVER_TIME) {
    let userId = overTimeNotificationObj.userId;
    let attendanceId = null;

    if (overTimeNotificationObj.metaData) {
      attendanceId = overTimeNotificationObj.metaData.id;
    }

    let overTimeNotifications = await getOpenOverTimeNotifications(userId, attendanceId);

    for (const notificationObj of overTimeNotifications) {

      await deleteDocument(Notification, notificationObj._id);
    }
  }
}