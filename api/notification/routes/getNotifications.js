'use strict';

const userNotFound = require('../../users/util/userFunctions').userNotFound;
const getUserById = require('../../users/util/userFunctions').getUserById;
const getNotifications = require('../util/notificationFunctions').getNotifications;
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');

module.exports = {
  method: 'GET',
  path: '/api/v1/user/notifications',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    // Before the route handler runs, verify that the user is unique
    pre: [
      { method: getUserById, assign: 'user' }
    ],  
    handler: async (req, h) => {

      try {
        
        var user = req.pre.user;
        var response = {
          time: new Date()
        }
  
        if (user == null) {

          await userNotFound(response);
          return h.response(response).code(400);
        }

        var notifications = await getNotifications(user._id);
        response.success = true;
        response.data = {};
        response.data.notifications = notifications;

        return h.response(response).code(200);
  
      } catch (err) {
        console.log('Exception occurred in getNotifications' + err);
      }
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema
    }
  }
}