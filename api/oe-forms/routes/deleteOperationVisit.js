'use strict';

const OperationVisit = require('../model/OperationVisit');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const getById = require('../../commons/util/collectionFunctions').getById;
const deleteDocument = require('../../commons/util/collectionFunctions').deleteDocument;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const successDeleteResponse = require('../../commons/util/commonFunctions').successDeleteResponse;
const constants = require('../../../constants');
const _ = require('lodash');

module.exports = {
  method: 'DELETE',
  path: '/api/v1/operationVisit/delete/{_id}',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var response = {
        'time': new Date(),
        'data': {}
      }
      let operationVisit = await getById(OperationVisit, req.params._id);

      if (operationVisit == null || _.isEmpty(operationVisit)) {

        await objectNotFound(response, constants.OPERATION_VISIT_REPORT);
        return h.response(response).code(200);
      }
      await deleteDocument(OperationVisit, req.params._id);

      await successDeleteResponse(response, constants.OPERATION_VISIT_REPORT);
      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema
    }
  }
}