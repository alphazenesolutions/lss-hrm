'use strict';

const ELearningResponse = require('../model/ELearningResponse');
const User = require('../../users/model/User');
const ELearning = require('../model/ELearning');
const submitELearningResultsSchema = require('../schemas/submitELearningResultsSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const successUpdateResponse = require('../../commons/util/commonFunctions').successUpdateResponse;
const update = require('../../commons/util/collectionFunctions').update;
const getById = require('../../commons/util/collectionFunctions').getById;
const constants = require('../../../constants');
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const getOEUsers = require('../../users/util/userFunctions').getOEUsers;
const populateNotification = require('../../notification/util/notificationFunctions').populateNotification;
const saveNotification = require('../../notification/util/notificationFunctions').saveNotification;
const _ = require('lodash');

module.exports = {
    method: 'POST',
    path: '/api/v1/eLearning/results',
    config: {
        auth: {
            strategy: 'jwt'
        },
        tags: ['api'],
        pre: [
            { method: getUserById, assign: 'user' }
        ],
        handler: async (req, h) => {

            var userObj = req.pre.user;

            var response = {
                'success': true,
                'time': new Date()
            }

            let eLearningResponse = await getById(ELearningResponse, req.payload._id);
            if (eLearningResponse == null || _.isEmpty(eLearningResponse)) {

                await objectNotFound(response, constants.E_LEARNING_RESPONSE);
                return h.response(response).code(200);
            }

            eLearningResponse.results = req.payload.results;
            eLearningResponse.isFailed = req.payload.isFailed;
            eLearningResponse.dateUpdated = new Date();
            eLearningResponse.userUpdated = userObj._id;
            await update(ELearningResponse, eLearningResponse);

            if (req.payload.isFailed == true) {
                await notifyUsers(userObj, eLearningResponse);
            }

            await successUpdateResponse(response, constants.E_LEARNING_RESPONSE);
            return h.response(response).code(200);
        },
        // Validate the payload against the Joi schema
        validate: {
            headers: checkCommonHeadersSchema,
            payload: submitELearningResultsSchema
        }
    }
}

async function notifyUsers(userObj, eLearningResponse) {

    let eLearning = await getById(ELearning, eLearningResponse.eLearningId);
    let employeeObj = await getById(User, eLearningResponse.userId);

    let notificationData = {
        "title": constants.E_LEARNING_FAILED,
        "id": eLearningResponse._id,
        "notifyUserId": employeeObj._id
    };

    let notification = null;
    if (eLearning != null && employeeObj != null) {

        notification = await populateNotification(employeeObj, eLearningResponse, false, notificationData);
        notification.description = 'You have failed the e-learning ' + eLearning.title;
        notification.metaData = { eLearningId: eLearning._id };

        await saveNotification(notification);
        let oeUsers = await getOEUsers();

        for (const oeUser of oeUsers) {
            notificationData.notifyUserId = oeUser._id;
            notification = await populateNotification(employeeObj, eLearningResponse, false, notificationData);
            notification.description = employeeObj.firstName + ' ' + employeeObj.lastName + ' have failed the e-learning ' + eLearning.title;
            notification.metaData = { eLearningId: eLearning._id };

            await saveNotification(notification);
        }
    }
}