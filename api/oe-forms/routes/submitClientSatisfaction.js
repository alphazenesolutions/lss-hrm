'use strict';

const ClientSatisfaction = require('../model/ClientSatisfaction');
const submitClientSatisfactionSchema = require('../schemas/submitClientSatisfactionSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const constants = require('../../../constants');
const _ = require('lodash');
const update = require('../../commons/util/collectionFunctions').update;
const getById = require('../../commons/util/collectionFunctions').getById;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const successUpdateResponse = require('../../commons/util/commonFunctions').successUpdateResponse;

module.exports = {
  method: 'POST',
  path: '/api/v1/clientSatisfaction/submit',
  config: {
    auth: false,
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;

      var response = {
        'success': true,
        'time': new Date()
      }

      let clientSatisfaction = await getById(ClientSatisfaction, req.payload._id);
      if (clientSatisfaction == null || _.isEmpty(clientSatisfaction)) {

        await objectNotFound(response, constants.CLIENT_SATISFACTION);
        return h.response(response).code(200);
      }
      if(clientSatisfaction.fileId){

        await linkExpired(response);
        return h.response(response).code(200);
      }

      clientSatisfaction.dateUpdated = new Date();

      await populateClientSatisfaction(clientSatisfaction, req);
      await update(ClientSatisfaction, clientSatisfaction);
 
      await successUpdateResponse(response, constants.CLIENT_SATISFACTION);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: submitClientSatisfactionSchema
    }
  }
}

async function linkExpired(response) {
  response.success = false;
  response.message = 'Failed';
  response.description = 'This link is expired, Please contact support team.';
}

async function populateClientSatisfaction(clientSatisfaction, req) {

  if (req.payload.itemData != null && !_.isEmpty(req.payload.itemData)) {

    clientSatisfaction.itemData = req.payload.itemData;
  }

  if (req.payload.comments != null && !_.isEmpty(req.payload.comments)) {

    clientSatisfaction.comments = req.payload.comments;
  }

}