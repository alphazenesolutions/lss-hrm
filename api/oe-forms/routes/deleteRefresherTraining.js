'use strict';

const RefresherTraining = require('../model/RefresherTraining');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const getById = require('../../commons/util/collectionFunctions').getById;
const deleteDocument = require('../../commons/util/collectionFunctions').deleteDocument;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const successDeleteResponse = require('../../commons/util/commonFunctions').successDeleteResponse;
const constants = require('../../../constants');
const _ = require('lodash');

module.exports = {
  method: 'DELETE',
  path: '/api/v1/refresherTraining/delete/{_id}',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var response = {
        'time': new Date(),
        'data': {}
      }
      let refresherTraining = await getById(RefresherTraining, req.params._id);

      if (refresherTraining == null || _.isEmpty(refresherTraining)) {

        await objectNotFound(response, constants.REFRESHER_TRAINING);
        return h.response(response).code(200);
      }
      await deleteDocument(RefresherTraining, req.params._id);

      await successDeleteResponse(response, constants.REFRESHER_TRAINING);
      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema
    }
  }
}