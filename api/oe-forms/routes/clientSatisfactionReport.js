'use strict';

const ClientSatisfaction = require('../model/ClientSatisfaction');
const clientSatisfactionReportSchema = require('../schemas/clientSatisfactionReportSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const getUserById = require('../../users/util/userFunctions').getUserById;
const successGetResponse = require('../../commons/util/commonFunctions').successGetResponse;
const getDataByFilter = require('../../commons/util/collectionFunctions').getDataByFilter;
const _ = require('lodash');
const getStartTime = require('../../commons/util/dateFunctions').getStartTime;
const getEndTime = require('../../commons/util/dateFunctions').getEndTime;

module.exports = {
  method: 'POST',
  path: '/api/v1/clientSatisfaction/report',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var response = {
        'success': true,
        'time': new Date()
      }

      if (req.payload.siteId) {

        await validateSiteId(req, response);
      }

      if (!response.success) {
        return h.response(response).code(200);
      }

      let queryFilter = await populateQueryFilter(req);
      let data = await getDataByFilter(ClientSatisfaction, queryFilter);

      await successGetResponse(response, data);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: clientSatisfactionReportSchema
    }
  }
}

async function populateQueryFilter(req) {

  let timeOffset = null;
  if (req.headers.timezone != null) {

    timeOffset = parseInt(req.headers.timezone);
  }

  let startDate = await getStartTime(req.payload.fromDate, timeOffset);
  let endDate = await getEndTime(req.payload.toDate, timeOffset);

  let filter = {
    dateCreated: { "$gte": startDate, "$lt": endDate }
  }
  if (req.payload.siteId != null) {
    filter.siteId = req.payload.siteId;
  }

  return filter;
}