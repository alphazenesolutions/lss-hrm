'use strict';

const ELearningResponse = require('../model/ELearningResponse');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getById = require('../../commons/util/collectionFunctions').getById;
const getUserById = require('../../users/util/userFunctions').getUserById;
const successGetResponse = require('../../commons/util/commonFunctions').successGetResponse;
const constants = require('../../../constants');
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const _ = require('lodash');

module.exports = {
  method: 'GET',
  path: '/api/v1/eLearningResponse/{id}',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var response = {
        'time': new Date(),
        'data': {}
      }

      let eLearningResponseObj = await getById(ELearningResponse, req.params.id);
      if (eLearningResponseObj == null || _.isEmpty(eLearningResponseObj)) {

        await objectNotFound(response, constants.E_LEARNING_RESPONSE);
        return h.response(response).code(200);
      }

      await successGetResponse(response, eLearningResponseObj);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema
    }
  }
}