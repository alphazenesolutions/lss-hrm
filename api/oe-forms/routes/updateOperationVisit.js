'use strict';

const OperationVisit = require('../model/OperationVisit');
const operationVisitUpdateSchema = require('../schemas/operationVisitUpdateSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const getUserByUserId = require('../../users/util/userFunctions').getUserByUserId;
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const constants = require('../../../constants');
const populateOperationVisit = require('../util/operationVisitFunctions').populateOperationVisit;
const invalidConductedUser = require('../util/operationVisitFunctions').invalidConductedUser;
const _ = require('lodash');
const update = require('../../commons/util/collectionFunctions').update;
const getById = require('../../commons/util/collectionFunctions').getById;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const successUpdateResponse = require('../../commons/util/commonFunctions').successUpdateResponse;

module.exports = {
  method: 'POST',
  path: '/api/v1/operationVisit/update',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      let response = {
        'success': true,
        'time': new Date()
      }

      await validateSiteId(req, response);

      if (!response.success) {

        return h.response(response).code(200);
      }

      let conductedByUser = await getUserByUserId(req.payload.conductedBy);
      if (conductedByUser == null) {

        await invalidConductedUser(response);
        return h.response(response).code(200);
      }

      let operationVisit = await getById(OperationVisit, req.payload._id);
      if (operationVisit == null || _.isEmpty(operationVisit)) {

        await objectNotFound(response, constants.JOB_APPRAISAL);
        return h.response(response).code(200);
      }

      operationVisit.updateUser = userObj._id;
      operationVisit.updatedDate = new Date();

      await populateOperationVisit(operationVisit, req);
      await update(OperationVisit, operationVisit);
      await successUpdateResponse(response, constants.OPERATION_VISIT_REPORT);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: operationVisitUpdateSchema
    }
  }
}