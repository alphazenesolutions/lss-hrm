'use strict';

const ELearning = require('../model/ELearning');
const eLearningSchema = require('../schemas/eLearningSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const successResponse = require('../../commons/util/commonFunctions').successResponse;
const create = require('../../commons/util/collectionFunctions').create;
const update = require('../../commons/util/collectionFunctions').update;
const constants = require('../../../constants');
const uploadFile = require('../../commons/util/fileFunctions').uploadFile;
const populateELearning = require('../util/eLearningFunctions').populateELearning;
const sendELearningNotification = require('../util/eLearningFunctions').sendELearningNotification;

module.exports = {
    method: 'POST',
    path: '/api/v1/eLearning',
    config: {
        auth: {
            strategy: 'jwt'
        },
        tags: ['api'],
        payload: constants.MULTIPART,
        pre: [
            { method: getUserById, assign: 'user' }
        ],
        handler: async (req, h) => {

            var userObj = req.pre.user;
            var response = {
                'success': true,
                'time': new Date()
            }

            let eLearning = new ELearning();
            eLearning.userId = userObj._id;
            eLearning.dateCreated = new Date();

            await populateELearning(eLearning, req);
            eLearning = await create(ELearning, eLearning);

            const document = req.payload['document']; // accept a field call document
            
            if (document != null) {

                let fileId = await uploadFile(document, eLearning, constants.FOLDER_ELEARNING);
                eLearning.fileId = fileId;
                await update(ELearning, eLearning);
            }

            if (req.payload.notifySwitch == true) {

                await sendELearningNotification(userObj, eLearning, req.payload.empSelection, req.payload.selectedEmployees);
            }

            await successResponse(response, constants.E_LEARNING, eLearning);
            return h.response(response).code(200);
        },
        // Validate the payload against the Joi schema
        validate: {
            headers: checkCommonHeadersSchema,
            payload: eLearningSchema
        }
    }
}