'use strict';

const ELearning = require('../model/ELearning');
const updateELearningSchema = require('../schemas/updateELearningSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const constants = require('../../../constants');
const uploadDocument = require('../../commons/util/uploadFunctions').uploadDocument;
const _ = require('lodash');
const update = require('../../commons/util/collectionFunctions').update;
const getById = require('../../commons/util/collectionFunctions').getById;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const successUpdateResponse = require('../../commons/util/commonFunctions').successUpdateResponse;
const populateELearning = require('../util/eLearningFunctions').populateELearning;
const sendELearningNotification = require('../util/eLearningFunctions').sendELearningNotification;

module.exports = {
    method: 'POST',
    path: '/api/v1/eLearning/update',
    config: {
        auth: {
            strategy: 'jwt'
        },
        tags: ['api'],
        payload: {
            maxBytes: 15 * 1024 * 1024, // 15 mb
            output: 'stream',
            allow: 'multipart/form-data', // important
            parse: true
        },
        pre: [
            { method: getUserById, assign: 'user' }
        ],
        handler: async (req, h) => {

            var userObj = req.pre.user;
            var response = {
                'success': true,
                'time': new Date()
            }

            let eLearning = await getById(ELearning, req.payload._id);
            if (eLearning == null || _.isEmpty(eLearning)) {

                await objectNotFound(response, constants.E_LEARNING);
                return h.response(response).code(200);
            }

            eLearning.updateUser = userObj._id;
            eLearning.updatedDate = new Date();
      
            await populateELearning(eLearning, req);
            await update(ELearning, eLearning);

            const document = req.payload['document']; // accept a field call document
            await uploadDocument(document, ELearning, eLearning);

            if(req.payload.notifySwitch == true){

                await sendELearningNotification(userObj, eLearning, req.payload.empSelection, req.payload.selectedEmployees);
            }

            await successUpdateResponse(response, constants.E_LEARNING);
            return h.response(response).code(200);
        },
        // Validate the payload against the Joi schema
        validate: {
            headers: checkCommonHeadersSchema,
            payload: updateELearningSchema
        }
    }
}