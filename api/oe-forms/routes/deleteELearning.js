'use strict';

const ELearning = require('../model/ELearning');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const getById = require('../../commons/util/collectionFunctions').getById;
const deleteDocument = require('../../commons/util/collectionFunctions').deleteDocument;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const successDeleteResponse = require('../../commons/util/commonFunctions').successDeleteResponse;
const constants = require('../../../constants');
const _ = require('lodash');

module.exports = {
  method: 'DELETE',
  path: '/api/v1/eLearning/delete/{_id}',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var response = {
        'time': new Date(),
        'data': {}
      }
      let jobTraining = await getById(ELearning, req.params._id);

      if (jobTraining == null || _.isEmpty(jobTraining)) {

        await objectNotFound(response, constants.E_LEARNING);
        return h.response(response).code(200);
      }
      await deleteDocument(ELearning, req.params._id);

      await successDeleteResponse(response, constants.E_LEARNING);
      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema
    }
  }
}