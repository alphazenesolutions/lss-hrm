'use strict';

const ClientSatisfaction = require('../model/ClientSatisfaction');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getCollectionDataById = require('../../commons/util/commonFunctions').getCollectionDataById;
const getUserById = require('../../users/util/userFunctions').getUserById;
const getUserByUserId = require('../../users/util/userFunctions').getUserByUserId;
const successGetResponse = require('../../commons/util/commonFunctions').successGetResponse;
const getSite = require('../../sites/util/siteFunctions').getSite;

module.exports = {
  method: 'GET',
  path: '/api/v1/clientSatisfaction/{id}',
  config: {
    auth: false,
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var response = {
        'time': new Date(),
        'data': {},
        'metadata': {}
      }

      let clientSatisfactionObj = await getCollectionDataById(ClientSatisfaction, req.params.id);

      if (clientSatisfactionObj != null) {

        let userCreated = await getUserByUserId(clientSatisfactionObj.userId);
        if (userCreated != null) {

          response.metadata.userCreated = userCreated.firstName + ' ' + userCreated.lastName;
        }

        let siteObj = await getSite(clientSatisfactionObj.siteId);
        if (siteObj != null) {

          response.metadata.siteName = siteObj.siteName;
        }
      }

      await successGetResponse(response, clientSatisfactionObj);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema
    }
  }
}