'use strict';

const RefresherTraining = require('../model/RefresherTraining');
const refresherTrainingUpdateSchema = require('../schemas/refresherTrainingUpdateSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const populateRefresherTraining = require('../util/refresherTrainingFunctions').populateRefresherTraining;
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const constants = require('../../../constants');
const _ = require('lodash');
const update = require('../../commons/util/collectionFunctions').update;
const getById = require('../../commons/util/collectionFunctions').getById;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const successUpdateResponse = require('../../commons/util/commonFunctions').successUpdateResponse;

module.exports = {
  method: 'POST',
  path: '/api/v1/refresherTraining/update',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      var response = {
        'success': true,
        'time': new Date()
      }

      await validateSiteId(req, response);

      if (!response.success) {
        return h.response(response).code(200);
      }

      let refresherTraining = await getById(RefresherTraining, req.payload._id);
      if (refresherTraining == null || _.isEmpty(refresherTraining)) {

        await objectNotFound(response, constants.REFRESHER_TRAINING);
        return h.response(response).code(200);
      }

      refresherTraining.updateUser = userObj._id;
      refresherTraining.updatedDate = new Date();

      await populateRefresherTraining(refresherTraining, req);
      await update(RefresherTraining, refresherTraining);
      await successUpdateResponse(response, constants.REFRESHER_TRAINING);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: refresherTrainingUpdateSchema
    }
  }
}

