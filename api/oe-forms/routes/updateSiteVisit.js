'use strict';

const SiteVisit = require('../model/SiteVisit');
const siteVisitUpdateSchema = require('../schemas/siteVisitUpdateSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const populateSiteVisit = require('../../sites/util/siteFunctions').populateSiteVisit;
const constants = require('../../../constants');
const _ = require('lodash');
const update = require('../../commons/util/collectionFunctions').update;
const getById = require('../../commons/util/collectionFunctions').getById;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const successUpdateResponse = require('../../commons/util/commonFunctions').successUpdateResponse;

module.exports = {
  method: 'POST',
  path: '/api/v1/siteVisit/update',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      var response = {
        'success': true,
        'time': new Date()
      }

      await validateSiteId(req, response);

      if (!response.success) {
        return h.response(response).code(200);
      }

      let siteVisit = await getById(SiteVisit, req.payload._id);
      if (siteVisit == null || _.isEmpty(siteVisit)) {

        await objectNotFound(response, constants.SITE_VISIT);
        return h.response(response).code(200);
      }

      siteVisit.updateUser = userObj._id;
      siteVisit.updatedDate = new Date();

      await populateSiteVisit(siteVisit, req);
      await update(SiteVisit, siteVisit);
      await successUpdateResponse(response, constants.SITE_VISIT);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: siteVisitUpdateSchema
    }
  }
}