'use strict';

const ClientSatisfaction = require('../model/ClientSatisfaction');
const clientSatisfactionSchema = require('../schemas/clientSatisfactionSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const getUserById = require('../../users/util/userFunctions').getUserById;
const saveClientSatisfaction = require('../util/clientSatisfactionFunctions').saveClientSatisfaction;
const successResponse = require('../../commons/util/commonFunctions').successResponse;
const constants = require('../../../constants');
const sendEmail = require('../../commons/util/emailFunctions').sendEmail;

module.exports = {
  method: 'POST',
  path: '/api/v1/clientSatisfaction',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;

      var response = {
        'success': true,
        'time': new Date()
      }

      await validateSiteId(req, response);

      if (!response.success) {
        return h.response(response).code(200);
      }

      let clientSatisfaction = new ClientSatisfaction();
      await populateClientSatisfaction(clientSatisfaction, userObj, req);
      clientSatisfaction = await saveClientSatisfaction(clientSatisfaction);

      //TODO update the domain 
      let message = ' Dear ' + clientSatisfaction.clientName + ',';
      message = message + '\n\n Please fill the Satisfaction Survey, please click on the following URL.';
      message = message + '\n http://128.199.210.214/#/client-satisfaction/' + clientSatisfaction._id;
      message = message + '\n\n Thanks, \n Live Sensor Security';

      await sendEmail(clientSatisfaction.clientEmail, 'dumzuservices@gmail.com', 'Client Satisfaction', message);
      await successResponse(response, constants.CLIENT_SATISFACTION, clientSatisfaction);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: clientSatisfactionSchema
    }
  }
}

function populateClientSatisfaction(clientSatisfaction, userObj, req) {

  clientSatisfaction.userId = userObj._id;
  clientSatisfaction.siteId = req.payload.siteId;
  clientSatisfaction.clientName = req.payload.clientName;
  clientSatisfaction.clientEmail = req.payload.clientEmail;
  clientSatisfaction.clientDesignation = req.payload.clientDesignation;
  clientSatisfaction.dateCreated = new Date();
}