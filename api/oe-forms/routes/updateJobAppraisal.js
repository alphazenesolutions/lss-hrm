'use strict';

const JobAppraisal = require('../model/JobAppraisal');
const jobAppraisalUpdateSchema = require('../schemas/jobAppraisalUpdateSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const invalidEmployee = require('../util/jobAppraisalFunctions').invalidEmployee;
const populateJobAppraisal = require('../util/jobAppraisalFunctions').populateJobAppraisal;
const getUserByUserId = require('../../users/util/userFunctions').getUserByUserId;
const constants = require('../../../constants');
const _ = require('lodash');
const update = require('../../commons/util/collectionFunctions').update;
const getById = require('../../commons/util/collectionFunctions').getById;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const successUpdateResponse = require('../../commons/util/commonFunctions').successUpdateResponse;

module.exports = {
  method: 'POST',
  path: '/api/v1/jobAppraisal/update',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      var response = {
        'success': true,
        'time': new Date()
      }

      let empObj = getUserByUserId(req.payload.employeeId);
      if (empObj == null) {

        await invalidEmployee(response);
        return h.response(response).code(200);
      }

      let jobAppraisal = await getById(JobAppraisal, req.payload._id);
      if (jobAppraisal == null || _.isEmpty(jobAppraisal)) {

        await objectNotFound(response, constants.JOB_APPRAISAL);
        return h.response(response).code(200);
      }

      jobAppraisal.updateUser = userObj._id;
      jobAppraisal.updatedDate = new Date();

      await populateJobAppraisal(jobAppraisal, req);
      await update(JobAppraisal, jobAppraisal);
      await successUpdateResponse(response, constants.JOB_APPRAISAL);
      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: jobAppraisalUpdateSchema
    }
  }
}

