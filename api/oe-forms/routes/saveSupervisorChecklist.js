'use strict';

const SupervisorChecklist = require('../model/SupervisorChecklists');
const supervisorChecklistSchema = require('../schemas/supervisorChecklistSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const saveSupervisorChecklist = require('../util/supervisorChecklistFunctions').saveSupervisorChecklist;
const uploadBase64Images = require('../../commons/util/base64ImageFunctions').uploadBase64Images;
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const saveOccurrenceBookWithDetails = require('../../oe-forms/util/occurrenceBookFunctions').saveOccurrenceBookWithDetails;
const constants = require('../../../constants');
const successResponse = require('../../commons/util/commonFunctions').successResponse;
const populateSupervisorChecklist = require('../util/supervisorChecklistFunctions').populateSupervisorChecklist;

module.exports = {
  method: 'POST',
  path: '/api/v1/supervisorChecklist',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      var response = {
        'success': true,
        'time': new Date()
      }

      await validateSiteId(req, response);

      if (!response.success) {
        return h.response(response).code(200);
      }

      let supervisorChecklist = new SupervisorChecklist();
      supervisorChecklist.userId = userObj._id;
      supervisorChecklist.dateCreated = new Date();
      await populateSupervisorChecklist(supervisorChecklist, req);

      supervisorChecklist = await saveSupervisorChecklist(supervisorChecklist);

      const images = req.payload['images'];
      await uploadBase64Images(images, supervisorChecklist, SupervisorChecklist);

      let occurenceDesc = constants.SHIFT_SUPERVISOR + ' has reported by ' + userObj.firstName + ' ' + userObj.lastName;
      await saveOccurrenceBookWithDetails(req, constants.SHIFT_SUPERVISOR, occurenceDesc);

      await successResponse(response, constants.SHIFT_SUPERVISOR, supervisorChecklist);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: supervisorChecklistSchema
    }
  }
}