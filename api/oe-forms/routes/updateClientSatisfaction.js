'use strict';

const ClientSatisfaction = require('../model/ClientSatisfaction');
const updateClientSatisfactionSchema = require('../schemas/updateClientSatisfactionSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const constants = require('../../../constants');
const _ = require('lodash');
const getById = require('../../commons/util/collectionFunctions').getById;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const successUpdateResponse = require('../../commons/util/commonFunctions').successUpdateResponse;
const uploadFile = require('../../commons/util/fileFunctions').uploadFile;
const update = require('../../commons/util/collectionFunctions').update;

module.exports = {
  method: 'POST',
  path: '/api/v1/clientSatisfaction/update',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    payload: {
      maxBytes: 15 * 1024 * 1024, // 15 mb
      output: 'stream',
      allow: 'multipart/form-data', // important
      parse: true
    },
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;

      var response = {
        'success': true,
        'time': new Date()
      }

      let clientSatisfaction = await getById(ClientSatisfaction, req.payload._id);
      if (clientSatisfaction == null || _.isEmpty(clientSatisfaction)) {

        await objectNotFound(response, constants.CLIENT_SATISFACTION);
        return h.response(response).code(200);
      }

      clientSatisfaction.updateUser = userObj._id;
      clientSatisfaction.updatedDate = new Date();

      const document = req.payload['document']; // accept a field call document
      let fileId = await uploadFile(document, clientSatisfaction, constants.FOLDER_CLIENT_SATISFACTION);
            
      clientSatisfaction.fileId = fileId;
      await update(ClientSatisfaction, clientSatisfaction);

      await successUpdateResponse(response, constants.CLIENT_SATISFACTION);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: updateClientSatisfactionSchema
    }
  }
}