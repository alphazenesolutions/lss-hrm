'use strict';

const JobAppraisal = require('../model/JobAppraisal');
const jobAppraisalSchema = require('../schemas/jobAppraisalSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const saveJobAppraisal = require('../util/jobAppraisalFunctions').saveJobAppraisal;
const getUserByUserId = require('../../users/util/userFunctions').getUserByUserId;
const sendNotification = require('../../notification/util/notificationFunctions').sendNotification;
const successResponse = require('../../commons/util/commonFunctions').successResponse;
const constants = require('../../../constants');
const invalidEmployee = require('../util/jobAppraisalFunctions').invalidEmployee;
const populateJobAppraisal = require('../util/jobAppraisalFunctions').populateJobAppraisal;

module.exports = {
  method: 'POST',
  path: '/api/v1/jobAppraisal',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      var response = {
        'success': true,
        'time': new Date()
      }

      let empObj = getUserByUserId(req.payload.employeeId);
      if (empObj == null) {

        await invalidEmployee(response);
        return h.response(response).code(200);
      }

      let jobAppraisal = new JobAppraisal();
      jobAppraisal.userId = userObj._id;
      jobAppraisal.dateCreated = new Date();

      await populateJobAppraisal(jobAppraisal, req);
      jobAppraisal = await saveJobAppraisal(jobAppraisal);

      let notificationData = {
        "title": constants.JOB_APPRAISAL,
        "id": jobAppraisal._id,
        "notifyUserId": req.payload.employeeId
      };
      await sendNotification(userObj, jobAppraisal, notificationData);
      await successResponse(response, constants.JOB_APPRAISAL, jobAppraisal);
      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: jobAppraisalSchema
    }
  }
}