'use strict';

const RefresherTraining = require('../model/RefresherTraining');
const refresherTrainingSchema = require('../schemas/refresherTrainingSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const saveRefresherTraining = require('../util/refresherTrainingFunctions').saveRefresherTraining;
const sendNotification = require('../../notification/util/notificationFunctions').sendNotification;
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const saveOccurrenceBookWithDetails = require('../../oe-forms/util/occurrenceBookFunctions').saveOccurrenceBookWithDetails;
const uploadBase64Images = require('../../commons/util/base64ImageFunctions').uploadBase64Images;
const constants = require('../../../constants');
const successResponse = require('../../commons/util/commonFunctions').successResponse;
const populateRefresherTraining = require('../util/refresherTrainingFunctions').populateRefresherTraining;

module.exports = {
  method: 'POST',
  path: '/api/v1/refresherTraining',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      var response = {
        'success': true,
        'time': new Date()
      }

      await validateSiteId(req, response);

      if (!response.success) {
        return h.response(response).code(200);
      }

      let refresherTraining = new RefresherTraining();
      refresherTraining.userId = userObj._id;
      refresherTraining.dateCreated = new Date();

      await populateRefresherTraining(refresherTraining, req);
      refresherTraining = await saveRefresherTraining(refresherTraining);

      const images = req.payload['images'];
      await uploadBase64Images(images, refresherTraining, RefresherTraining);

      let notificationData = {
        "id": refresherTraining._id,
        "title": constants.REFRESHER_TRAINING
      };

      for (const userId of refresherTraining.attendees) {
        notificationData.notifyUserId = userId;
        await sendNotification(userObj, refresherTraining, notificationData);
      }

      let occurenceDesc = constants.REFRESHER_TRAINING + ' has reported by ' + userObj.firstName + ' ' + userObj.lastName;
      await saveOccurrenceBookWithDetails(req, constants.REFRESHER_TRAINING, occurenceDesc);

      await successResponse(response, constants.REFRESHER_TRAINING, refresherTraining);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: refresherTrainingSchema
    }
  }
}