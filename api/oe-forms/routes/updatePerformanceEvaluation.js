'use strict';

const PerformanceEvaluation = require('../model/PerformanceEvaluation');
const performanceEvaluationUpdateSchemas = require('../schemas/performanceEvaluationUpdateSchemas');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const constants = require('../../../constants');
const _ = require('lodash');
const update = require('../../commons/util/collectionFunctions').update;
const getById = require('../../commons/util/collectionFunctions').getById;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const successUpdateResponse = require('../../commons/util/commonFunctions').successUpdateResponse;
const populatePerformanceEvaluation = require('../util/performanceEvaluationFunctions').populatePerformanceEvaluation;

module.exports = {
  method: 'POST',
  path: '/api/v1/performanceEvaluation/update',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      var response = {
        'success': true,
        'time': new Date()
      }

      let performanceEvaluation = await getById(PerformanceEvaluation, req.payload._id);
      
      if (performanceEvaluation == null || _.isEmpty(performanceEvaluation)) {

        await objectNotFound(response, constants.PERFORMANCE_EVAL);
        return h.response(response).code(200);
      }

      performanceEvaluation.updateUser = userObj._id;
      performanceEvaluation.updatedDate = new Date();

      await populatePerformanceEvaluation(performanceEvaluation, req);
      await update(PerformanceEvaluation, performanceEvaluation);

      await successUpdateResponse(response, constants.PERFORMANCE_EVAL);
      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: performanceEvaluationUpdateSchemas
    }
  }
}