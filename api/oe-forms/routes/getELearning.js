'use strict';

const ELearning = require('../model/ELearning');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getById = require('../../commons/util/collectionFunctions').getById;
const getUserById = require('../../users/util/userFunctions').getUserById;
const successGetResponse = require('../../commons/util/commonFunctions').successGetResponse;
const populateNotification = require('../../notification/util/notificationFunctions').populateNotification;
const saveNotification = require('../../notification/util/notificationFunctions').saveNotification;
const getHRUsers = require('../../users/util/userFunctions').getHRUsers;
const constants = require('../../../constants');
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const _ = require('lodash');

module.exports = {
  method: 'GET',
  path: '/api/v1/eLearning/{id}',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;

      var response = {
        'time': new Date(),
        'data': {}
      }

      let eLearningObj = await getById(ELearning, req.params.id);
      if (eLearningObj == null || _.isEmpty(eLearningObj)) {

        await objectNotFound(response, constants.E_LEARNING);
        return h.response(response).code(200);
      }

      let hrUsers = await getHRUsers();

      if (hrUsers != null && eLearningObj != null && userObj.role != 'HR') {
    
        for (const hrUser of hrUsers) {
          let notificationData = {
            "title": constants.E_LEARNING_VIEW,
            "id": eLearningObj._id,
            "notifyUserId": hrUser._id
          };
  
          let notification = await populateNotification(userObj, eLearningObj, false, notificationData);
          notification.description = userObj.firstName + ' ' + userObj.lastName + ' has opened the ' + notificationData.title;
  
          await saveNotification(notification);
        }
      }

      await successGetResponse(response, eLearningObj);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema
    }
  }
}