'use strict';

const JobTraining = require('../model/JobTraining');
const jobTrainingUpdateSchema = require('../schemas/jobTrainingUpdateSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const populateJobTraining = require('../util/jobTrainingFunctions').populateJobTraining;
const invalidMentor = require('../util/jobTrainingFunctions').invalidMentor;
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const getUserByUserId = require('../../users/util/userFunctions').getUserByUserId;
const constants = require('../../../constants');
const _ = require('lodash');
const update = require('../../commons/util/collectionFunctions').update;
const getById = require('../../commons/util/collectionFunctions').getById;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const successUpdateResponse = require('../../commons/util/commonFunctions').successUpdateResponse;

module.exports = {
  method: 'POST',
  path: '/api/v1/jobTraining/update',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      var response = {
        'success': true,
        'time': new Date()
      }

      await validateSiteId(req, response);

      if (!response.success) {
        return h.response(response).code(200);
      }

      let mentorObj = getUserByUserId(req.payload.mentorId);
      if (mentorObj == null) {

        await invalidMentor(response);
        return h.response(response).code(200);
      }

      let jobTraining = await getById(JobTraining, req.payload._id);
      if (jobTraining == null || _.isEmpty(jobTraining)) {

        await objectNotFound(response, constants.ON_THE_JOB_TRAINING);
        return h.response(response).code(200);
      }

      jobTraining.updateUser = userObj._id;
      jobTraining.updatedDate = new Date();

      await populateJobTraining(jobTraining, req);
      await update(JobTraining, jobTraining);
      await successUpdateResponse(response, constants.ON_THE_JOB_TRAINING);
      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: jobTrainingUpdateSchema
    }
  }
}