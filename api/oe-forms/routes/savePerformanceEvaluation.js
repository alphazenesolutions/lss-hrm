'use strict';

const PerformanceEvaluation = require('../model/PerformanceEvaluation');
const performanceEvaluationSchema = require('../schemas/performanceEvaluationSchemas');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const savePerformanceEvaluation = require('../util/performanceEvaluationFunctions').savePerformanceEvaluation;
const sendNotification = require('../../notification/util/notificationFunctions').sendNotification;
const constants = require('../../../constants');
const successResponse = require('../../commons/util/commonFunctions').successResponse;
const populatePerformanceEvaluation = require('../util/performanceEvaluationFunctions').populatePerformanceEvaluation;

module.exports = {
  method: 'POST',
  path: '/api/v1/performanceEvaluation',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      var response = {
        'success': true,
        'time': new Date()
      }

      let performanceEvaluation = new PerformanceEvaluation();
      performanceEvaluation.userId = userObj._id;
      performanceEvaluation.dateCreated = new Date();

      await populatePerformanceEvaluation(performanceEvaluation, req);
      performanceEvaluation = await savePerformanceEvaluation(performanceEvaluation);

      let notificationData = {
        "id": performanceEvaluation._id,
        "title": constants.PERFORMANCE_EVAL,
        "notifyUserId": req.payload.employeeId
      };
      await sendNotification(userObj, performanceEvaluation, notificationData);

      await successResponse(response, constants.PERFORMANCE_EVAL, performanceEvaluation);
      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: performanceEvaluationSchema
    }
  }
}