'use strict';

const SupervisorChecklist = require('../model/SupervisorChecklists');
const supervisorChecklistUpdateSchema = require('../schemas/supervisorChecklistUpdateSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const populateSupervisorChecklist = require('../util/supervisorChecklistFunctions').populateSupervisorChecklist;
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const constants = require('../../../constants');
const _ = require('lodash');
const update = require('../../commons/util/collectionFunctions').update;
const getById = require('../../commons/util/collectionFunctions').getById;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const successUpdateResponse = require('../../commons/util/commonFunctions').successUpdateResponse;

module.exports = {
  method: 'POST',
  path: '/api/v1/supervisorChecklist/update',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      var response = {
        'success': true,
        'time': new Date()
      }

      await validateSiteId(req, response);

      if (!response.success) {
        return h.response(response).code(200);
      }

      let supervisorChecklist = await getById(SupervisorChecklist, req.payload._id);
      if (supervisorChecklist == null || _.isEmpty(supervisorChecklist)) {

        await objectNotFound(response, constants.JOB_APPRAISAL);
        return h.response(response).code(200);
      }

      supervisorChecklist.updateUser = userObj._id;
      supervisorChecklist.updatedDate = new Date();

      await populateSupervisorChecklist(supervisorChecklist, req);
      await update(SupervisorChecklist, supervisorChecklist);
      await successUpdateResponse(response, constants.SHIFT_SUPERVISOR);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: supervisorChecklistUpdateSchema
    }
  }
}