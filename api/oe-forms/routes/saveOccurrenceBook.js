'use strict';

const OccurrenceBook = require('../model/OccurrenceBook');
const occurrenceBookSchema = require('../schemas/occurrenceBookSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const saveOccurrenceBook = require('../util/occurrenceBookFunctions').saveOccurrenceBook;
const populateOccurrenceBook = require('../util/occurrenceBookFunctions').populateOccurrenceBook;
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const successResponse = require('../../commons/util/commonFunctions').successResponse;
const constants = require('../../../constants');

module.exports = {
  method: 'POST',
  path: '/api/v1/occurrenceBook/add',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      var response = {
        'success': true,
        'time': new Date()
      }

      await validateSiteId(req, response);

      if (!response.success) {
        return h.response(response).code(200);
      }

      let occurrenceBook = new OccurrenceBook();
      occurrenceBook.userId = userObj._id;
      occurrenceBook.dateCreated = new Date();

      await populateOccurrenceBook(occurrenceBook, req);
      occurrenceBook = await saveOccurrenceBook(occurrenceBook);
      await successResponse(response, constants.OCCURRENCE_BOOK, occurrenceBook);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: occurrenceBookSchema
    }
  }
}