'use strict';

const OperationVisit = require('../model/OperationVisit');
const operationVisitSchema = require('../schemas/operationVisitSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const saveOperationVisit = require('../util/siteVisitFunctions').saveOperationVisit;
const getUserByUserId = require('../../users/util/userFunctions').getUserByUserId;
const uploadBase64Images = require('../../commons/util/base64ImageFunctions').uploadBase64Images;
const sendNotification = require('../../notification/util/notificationFunctions').sendNotification;
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const saveOccurrenceBookWithDetails = require('../../oe-forms/util/occurrenceBookFunctions').saveOccurrenceBookWithDetails;
const constants = require('../../../constants');
const successResponse = require('../../commons/util/commonFunctions').successResponse;
const populateOperationVisit = require('../util/operationVisitFunctions').populateOperationVisit;
const invalidConductedUser = require('../util/operationVisitFunctions').invalidConductedUser;

module.exports = {
  method: 'POST',
  path: '/api/v1/operationVisit',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      let response = {
        'success': true,
        'time': new Date()
      }

      await validateSiteId(req, response);

      if (!response.success) {

        return h.response(response).code(200);
      }

      let conductedByUser = await getUserByUserId(req.payload.conductedBy);
      if (conductedByUser == null) {

        await invalidConductedUser(response);
        return h.response(response).code(200);
      }

      let operationVisit = new OperationVisit();
      operationVisit.dateCreated = new Date();
      operationVisit.userId = userObj._id;

      await populateOperationVisit(operationVisit, req);
      operationVisit = await saveOperationVisit(operationVisit);

      const images = req.payload['images'];
      await uploadBase64Images(images, operationVisit, OperationVisit);

      let notificationData = {
        "id": operationVisit._id,
        "title": constants.OPERATION_VISIT_REPORT
      };

      for (const userId of operationVisit.securityOfficers) {
        notificationData.notifyUserId = userId;
        await sendNotification(userObj, operationVisit, notificationData);
      }

      let occurenceDesc = constants.OPERATION_VISIT_REPORT + ' has reported by ' + userObj.firstName + ' ' + userObj.lastName;
      await saveOccurrenceBookWithDetails(req, constants.OPERATION_VISIT_REPORT, occurenceDesc);

      await successResponse(response, constants.OPERATION_VISIT_REPORT, operationVisit);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: operationVisitSchema
    }
  }
}