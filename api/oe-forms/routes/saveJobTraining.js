'use strict';

const JobTraining = require('../model/JobTraining');
const User = require('../../users/model/User');
const jobTrainingSchema = require('../schemas/jobTrainingSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const saveJobTraining = require('../util/jobTrainingFunctions').saveJobTraining;
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const getUserByUserId = require('../../users/util/userFunctions').getUserByUserId;
const sendNotification = require('../../notification/util/notificationFunctions').sendNotification;
const saveOccurrenceBookWithDetails = require('../../oe-forms/util/occurrenceBookFunctions').saveOccurrenceBookWithDetails;
const uploadBase64Images = require('../../commons/util/base64ImageFunctions').uploadBase64Images;
const successResponse = require('../../commons/util/commonFunctions').successResponse;
const constants = require('../../../constants');
const populateJobTraining = require('../util/jobTrainingFunctions').populateJobTraining;
const invalidUser = require('../util/jobTrainingFunctions').invalidUser;
const update = require('../../commons/util/collectionFunctions').update;

module.exports = {
  method: 'POST',
  path: '/api/v1/jobTraining',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      var response = {
        'success': true,
        'time': new Date()
      }

      await validateSiteId(req, response);

      if (!response.success) {
        return h.response(response).code(200);
      }

      let mentorObj = await getUserByUserId(req.payload.mentorId);
      if (mentorObj == null) {

        await invalidUser(response, constants.MENTOR);
        return h.response(response).code(200);
      }

      let traineeObj = await getUserByUserId(req.payload.traineeId);
      if (traineeObj == null) {

        await invalidUser(response, constants.TRAINEE);
        return h.response(response).code(200);
      }

      let jobTraining = new JobTraining();
      jobTraining.userId = userObj._id;
      jobTraining.dateCreated = new Date();

      await populateJobTraining(jobTraining, req);
      jobTraining = await saveJobTraining(jobTraining);

      traineeObj.ojtStatus = constants.COMPLETE;
      
      await update(User, traineeObj);

      const images = req.payload['images'];
      await uploadBase64Images(images, jobTraining, JobTraining);

      let notificationData = {
        "id": jobTraining._id,
        "title": constants.ON_THE_JOB_TRAINING,
        "notifyUserId": jobTraining.traineeId
      };
      await sendNotification(userObj, jobTraining, notificationData);

      let occurenceDesc = constants.ON_THE_JOB_TRAINING + ' has reported by ' + userObj.firstName + ' ' + userObj.lastName;
      await saveOccurrenceBookWithDetails(req, constants.ON_THE_JOB_TRAINING, occurenceDesc);

      await successResponse(response, constants.ON_THE_JOB_TRAINING, jobTraining);
      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: jobTrainingSchema
    }
  }
}