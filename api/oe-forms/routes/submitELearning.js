'use strict';

const ELearning = require('../model/ELearning');
const Notification = require('../../notification/model/Notification');
const ELearningResponse = require('../model/ELearningResponse');
const submitELearningSchema = require('../schemas/submitELearningSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const successUpdateResponse = require('../../commons/util/commonFunctions').successUpdateResponse;
const create = require('../../commons/util/collectionFunctions').create;
const update = require('../../commons/util/collectionFunctions').update;
const getById = require('../../commons/util/collectionFunctions').getById;
const constants = require('../../../constants');
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const populateNotification = require('../../notification/util/notificationFunctions').populateNotification;
const saveNotification = require('../../notification/util/notificationFunctions').saveNotification;
const getHRUsers = require('../../users/util/userFunctions').getHRUsers;
const _ = require('lodash');

module.exports = {
    method: 'POST',
    path: '/api/v1/eLearning/submit',
    config: {
        auth: {
            strategy: 'jwt'
        },
        tags: ['api'],
        pre: [
            { method: getUserById, assign: 'user' }
        ],
        handler: async (req, h) => {

            var userObj = req.pre.user;

            var response = {
                'success': true,
                'time': new Date()
            }

            let eLearning = await getById(ELearning, req.payload._id);
            if (eLearning == null || _.isEmpty(eLearning)) {

                await objectNotFound(response, constants.E_LEARNING);
                return h.response(response).code(200);
            }

            let eLearningResponse = new ELearningResponse();
            eLearningResponse.userId = userObj._id;
            eLearningResponse.eLearningId = req.payload._id;
            eLearningResponse.dateCreated = new Date();
            eLearningResponse.learningData = req.payload.learningData;

            eLearningResponse = await create(ELearningResponse, eLearningResponse);

            let notification = await getById(Notification, req.payload.notificationId);

            if (notification != null && notification.acknowledgement == false) {
                notification.acknowledgement = true;
                notification.dateUpdated = new Date();

                await update(Notification, notification);
            }

            let hrUsers = await getHRUsers();

            if (hrUsers != null && eLearningResponse != null) {

                for (const hrUser of hrUsers) {
                    let notificationData = {
                        "title": constants.E_LEARNING_RESPONSE,
                        "id": eLearningResponse._id,
                        "notifyUserId": hrUser._id
                    };

                    let notification = await populateNotification(userObj, eLearningResponse, false, notificationData);
                    notification.description = userObj.firstName + ' ' + userObj.lastName + ' has submitted the e-learning';
                    notification.metaData = { eLearningId: req.payload._id };
                    
                    await saveNotification(notification);
                }
            }
            await successUpdateResponse(response, constants.E_LEARNING_RESPONSE);
            return h.response(response).code(200);
        },
        // Validate the payload against the Joi schema
        validate: {
            headers: checkCommonHeadersSchema,
            payload: submitELearningSchema
        }
    }
}