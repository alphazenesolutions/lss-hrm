'use strict';

const SiteVisit = require('../model/SiteVisit');
const siteVisitSchema = require('../schemas/siteVisitSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const saveSiteVisit = require('../util/siteVisitFunctions').saveSiteVisit;
const populateSiteVisit = require('../../sites/util/siteFunctions').populateSiteVisit;
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const saveOccurrenceBookWithDetails = require('../../oe-forms/util/occurrenceBookFunctions').saveOccurrenceBookWithDetails;
const uploadBase64Images = require('../../commons/util/base64ImageFunctions').uploadBase64Images;
const constants = require('../../../constants');
const successResponse = require('../../commons/util/commonFunctions').successResponse;

module.exports = {
  method: 'POST',
  path: '/api/v1/siteVisit',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      var response = {
        'success': true,
        'time': new Date()
      }

      await validateSiteId(req, response);

      if (!response.success) {
        return h.response(response).code(200);
      }

      let siteVisit = new SiteVisit();
      siteVisit.userId = userObj._id;
      siteVisit.dateCreated = new Date();

      await populateSiteVisit(siteVisit, req);
      siteVisit = await saveSiteVisit(siteVisit);
      
      const images = req.payload['images'];
      await uploadBase64Images(images, siteVisit, SiteVisit);

      let occurenceDesc = constants.SITE_VISIT + ' has reported by ' + userObj.firstName + ' ' + userObj.lastName;
      await saveOccurrenceBookWithDetails(req, constants.SITE_VISIT, occurenceDesc);

      await successResponse(response, constants.SITE_VISIT, siteVisit);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: siteVisitSchema
    }
  }
}
