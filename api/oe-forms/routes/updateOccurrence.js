'use strict';

const OccurrenceBook = require('../model/OccurrenceBook');
const updateOccurrenceBookSchema = require('../schemas/updateOccurrenceBookSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const populateOccurrenceBook = require('../util/occurrenceBookFunctions').populateOccurrenceBook;
const constants = require('../../../constants');
const _ = require('lodash');
const update = require('../../commons/util/collectionFunctions').update;
const getById = require('../../commons/util/collectionFunctions').getById;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const successUpdateResponse = require('../../commons/util/commonFunctions').successUpdateResponse;

module.exports = {
  method: 'POST',
  path: '/api/v1/occurrenceBook/update',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var response = {
        'success': true,
        'time': new Date()
      }

      await validateSiteId(req, response);

      if (!response.success) {
        return h.response(response).code(200);
      }

      let occurrenceBook = await getById(OccurrenceBook, req.payload._id);
      if (occurrenceBook == null || _.isEmpty(occurrenceBook)) {

        await objectNotFound(response, constants.OCCURRENCE_BOOK);
        return h.response(response).code(200);
      }

      await populateOccurrenceBook(occurrenceBook, req);
      await update(OccurrenceBook, occurrenceBook);
      await successUpdateResponse(response, constants.OCCURRENCE_BOOK);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: updateOccurrenceBookSchema
    }
  }
}