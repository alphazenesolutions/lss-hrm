'use strict';

const occurrenceBookHistorySchema = require('../schemas/occurrenceBookHistorySchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const getOccurrenceBookHistory = require('../util/occurrenceBookFunctions').getOccurrenceBookHistory;
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const successGetResponse = require('../../commons/util/commonFunctions').successGetResponse;
const getStartTime = require('../../commons/util/dateFunctions').getStartTime;
const getEndTime = require('../../commons/util/dateFunctions').getEndTime;

module.exports = {
  method: 'POST',
  path: '/api/v1/occurrenceBook/list',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var response = {
        'time': new Date(),
        'success': true,
        'data': {}
      }

      await validateSiteId(req, response);

      if (!response.success) {
        return h.response(response).code(200);
      }

      let timeOffset = null;
      if (req.headers.timezone != null) {
    
        timeOffset = parseInt(req.headers.timezone);
      }
    
      let startDate = await getStartTime(req.payload.fromDate, timeOffset);
      let endDate = await getEndTime(req.payload.toDate, timeOffset);
    
      let occurrenceBooks = await getOccurrenceBookHistory(startDate, endDate, req.payload.siteId);
      await successGetResponse(response, occurrenceBooks);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: occurrenceBookHistorySchema
    }
  }
}