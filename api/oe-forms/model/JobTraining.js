'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const jobTrainingModel = new Schema({
  userId: { type: String, required: true },
  updateUser: { type: String, required: false },
  mentorId: { type: String, required: true },
  siteId: { type: String, required: true },
  location: { type: String, required: true },
  traineeId: { type: String, required: true },
  trainingDate: { type: Date, required: true },
  overallRating: { type: String, required: true },
  trainingPeriod: { type: String, required: false },
  trainingProgress: { type: Object, required: true },
  areasOfConcern: { type: Object, required: true },
  images: { type: Array, required: false }, 
  dateCreated: { type: Date, required: true },
  dateUpdated: { type: Date, required: false }
});

module.exports = mongoose.model('JobTraining', jobTrainingModel);