'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const operationVisitModel = new Schema({
  userId: { type: String, required: true },
  updateUser: { type: String, required: false },
  siteId: { type: String, required: true },
  visitDate: { type: Date, required: true },
  conductedBy: { type: String, required: true },
  securityOfficers: { type: Array, required: true },
  officerRatings: { type: Object, required: false },
  officerDuties: { type: Object, required: false },
  overallPerformance: { type: String, required: false },
  feedbackFromSiteOfficer: { type: String, required: false },
  commentsRecommendations: { type: String, required: false },
  clientName: { type: String, required: false },
  clientDesignation: { type: String, required: false },
  images: { type: Array, required: false },
  dateCreated: { type: Date, required: true },
  dateUpdated: { type: Date, required: false }
});

module.exports = mongoose.model('OperationVisit', operationVisitModel);