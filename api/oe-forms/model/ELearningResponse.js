'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const eLearningResponseModel = new Schema({
    userId: { type: String, required: true },
    eLearningId: { type: String, required: true },
    learningData: { type: Object, required: true },
    results: { type: String, required: false },
    isFailed: { type: Boolean, required: false },
    dateCreated: { type: Date, required: true },
    dateUpdated: { type: Date, required: false },
    userUpdated: { type: String, required: false }
});

module.exports = mongoose.model('ELearningResponse', eLearningResponseModel);