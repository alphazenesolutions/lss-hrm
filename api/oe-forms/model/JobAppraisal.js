'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const jobAppraisalModel = new Schema({
  userId: { type: String, required: true },
  updateUser: { type: String, required: false },
  employeeId: { type: String, required: true },
  appraisalPeriod: { type: String, required: true },
  appraisalDate: { type: Date, required: true },
  performanceReview: { type: Object, required: true },
  overallRating: { type: String, required: true },
  empStrengths: { type: String, required: false },
  empPerformanceAreas: { type: String, required: false },
  planOfAction: { type: String, required: false },
  dateCreated: { type: Date, required: true },
  dateUpdated: { type: Date, required: false }
});

module.exports = mongoose.model('JobAppraisal', jobAppraisalModel);