'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const refresherTrainingModel = new Schema({
  userId: { type: String, required: true },
  updateUser: { type: String, required: false },
  siteId: { type: String, required: true },
  conductedBy: { type: String, required: true },
  trainingTopic: { type: String, required: true },
  trainingDuration: { type: String, required: false },
  trainingDate: { type: Date, required: true },
  dateCreated: { type: Date, required: true },
  dateUpdated: { type: Date, required: false },
  attendees: { type: Array, required: true },
  images: { type: Array, required: false }
});

module.exports = mongoose.model('RefresherTraining', refresherTrainingModel);