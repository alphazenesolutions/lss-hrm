'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const clientSatisfactionModel = new Schema({
  userId: { type: String, required: true },
  siteId: { type: String, required: true },
  clientName: { type: String, required: true },
  clientDesignation: { type: String, required: false },
  clientEmail: { type: String, required: true },
  itemData: { type: Array, required: false },
  contact: { type: String, required: false },
  comments: { type: Array, required: false },
  fileId: { type: String, required: false },
  dateCreated: { type: Date, required: true },
  dateUpdated: { type: Date, required: false }
});

module.exports = mongoose.model('ClientSatifaction', clientSatisfactionModel);