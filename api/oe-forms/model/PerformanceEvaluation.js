'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const performanceEvaluationModel = new Schema({
  userId: { type: String, required: true },
  updateUser: { type: String, required: false },
  evaluatorId: { type: String, required: true },
  employeeId: { type: String, required: true },
  reviewPeriod: { type: String, required: true },
  dateCreated: { type: Date, required: true },
  dateUpdated: { type: Date, required: false },
  objectives: { type: String, required: false },
  accomplishments: { type: String, required: false },
  performanceCriteria: { type: Object, required: false },
  performanceSummary: { type: String, required: false },
  developmentPlan: { type: String, required: false },
  nextYearTarget: { type: String, required: false },
  overallPerformance: { type: String, required: true }
});

module.exports = mongoose.model('PerformanceEvaluation', performanceEvaluationModel);