'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const siteVisitModel = new Schema({
  userId: { type: String, required: true },
  userUpdated: { type: String, required: false },
  siteId: { type: String, required: true },
  visitedUserId: { type: String, required: true },
  timeIn: { type: Date, required: true },
  timeOut: { type: Date, required: true },
  dateCreated: { type: Date, required: true },
  dateUpdated: { type: Date, required: false },
  images: { type: Array, required: false }
});

module.exports = mongoose.model('SiteVisit', siteVisitModel);