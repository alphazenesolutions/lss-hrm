'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const occurrenceBookModel = new Schema({
  userId: { type: String, required: true },
  siteId: { type: String, required: true },
  dateTime: { type: Date, required: true },
  subject: { type: String, required: true },
  occurrence: { type: String, required: true },
  dateCreated: { type: Date, required: true }
});

module.exports = mongoose.model('OccurrenceBook', occurrenceBookModel);