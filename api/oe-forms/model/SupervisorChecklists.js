'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const supervisorChecklistModel = new Schema({
  userId: { type: String, required: true },
  updateUser: { type: String, required: false },
  securitySupervisorId: { type: String, required: true },
  siteId: { type: String, required: true },
  dateTime: { type: Date, required: true },
  address: { type: String, required: false },
  turnoutBearings: { type: Object, required: true },
  occurrenceBook: { type: Object, required: true },
  equipment: { type: Object, required: true },
  documents: { type: Object, required: true },
  otherRemarks: { type: String, required: false },
  images: { type: Array, required: false },
  dateCreated: { type: Date, required: true },
  dateUpdated: { type: Date, required: false }
});

module.exports = mongoose.model('SupervisorChecklist', supervisorChecklistModel);