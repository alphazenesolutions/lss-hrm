'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const eLearningModel = new Schema({
    userId: { type: String, required: true },
    updateUser: { type: String, required: false },
    title: { type: String, required: true },
    details: { type: String, required: true },
    learningData: { type: Object, required: true },
    fileId: { type: String, required: false },
    dateCreated: { type: Date, required: true },
    dateUpdated: { type: Date, required: false },
    empSelection: { type: String, required: true },
    selectedEmployees: { type: Array, required: false }
});

module.exports = mongoose.model('ELearning', eLearningModel);