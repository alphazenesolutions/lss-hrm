'use strict';

const Joi = require('joi');

const eLearningResultsSchema = Joi.object({
    _id: Joi.string().required().error(new Error(
        "Learning Response Id is required."
    )),
    results: Joi.string().required().error(new Error(
        "Results is required."
    )),
    isFailed: Joi.boolean().allow('', null)
});

module.exports = eLearningResultsSchema;