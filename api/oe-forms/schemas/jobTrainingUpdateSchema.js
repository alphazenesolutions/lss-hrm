'use strict';

const Joi = require('joi');
const jobTrainingUpdateSchema = Joi.object({
  _id: Joi.string().required(),
  siteId: Joi.string().required(),
  mentorId: Joi.string().required(),
  location: Joi.string().required(),
  traineeId: Joi.string().required(),
  trainingPeriod: Joi.string().required(),
  trainingDate: Joi.date().required(),
  trainingProgress: Joi.object().required(),
  overallRating: Joi.string().required(),
  areasOfConcern: Joi.object().allow('', null)
});

module.exports = jobTrainingUpdateSchema;