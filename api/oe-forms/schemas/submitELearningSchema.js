'use strict';

const Joi = require('joi');

const eLearningSchema = Joi.object({
    _id: Joi.string().required().error(new Error(
        "Learning Reference Id is required."
    )),
    learningData: Joi.object().required().error(new Error(
        "Learning Data Json is required."
    )),
    notificationId: Joi.string().required().error(new Error(
        "Notification Id is required."
    ))
});

module.exports = eLearningSchema;