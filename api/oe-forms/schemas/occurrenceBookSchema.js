'use strict';

const Joi = require('joi');
const occurrenceBookSchema = Joi.object({
  dateTime: Joi.date().required(),
  subject: Joi.string().required(),
  occurrence: Joi.string().required(),
  siteId: Joi.string().required()
});

module.exports = occurrenceBookSchema;