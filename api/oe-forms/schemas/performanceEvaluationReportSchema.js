'use strict';

const Joi = require('joi');

const performanceEvaluationReportSchema = Joi.object({
    employeeId: Joi.string().allow('', null),
    fromDate: Joi.date().allow('', null),
    toDate: Joi.date().allow('', null),
});

module.exports = performanceEvaluationReportSchema;