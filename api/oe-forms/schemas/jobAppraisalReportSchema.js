'use strict';

const Joi = require('joi');

const jobAppraisalReportSchema = Joi.object({
    fromDate: Joi.date().allow('', null),
    toDate: Joi.date().allow('', null),
    employeeId: Joi.string().allow('', null)
});

module.exports = jobAppraisalReportSchema;