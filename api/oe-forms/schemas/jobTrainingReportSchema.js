'use strict';

const Joi = require('joi');

const jobTrainingReportSchema = Joi.object({
    siteId: Joi.string().required(),
    fromDate: Joi.date().allow('', null),
    toDate: Joi.date().allow('', null),
    mentorId: Joi.string().allow('', null)
});

module.exports = jobTrainingReportSchema;