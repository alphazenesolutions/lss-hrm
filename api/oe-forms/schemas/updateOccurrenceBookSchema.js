'use strict';

const Joi = require('joi');
const occurrenceBookSchema = Joi.object({
    _id: Joi.string().required().error(new Error(
        "Occurrence Id is required."
    )),
    siteId: Joi.string().required(),
    subject: Joi.string().required(),
    dateTime: Joi.date().required(),
    occurrence: Joi.string().required(),
});

module.exports = occurrenceBookSchema;