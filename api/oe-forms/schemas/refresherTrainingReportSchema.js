'use strict';

const Joi = require('joi');

const refresherTrainingReportSchema = Joi.object({
  siteId: Joi.string().required(),
  fromDate: Joi.date().allow('', null),
  toDate: Joi.date().allow('', null),
  conductedBy: Joi.string().allow('', null)
});

module.exports = refresherTrainingReportSchema;