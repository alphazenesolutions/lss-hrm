'use strict';

const Joi = require('joi');

const updateClientSatisfactionSchema = Joi.object({
  _id: Joi.string().required().error(new Error(
    "Id is required."
  )),
  itemData: Joi.array().required().error(new Error(
    "Item Data is required."
  )),
  comments: Joi.array().allow('', null)
});

module.exports = updateClientSatisfactionSchema;