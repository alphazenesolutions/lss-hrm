'use strict';

const Joi = require('joi');
const Readable = require('stream').Readable

const eLearningSchema = Joi.object({
    title: Joi.string().required().error(new Error(
        "Title is required."
    )),
    details: Joi.string().required().error(new Error(
        "Details is required."
    )),
    learningData: Joi.object().required().error(new Error(
        "Learning Data Json is required."
    )),
    empSelection: Joi.string().only(['All Employees', 'Select Employees']),
    notifySwitch: Joi.boolean().required().error(new Error(
        "Notify Switch is required."
    )),
    selectedEmployees: Joi.array().allow('', null),
    document: Joi.object().type(Readable)
});

module.exports = eLearningSchema;