'use strict';

const Joi = require('joi');
const occurrenceBookHistorySchema = Joi.object({
  fromDate: Joi.date().required(),
  toDate: Joi.date().required(),
  siteId: Joi.string().required()
});

module.exports = occurrenceBookHistorySchema;