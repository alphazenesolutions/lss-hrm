'use strict';

const Joi = require('joi');
const jobAppraisalUpdateSchema = Joi.object({
  _id: Joi.string().required(),
  employeeId: Joi.string().required(),
  appraisalPeriod: Joi.string().required(),
  appraisalDate: Joi.date().required(),
  performanceReview: Joi.object().required(),
  overallRating: Joi.string().required(),
  empStrengths: Joi.string().allow('', null),
  empPerformanceAreas: Joi.string().allow('', null),
  planOfAction: Joi.string().allow('', null)
});

module.exports = jobAppraisalUpdateSchema;