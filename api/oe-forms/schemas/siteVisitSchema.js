'use strict';

const Joi = require('joi');
const siteVisitSchema = Joi.object({
  siteId: Joi.string().required(),
  visitedUserId: Joi.string().required(),
  timeIn: Joi.date().required(),
  timeOut: Joi.date().required(),
  images: Joi.array().allow('', null)
});

module.exports = siteVisitSchema;