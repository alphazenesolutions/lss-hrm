'use strict';

const Joi = require('joi');
const clientSatisfactionSchema = Joi.object({

  siteId: Joi.string().required(),
  clientName: Joi.string().required(),
  clientEmail: Joi.string().required(),
  clientDesignation: Joi.string().allow('', null)
});

module.exports = clientSatisfactionSchema;