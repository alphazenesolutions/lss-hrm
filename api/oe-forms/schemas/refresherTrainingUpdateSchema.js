'use strict';

const Joi = require('joi');
const refresherTrainingUpdateSchema = Joi.object({
  _id: Joi.string().required(),
  siteId: Joi.string().required(),
  conductedBy: Joi.string().required(),
  trainingTopic: Joi.string().required(),
  trainingDuration: Joi.string().required(),
  trainingDate: Joi.date().required(),
  attendees: Joi.array().required()
});

module.exports = refresherTrainingUpdateSchema;