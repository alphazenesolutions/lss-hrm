'use strict';

const Joi = require('joi');

const performanceEvaluationSchema = Joi.object({
  evaluatorId: Joi.string().required(),
  employeeId: Joi.string().required(),
  reviewPeriod: Joi.string().required(),
  objectives: Joi.string().required(),
  accomplishments: Joi.string().allow('', null),
  performanceCriteria: Joi.object().allow('', null),
  performanceSummary: Joi.string().allow('', null),
  developmentPlan: Joi.string().allow('', null),
  nextYearTarget: Joi.string().allow('', null),
  overallPerformance: Joi.string().required()
});

module.exports = performanceEvaluationSchema;