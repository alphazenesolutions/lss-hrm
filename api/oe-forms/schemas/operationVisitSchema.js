'use strict';

const Joi = require('joi');
const operationVisitSchema = Joi.object({
  siteId: Joi.string().required(),
  visitDate: Joi.date().required(),
  conductedBy: Joi.string().required(),
  securityOfficers: Joi.array().required(),
  officerRatings: Joi.object().allow('', null),
  officerDuties: Joi.object().allow('', null),
  overallPerformance: Joi.string().allow('', null),
  feedbackFromSiteOfficer: Joi.string().allow('', null),
  commentsRecommendations: Joi.string().allow('', null),
  clientName: Joi.string().allow('', null),
  clientDesignation: Joi.string().allow('', null),
  images: Joi.array().allow('', null)
});

module.exports = operationVisitSchema;