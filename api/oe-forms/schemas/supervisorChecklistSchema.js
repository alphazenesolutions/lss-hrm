'use strict';

const Joi = require('joi');

const supervisorChecklistSchema = Joi.object({
  securitySupervisorId: Joi.string().required(),
  siteId: Joi.string().required(),
  dateTime: Joi.date().required(),
  address: Joi.string().allow('', null),
  turnoutBearings: Joi.object().required(),
  occurrenceBook: Joi.object().required(),
  equipment: Joi.object().required(),
  documents: Joi.object().required(),
  otherRemarks: Joi.string().required(),
  images: Joi.array().allow('', null)
});

module.exports = supervisorChecklistSchema;