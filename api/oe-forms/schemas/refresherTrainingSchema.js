'use strict';

const Joi = require('joi');
const refresherTrainingSchema = Joi.object({
  siteId: Joi.string().required(),
  conductedBy: Joi.string().required(),
  trainingTopic: Joi.string().required(),
  trainingDuration: Joi.string().required(),
  trainingDate: Joi.date().required(),
  attendees: Joi.array().required(),
  images: Joi.array().allow('', null)
});

module.exports = refresherTrainingSchema;