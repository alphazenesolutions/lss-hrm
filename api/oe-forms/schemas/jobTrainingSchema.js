'use strict';

const Joi = require('joi');
const jobTrainingSchema = Joi.object({
  siteId: Joi.string().required(),
  mentorId: Joi.string().required(),
  location: Joi.string().required(),
  traineeId: Joi.string().required(),
  trainingPeriod: Joi.string().required(),
  trainingDate: Joi.date().required(),
  trainingProgress: Joi.object().required(),
  overallRating: Joi.string().required(),
  areasOfConcern: Joi.object().allow('', null),
  images: Joi.array().allow('', null)
});

module.exports = jobTrainingSchema;