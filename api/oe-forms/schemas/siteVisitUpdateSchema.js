'use strict';

const Joi = require('joi');
const siteVisitUpdateSchema = Joi.object({
  _id: Joi.string().required(),
  siteId: Joi.string().required(),
  visitedUserId: Joi.string().required(),
  timeIn: Joi.date().required(),
  timeOut: Joi.date().required()
});

module.exports = siteVisitUpdateSchema;