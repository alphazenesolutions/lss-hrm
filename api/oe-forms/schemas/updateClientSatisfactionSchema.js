'use strict';

const Joi = require('joi');
const Readable = require('stream').Readable

const updateClientSatisfactionSchema = Joi.object({
  _id: Joi.string().required().error(new Error(
    "Id is required."
  )),
  document: Joi.object().type(Readable).required().error(new Error(
    "Document is required."
  ))
});

module.exports = updateClientSatisfactionSchema;