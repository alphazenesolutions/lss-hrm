'use strict';

const Joi = require('joi');

const supervisorChecklistUpdateSchema = Joi.object({
  _id: Joi.string().required(),
  securitySupervisorId: Joi.string().required(),
  siteId: Joi.string().required(),
  dateTime: Joi.date().required(),
  address: Joi.string().allow('', null),
  turnoutBearings: Joi.object().required(),
  occurrenceBook: Joi.object().required(),
  equipment: Joi.object().required(),
  documents: Joi.object().required(),
  otherRemarks: Joi.string().required()
});

module.exports = supervisorChecklistUpdateSchema;