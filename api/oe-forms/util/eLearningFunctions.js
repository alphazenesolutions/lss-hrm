'use strict';

const getOEandSOs = require('../../users/util/userFunctions').getOEandSOs;
const create = require('../../commons/util/collectionFunctions').create;
const sendNotification = require('../../notification/util/notificationFunctions').sendNotification;
const constants = require('../../../constants');

async function sendELearningNotification(userObj, eLearning, empSelection, selectedEmployees) {
    if (empSelection == 'All Employees') {
        let securityOfficers = await getOEandSOs();
        for (const so of securityOfficers) {

            let notificationData = {
                "title": constants.E_LEARNING,
                "id": eLearning._id,
                "notifyUserId": so._id
            };
            await sendNotification(userObj, eLearning, notificationData);
        }
    }
    else {
        for (const employeeId of selectedEmployees) {

            let notificationData = {
                "title": constants.E_LEARNING,
                "id": eLearning._id,
                "notifyUserId": employeeId
            };
            await sendNotification(userObj, eLearning, notificationData);
        }
    }
}

async function populateELearning(eLearning, req) {
    eLearning.title = req.payload.title;
    eLearning.details = req.payload.details;
    eLearning.learningData = req.payload.learningData;
    if (req.payload.empSelection == null) {

        eLearning.empSelection = false;
    }
    else {
        eLearning.empSelection = req.payload.empSelection;
    }
    eLearning.selectedEmployees = req.payload.selectedEmployees;
}

module.exports = {
    populateELearning: populateELearning,
    sendELearningNotification: sendELearningNotification
}