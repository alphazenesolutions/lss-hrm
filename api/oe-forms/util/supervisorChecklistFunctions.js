'use strict';
const Boom = require('boom');

const SupervisorChecklist = require('../model/SupervisorChecklists');

async function saveSupervisorChecklist(supervisorChecklist) {

  var result;

  await SupervisorChecklist.create(supervisorChecklist).then((checkListObj) => {
    result = checkListObj;
  }).catch((err) => {
    console.log('error occurred while saving supervisor checklist.. ' + err);
    return Boom.badImplementation('Error', err);
  });

  return result;
}

async function populateSupervisorChecklist(supervisorChecklist, req) {
  supervisorChecklist.securitySupervisorId = req.payload.securitySupervisorId;
  supervisorChecklist.siteId = req.payload.siteId;
  supervisorChecklist.dateTime = req.payload.dateTime;
  supervisorChecklist.address = req.payload.address;
  supervisorChecklist.turnoutBearings = req.payload.turnoutBearings;
  supervisorChecklist.occurrenceBook = req.payload.occurrenceBook;
  supervisorChecklist.equipment = req.payload.equipment;
  supervisorChecklist.documents = req.payload.documents;
  supervisorChecklist.otherRemarks = req.payload.otherRemarks;
}

module.exports = {
  saveSupervisorChecklist: saveSupervisorChecklist,
  populateSupervisorChecklist: populateSupervisorChecklist
}