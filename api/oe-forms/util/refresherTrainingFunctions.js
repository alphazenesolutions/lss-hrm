'use strict';
const Boom = require('boom');

const RefresherTraining = require('../model/RefresherTraining');

async function saveRefresherTraining(refresherTraining) {

  var result;

  await RefresherTraining.create(refresherTraining).then((checkListObj) => {
    result = checkListObj;
  }).catch((err) => {
    console.log('error occurred while saving Refresher Training.. ' + err);
    return Boom.badImplementation('Error', err);
  });

  return result;
}

function populateRefresherTraining(refresherTraining, req) {

  refresherTraining.siteId = req.payload.siteId;
  refresherTraining.conductedBy = req.payload.conductedBy;
  refresherTraining.trainingTopic = req.payload.trainingTopic;
  refresherTraining.trainingDuration = req.payload.trainingDuration;
  refresherTraining.trainingDate = req.payload.trainingDate;
  refresherTraining.attendees = req.payload.attendees;
}

module.exports = {
  saveRefresherTraining: saveRefresherTraining,
  populateRefresherTraining: populateRefresherTraining
}