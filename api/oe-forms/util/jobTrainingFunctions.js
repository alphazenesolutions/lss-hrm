'use strict';
const Boom = require('boom');

const JobTraining = require('../model/JobTraining');

async function saveJobTraining(jobTraining) {

  var result;

  await JobTraining.create(jobTraining).then((checkListObj) => {
    result = checkListObj;
  }).catch((err) => {
    console.log('error occurred while saving Job Training.. ' + err);
    return Boom.badImplementation('Error', err);
  });

  return result;
}

function populateJobTraining(jobTraining, req) {

  jobTraining.siteId = req.payload.siteId;
  jobTraining.mentorId = req.payload.mentorId;
  jobTraining.location = req.payload.location;
  jobTraining.traineeId = req.payload.traineeId;
  jobTraining.trainingDate = req.payload.trainingDate;
  jobTraining.trainingPeriod = req.payload.trainingPeriod;
  jobTraining.trainingProgress = req.payload.trainingProgress;
  jobTraining.overallRating = req.payload.overallRating;
  jobTraining.areasOfConcern = req.payload.areasOfConcern;
}

async function invalidUser(response, title) {
  response.success = false;
  response.message = 'Error';
  response.description = 'Invalid ' + title + ', please select a mentor name.';
}

module.exports = {
  saveJobTraining: saveJobTraining,
  populateJobTraining: populateJobTraining,
  invalidUser: invalidUser
}