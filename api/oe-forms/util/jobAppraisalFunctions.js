'use strict';
const Boom = require('boom');

const JobAppraisal = require('../model/JobAppraisal');

async function saveJobAppraisal(jobAppraisal) {

  var result;

  await JobAppraisal.create(jobAppraisal).then((checkListObj) => {
    result = checkListObj;
  }).catch((err) => {
    console.log('error occurred while saving Job Appraisal.. ' + err);
    return Boom.badImplementation('Error', err);
  });

  return result;
}

async function invalidEmployee(response) {
  response.success = false;
  response.message = 'Error';
  response.description = 'Invalid employee, please select an employee.';
}

function populateJobAppraisal(jobAppraisal, req) {
  jobAppraisal.employeeId = req.payload.employeeId;
  jobAppraisal.appraisalPeriod = req.payload.appraisalPeriod;
  jobAppraisal.appraisalDate = req.payload.appraisalDate;
  jobAppraisal.performanceReview = req.payload.performanceReview;
  jobAppraisal.overallRating = req.payload.overallRating;
  jobAppraisal.empStrengths = req.payload.empStrengths;
  jobAppraisal.empPerformanceAreas = req.payload.empPerformanceAreas;
  jobAppraisal.planOfAction = req.payload.planOfAction;
}

module.exports = {
  saveJobAppraisal: saveJobAppraisal,
  invalidEmployee: invalidEmployee,
  populateJobAppraisal: populateJobAppraisal
}