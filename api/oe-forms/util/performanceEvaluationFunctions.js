'use strict';
const Boom = require('boom');

const PerformanceEvaluation = require('../model/PerformanceEvaluation');

async function savePerformanceEvaluation(performanceEvaluation) {

  var result;

  await PerformanceEvaluation.create(performanceEvaluation).then((checkListObj) => {
    result = checkListObj;
  }).catch((err) => {
    console.log('error occurred while saving performance evaluation.. ' + err);
    return Boom.badImplementation('Error', err);
  });

  return result;
}

async function populatePerformanceEvaluation(performanceEvaluation, req) {
  
  performanceEvaluation.evaluatorId = req.payload.evaluatorId;
  performanceEvaluation.employeeId = req.payload.employeeId;
  performanceEvaluation.reviewPeriod = req.payload.reviewPeriod;
  performanceEvaluation.objectives = req.payload.objectives;
  performanceEvaluation.accomplishments = req.payload.accomplishments;
  performanceEvaluation.performanceCriteria = req.payload.performanceCriteria;
  performanceEvaluation.performanceSummary = req.payload.performanceSummary;
  performanceEvaluation.developmentPlan = req.payload.developmentPlan;
  performanceEvaluation.nextYearTarget = req.payload.nextYearTarget;
  performanceEvaluation.overallPerformance = req.payload.overallPerformance;

}

module.exports = {
  savePerformanceEvaluation: savePerformanceEvaluation,
  populatePerformanceEvaluation: populatePerformanceEvaluation
}