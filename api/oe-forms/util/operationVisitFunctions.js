'use strict';
const Boom = require('boom');

const OperationVisit = require('../model/OperationVisit');

async function saveOperationVisit(operationVisit) {

  var result;

  await OperationVisit.create(operationVisit).then((savedObj) => {
    result = savedObj;
  }).catch((err) => {
    console.log('error occurred while saving operation visit.. ' + err);
    return Boom.badImplementation('Error', err);
  });

  return result;
}

function populateOperationVisit(operationVisit, req) {

  operationVisit.siteId = req.payload.siteId;
  operationVisit.conductedBy = req.payload.conductedBy;
  operationVisit.visitDate = req.payload.visitDate;
  operationVisit.securityOfficers = req.payload.securityOfficers;
  operationVisit.officerRatings = req.payload.officerRatings;
  operationVisit.officerDuties = req.payload.officerDuties;
  operationVisit.overallPerformance = req.payload.overallPerformance;
  operationVisit.feedbackFromSiteOfficer = req.payload.feedbackFromSiteOfficer;
  operationVisit.commentsRecommendations = req.payload.commentsRecommendations;
  operationVisit.clientName = req.payload.clientName;
  operationVisit.clientDesignation = req.payload.clientDesignation;
}

async function invalidConductedUser(response) {
  response.success = false;
  response.message = 'Error';
  response.description = 'Invalid conducted by user, please select a conducted by user.';
}

module.exports = {
  saveOperationVisit: saveOperationVisit,
  populateOperationVisit: populateOperationVisit,
  invalidConductedUser: invalidConductedUser
}