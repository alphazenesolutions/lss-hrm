'use strict';
const Boom = require('boom');

const OperationVisit = require('../model/OperationVisit');
const SiteVisit = require('../model/SiteVisit');

async function saveOperationVisit(operationVisit) {

  var result;

  await OperationVisit.create(operationVisit).then((checkListObj) => {
    result = checkListObj;
  }).catch((err) => {
    console.log('error occurred while saving operation visit.. ' + err);
    return Boom.badImplementation('Error', err);
  });

  return result;
}

async function saveSiteVisit(siteVisit) {

  var result;

  await SiteVisit.create(siteVisit).then((resultObj) => {
    result = resultObj;
  }).catch((err) => {
    console.log('error occurred while saving site visit.. ' + err);
    return Boom.badImplementation('Error', err);
  });

  return result;
}

function populateSiteVisit(siteVisit, req) {

  siteVisit.visitedUserId = req.payload.visitedUserId;
  siteVisit.siteId = req.payload.siteId;
  siteVisit.timeIn = req.payload.timeIn;
  siteVisit.timeOut = req.payload.timeOut;
}

module.exports = {
  saveOperationVisit: saveOperationVisit,
  saveSiteVisit: saveSiteVisit,
  populateSiteVisit: populateSiteVisit
}