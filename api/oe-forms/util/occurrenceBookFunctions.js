'use strict';
const Boom = require('boom');
const create = require('../../commons/util/collectionFunctions').create;
const getSiteByName = require('../../sites/util/siteFunctions').getSiteByName;
const getUserFromUserName = require('../../users/util/userFunctions').getUserFromUserName;
const getIsoDate = require('../../commons/util/dateFunctions').getIsoDate;

const OccurrenceBook = require('../model/OccurrenceBook');

async function saveOccurrenceBook(occurrenceBook) {

  var result;

  await OccurrenceBook.create(occurrenceBook).then((checkListObj) => {
    result = checkListObj;
  }).catch((err) => {
    console.log('error occurred while saving Occurrence Book.. ' + err);
    return Boom.badImplementation('Error', err);
  });

  return result;
}

async function saveOccurrenceBookWithDetails(req, subject, occurrence) {

  let occurrenceBook = new OccurrenceBook();
  occurrenceBook.subject = subject;
  occurrenceBook.occurrence = occurrence;
  occurrenceBook.userId = req.pre.user._id;
  occurrenceBook.siteId = req.payload.siteId;
  occurrenceBook.dateTime = new Date();
  occurrenceBook.dateCreated = new Date();

  await saveOccurrenceBook(occurrenceBook);
}

async function getOccurrenceBookHistory(fromDate, toDate, siteId) {

  var occurrenceBooks = await OccurrenceBook
    .find({ "siteId": siteId, "dateTime": { "$gte": fromDate, "$lt": toDate } });
  return occurrenceBooks;
}

async function importOccurrenceBook(row) {

  if (row[0] != null && row[1] != null) {

    let siteObj = await getSiteByName(row[0].trim());
    let userObj = await getUserFromUserName(row[1].trim());
    let occurrenceBook = await populateImportOccurrenceBook(siteObj, row, userObj);

    await create(OccurrenceBook, occurrenceBook);
  }
}

async function populateImportOccurrenceBook(siteObj, row, userObj) {
  let occurrenceBook = new OccurrenceBook();
  let dateTime = new Date(row[2]);

  occurrenceBook.userId = userObj._id;
  occurrenceBook.siteId = siteObj._id;
  occurrenceBook.dateTime = dateTime;
  occurrenceBook.subject = row[3];
  occurrenceBook.occurrence = row[4];
  occurrenceBook.dateCreated = dateTime;

  return occurrenceBook;
}

function populateOccurrenceBook(occurrenceBook, req) {

  occurrenceBook.siteId = req.payload.siteId;
  occurrenceBook.dateTime = req.payload.dateTime;
  occurrenceBook.subject = req.payload.subject;
  occurrenceBook.occurrence = req.payload.occurrence;

}

module.exports = {
  saveOccurrenceBook: saveOccurrenceBook,
  saveOccurrenceBookWithDetails: saveOccurrenceBookWithDetails,
  getOccurrenceBookHistory: getOccurrenceBookHistory,
  importOccurrenceBook: importOccurrenceBook,
  populateOccurrenceBook: populateOccurrenceBook
}