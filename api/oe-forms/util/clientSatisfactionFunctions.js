'use strict';
const Boom = require('boom');

const ClientSatisfaction = require('../model/ClientSatisfaction');

async function saveClientSatisfaction(clientSatisfaction) {

  var result;

  await ClientSatisfaction.create(clientSatisfaction).then((checkListObj) => {
    result = checkListObj;
  }).catch((err) => {
    console.log('error occurred while saving Client Satisfaction.. ' + err);
    return Boom.badImplementation('Error', err);
  });

  return result;
}

module.exports = {
  saveClientSatisfaction: saveClientSatisfaction
}