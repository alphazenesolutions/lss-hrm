'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const trainingModel = new Schema({
  userId: { type: String, required: true },
  title: { type: String, required: true },
  details: { type: String, required: false },
  location: { type: String, required: true },
  fromDate: { type: Date, required: true },
  toDate: { type: Date, required: true },
  empSelection: { type: String, required: true },
  selectedEmployees: { type: Array, required: false },
  trainedUsers: { type: Array, required: false },
  dateCreated: { type: Date, required: true },
  dateUpdated: { type: Date, required: false }
});

module.exports = mongoose.model('Training', trainingModel);