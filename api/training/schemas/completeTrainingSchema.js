'use strict';

const Joi = require('joi');

const completeTrainingSchema = Joi.object({
    _id: Joi.string().required().error(new Error(
        "Training Id is required."
    )),
    dateCompleted: Joi.date().required().error(new Error(
        "Date Completed is required."
    )),
    employees: Joi.array().required().error(new Error(
        "Employees are required."
    ))
});

module.exports = completeTrainingSchema;