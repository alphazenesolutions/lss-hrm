'use strict';

const Joi = require('joi');

const saveTrainingSchema = Joi.object({
    title: Joi.string().required().error(new Error(
        "Title is required."
    )),
    details: Joi.string().allow('', null),
    location: Joi.string().required().error(new Error(
        "Location is required."
    )),
    fromDate: Joi.date().required().error(new Error(
        "From Date is required."
    )),
    toDate: Joi.date().required().error(new Error(
        "To Date is required."
    )),
    empSelection: Joi.string().required().error(new Error(
        "Employee Selection is required."
    )),
    toDate: Joi.date().required().error(new Error(
        "To Date is required."
    )),
    selectedEmployees: Joi.array().allow('', null)
});

module.exports = saveTrainingSchema;