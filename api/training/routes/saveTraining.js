'use strict';

const Training = require('../model/Training');
const saveTrainingSchema = require('../schemas/saveTrainingSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const successResponse = require('../../commons/util/commonFunctions').successResponse;
const create = require('../../commons/util/collectionFunctions').create;
const constants = require('../../../constants');
const populateTraining = require('../util/trainingFunctions').populateTraining;
const sendTrainingNotification = require('../util/trainingFunctions').sendTrainingNotification;

module.exports = {
  method: 'POST',
  path: '/api/v1/training/add',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      var response = {
        'success': true,
        'time': new Date()
      }

      let training = new Training();
      training.userId = userObj._id;
      training.dateCreated = new Date();

      await populateTraining(training, req);
      training = await create(Training, training);
      await sendTrainingNotification(userObj, training, req.payload.empSelection, req.payload.selectedEmployees);

      await successResponse(response, constants.TRAINING, training);
      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: saveTrainingSchema
    }
  }
}
