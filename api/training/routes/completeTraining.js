'use strict';

const Training = require('../model/Training');
const User = require('../../users/model/User');
const completeTrainingSchema = require('../schemas/completeTrainingSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const getUserByUserId = require('../../users/util/userFunctions').getUserByUserId;
const constants = require('../../../constants');
const _ = require('lodash');
const update = require('../../commons/util/collectionFunctions').update;
const getById = require('../../commons/util/collectionFunctions').getById;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const successUpdateResponse = require('../../commons/util/commonFunctions').successUpdateResponse;

module.exports = {
  method: 'POST',
  path: '/api/v1/training/complete',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var response = {
        'success': true,
        'time': new Date()
      }

      let training = await getById(Training, req.payload._id);
      if (training == null || _.isEmpty(training)) {

        await objectNotFound(response, constants.TRAINING);
        return h.response(response).code(200);
      }

      let employee = null;
      let trainingData = null;
      let completedEmployees = [];
      let employeeData = null;

      for (const employeeId of req.payload.employees) {

        employee = await getUserByUserId(employeeId);

        if (employee.trainings == null) {
          employee.trainings = [];
        }
        trainingData = {
          "title": training.title,
          "trainingId": req.payload._id,
          "dateCompleted": req.payload.dateCompleted
        };

        employee.trainings.push(trainingData);
        employeeData = {
          "employeeId": employeeId,
          "dateCompleted": req.payload.dateCompleted
        };
        completedEmployees.push(employeeData)

        await update(User, employee);
      }

      if (training.trainedUsers == null) {

        training.trainedUsers = completedEmployees;
      } else {
        training.trainedUsers = training.trainedUsers.concat(completedEmployees);
      }

      await update(Training, training);

      await successUpdateResponse(response, constants.TRAINING);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: completeTrainingSchema
    }
  }
}