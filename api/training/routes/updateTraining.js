'use strict';

const Training = require('../model/Training');
const updateTrainingSchema = require('../schemas/updateTrainingSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const populateTraining = require('../util/trainingFunctions').populateTraining;
const constants = require('../../../constants');
const _ = require('lodash');
const update = require('../../commons/util/collectionFunctions').update;
const getById = require('../../commons/util/collectionFunctions').getById;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const successUpdateResponse = require('../../commons/util/commonFunctions').successUpdateResponse;

module.exports = {
  method: 'POST',
  path: '/api/v1/training/update',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;
      var response = {
        'success': true,
        'time': new Date()
      }

      let training = await getById(Training, req.payload._id);
      if (training == null || _.isEmpty(training)) {

        await objectNotFound(response, constants.TRAINING);
        return h.response(response).code(200);
      }

      training.updateUser = userObj._id;
      training.updatedDate = new Date();

      await populateTraining(training, req);
      await update(Training, training);

      await successUpdateResponse(response, constants.TRAINING);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: updateTrainingSchema
    }
  }
}