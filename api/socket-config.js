'use strict';

var pub = require('redis-connection')();
var sub = require('redis-connection')('subscriber');
var handleError = require('hapi-error').handleError; // libraries.io/npm/hapi-error

var SocketIO = require('socket.io');
var io;
const getUserByUserId = require('./users/util/userFunctions').getUserByUserId;
const _ = require('lodash');

// please see: .
async function sanitise(text) {
  var sanitised_text = text;

  /* istanbul ignore else */
  if (text.indexOf('<') > -1 /* istanbul ignore next */
    || text.indexOf('>') > -1) {
    sanitised_text = text.replace(/</g, '&lt').replace(/>/g, '&gt');
  }

  return sanitised_text;
}

async function chatHandler(socket) {
  // welcome new clients

  socket.on('adduser', async function (userId) {

    console.log('socket adduser conn.id : ', socket.client.conn.id);
    console.log('socket adduser userId : ', userId);

    pub.hset('people', socket.client.conn.id, userId);
    pub.publish('live:people:new', userId);
    socket.userId = userId;

    let user = await getUserByUserId(userId);
    console.log('socket adduser user obj: ', user);

    socket.friends = user.friends;

    _.each(user.friends, function (userId) {
      socket.join(userId);
    });

    console.log('broadcasting to userconnected, room : ', userId);
    socket.broadcast.to(userId).emit('userconnected', userId + ' has connected to this room');

  });

  socket.on('updateLocation', async function (locationMessage) {
    var locationPayload;

    console.log('socket :: updateLocation, msg:', locationMessage);
    console.log('socket :: userId :' + socket.userId);
    console.log('socket :: friends : ' + JSON.stringify(socket.friends));

    locationPayload = JSON.stringify({ // store each message as a JSON object
      loc: locationMessage,
      time: new Date().toLocaleString(),
      userId: socket.userId
    });
    console.log('socket :: broadcasting to locationupdate, room :' + socket.userId);

    socket.broadcast.to(socket.userId).emit('locationupdate', locationPayload);
  });

  socket.on('clienterror', async function (error) {
    console.log('***********************************************************************');
    console.log('socket :: userId :' + socket.userId);
    console.log('client error : ' + error);
    console.log('***********************************************************************');
  });

  socket.on('clientlog', async function (message) {
    console.log('-----------------------------------------------------------------------');
    console.log('socket :: userId :' + socket.userId);
    console.log('log message : ' + message);
    console.log('-----------------------------------------------------------------------');
  });

  /* istanbul ignore next */
  socket.on('error', async function (error) {
    console.log('error occurred : ' + error);
    await handleError(error, error.stack);
  });
}

/**
 * chat is our Public interface
 * @param {object} listener [required] - the http/hapi server object.
 * @param {function} callback - called once the socket server is running.
 * @returns {function} - returns the callback after 300ms (ample boot time)
 */
async function init(listener) {
  // setup redis pub/sub independently of any socket.io connections
  await pub.on('ready', async function () {
    // console.log("PUB Ready!");
    await sub.on('ready', async function () {
      sub.subscribe('chat:messages:latest', 'chat:people:new');
      // now start the socket.io
      io = SocketIO.listen(listener);
      io.on('connection', chatHandler);
      console.log('redis initilized..');
      
      // Here's where all Redis messages get relayed to Socket.io clients

    });
  });
}

module.exports = {
  init: init,
  pub: pub,
  sub: sub
};
