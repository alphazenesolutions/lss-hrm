'use strict';

const ClockingPoint = require('../model/ClockingPoint');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const getById = require('../../commons/util/collectionFunctions').getById;
const deleteDocument = require('../../commons/util/collectionFunctions').deleteDocument;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const constants = require('../../../constants');
const successDeleteResponse = require('../../commons/util/commonFunctions').successDeleteResponse;
const _ = require('lodash');

module.exports = {
  method: 'DELETE',
  path: '/api/v1/qrCodes/delete/{_id}',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {
      
      var response = {
        'success': true,
        'time': new Date()
      }

      let clockingPoint = await getById(ClockingPoint, req.params._id);

      if (clockingPoint == null || _.isEmpty(clockingPoint)) {

        await objectNotFound(response, constants.QR_CODE);
        return h.response(response).code(200);
      }
      await deleteDocument(ClockingPoint, req.params._id);

      await successDeleteResponse(response, constants.QR_CODE);
      return h.response(response).code(200);

    },
    tags: ['api'],
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema
    }
  }
}