'use strict';

const ClockingReport = require('../model/ClockingReport');
const saveClockingSchema = require('../schemas/saveClockingReportSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const uploadBase64Images = require('../../commons/util/base64ImageFunctions').uploadBase64Images;
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const constants = require('../../../constants');
const successResponse = require('../../commons/util/commonFunctions').successResponse;
const create = require('../../commons/util/collectionFunctions').create;

module.exports = {
  method: 'POST',
  path: '/api/v2/clocking/add',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;

      var response = {
        'success': true,
        'time': new Date()
      }

      await validateSiteId(req, response);

      if (!response.success) {
        return h.response(response).code(200);
      }

      let clockingReport = new ClockingReport();
      await populateClocking(clockingReport, userObj, req);
      clockingReport = await create(ClockingReport, clockingReport);

      const images = req.payload['images'];
      await uploadBase64Images(images, clockingReport, ClockingReport);
      await successResponse(response, constants.CLOCKING, clockingReport);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: saveClockingSchema
    }
  }
}

function populateClocking(clocking, userObj, req) {

  clocking.userId = userObj._id;
  clocking.siteId = req.payload.siteId;
  clocking.planTime = req.payload.planTime;
  clocking.clockingData = req.payload.clockingData;
  clocking.dateCreated = new Date();
}