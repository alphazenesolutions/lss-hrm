'use strict';

const ClockingReport = require('../model/ClockingReport');
const updateClockingSchema = require('../schemas/updateClockingReportSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const constants = require('../../../constants');
const _ = require('lodash');
const update = require('../../commons/util/collectionFunctions').update;
const getById = require('../../commons/util/collectionFunctions').getById;
const objectNotFound = require('../../commons/util/commonFunctions').objectNotFound;
const successUpdateResponse = require('../../commons/util/commonFunctions').successUpdateResponse;

module.exports = {
  method: 'POST',
  path: '/api/v1/clocking/update',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var userObj = req.pre.user;

      var response = {
        'success': true,
        'time': new Date()
      }

      let clockingReport = await getById(ClockingReport, req.payload._id);
      if (clockingReport == null || _.isEmpty(clockingReport)) {

        await objectNotFound(response, constants.CLOCKING);
        return h.response(response).code(200);
      }

      clockingReport.updateUser = userObj._id;
      clockingReport.updatedDate = new Date();
      clockingReport.clockingData = req.payload.clockingData;

      await update(ClockingReport, clockingReport);
      await successUpdateResponse(response, constants.CLOCKING);
      return h.response(response).code(200);

    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: updateClockingSchema
    }
  }
}