'use strict';

const ClockingReport = require('../model/ClockingReport');
const clockingReportSchema = require('../schemas/clockingReportSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const successGetResponse = require('../../commons/util/commonFunctions').successGetResponse;
const getDataByFilter = require('../util/clockingFunctions').getDataByFilter;
const getStartTime = require('../../commons/util/dateFunctions').getStartTime;
const getEndTime = require('../../commons/util/dateFunctions').getEndTime;

module.exports = {
  method: 'POST',
  path: '/api/v1/clocking/report',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var response = {
        'success': true,
        'time': new Date()
      }

      if (req.payload.siteId != null) {

        await validateSiteId(req, response);

        if (!response.success) {
          return h.response(response).code(200);
        }
      }
      let queryFilter = await populateQueryFilter(req);
      let clockingData = await getDataByFilter(ClockingReport, queryFilter);

      await successGetResponse(response, clockingData);

      return h.response(response).code(200);
    },
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: clockingReportSchema
    }
  }
}

async function populateQueryFilter(req) {
  let timeOffset = null;
  if (req.headers.timezone != null) {

    timeOffset = parseInt(req.headers.timezone);
  }

  let startDate = await getStartTime(req.payload.fromDate, timeOffset);
  let endDate = await getEndTime(req.payload.toDate, timeOffset);

  let filter = {

    dateCreated: { "$gte": startDate, "$lt": endDate }
  }

  if (req.payload.siteId != null) {
    filter.siteId = req.payload.siteId;
  }

  return filter;
}