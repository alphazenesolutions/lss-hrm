'use strict';

const getUserById = require('../../users/util/userFunctions').getUserById;
const getClockingPoints = require('../util/clockingFunctions').getClockingPoints;
const successGetResponse = require('../../commons/util/commonFunctions').successGetResponse;
const getSite = require('../../sites/util/siteFunctions').getSite;
const invalidSiteId = require('../../sites/util/siteFunctions').invalidSiteId;
const _ = require('lodash');

module.exports = {
  method: 'GET',
  path: '/api/v1/clockingPoints/{siteId}',
  config: {
    auth: false,
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {

      var response = {
        'success': true,
        'time': new Date()
      }

      let siteObj = await getSite(req.params.siteId);

      if (siteObj == null || _.isEmpty(siteObj)) {

        await invalidSiteId(response);
        return h.response(response).code(200);
      }

      let data = {};
      let result = await getClockingPoints(req.params.siteId);
      data.clockingPoints = result;
      data.planTime = [];

      if (siteObj.clockingDetails != null && siteObj.clockingDetails.planTime != null){

        data.planTime = siteObj.clockingDetails.planTime;
      }

      await successGetResponse(response, data);
      return h.response(response).code(200);
    }
  }
}