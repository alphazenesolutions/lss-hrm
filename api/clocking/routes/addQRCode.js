'use strict';

const Site = require('../../sites/model/Site');
const addQRCodeSchema = require('../schemas/addQRCodeSchema');
const checkCommonHeadersSchema = require('../../users/schemas/checkCommonHeaders');
const getUserById = require('../../users/util/userFunctions').getUserById;
const successUpdateResponse = require('../../commons/util/commonFunctions').successUpdateResponse;
const saveQRCode = require('../util/clockingFunctions').saveQRCode;
const validateSiteId = require('../../sites/util/siteFunctions').validateSiteId;
const constants = require('../../../constants');
const getCollectionDataById = require('../../commons/util/commonFunctions').getCollectionDataById;

module.exports = {
  method: 'POST',
  path: '/api/v1/qrCodes/add',
  config: {
    auth: {
      strategy: 'jwt'
    },
    tags: ['api'],
    pre: [
      { method: getUserById, assign: 'user' }
    ],
    handler: async (req, h) => {
      
      let userObj = req.pre.user;

      var response = {
        'success': true,
        'time': new Date()
      }

      await validateSiteId(req, response);

      if (!response.success) {
        return h.response(response).code(200);
      }

      let siteObj = await getCollectionDataById(Site, req.payload.siteId);

      await saveQRCode(siteObj, req.payload.qrCodes, userObj._id);

      await successUpdateResponse(response, constants.QR_CODE);
      return h.response(response).code(200);

    },
    tags: ['api'],
    // Validate the payload against the Joi schema
    validate: {
      headers: checkCommonHeadersSchema,
      payload: addQRCodeSchema
    }
  }
}