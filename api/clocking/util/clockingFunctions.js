'use strict';

const ClockingPoint = require('../model/ClockingPoint');
const ClockingReport = require('../model/ClockingReport');
const create = require('../../commons/util/collectionFunctions').create;
const getUserFromUserName = require('../../users/util/userFunctions').getUserFromUserName;

async function getClockingPoints(siteId) {

  try {

    let clockingPoints = await ClockingPoint.find({ siteId: siteId })
      .select('qrData');

    return clockingPoints;
  } catch (error) {

    console.log('error occurred during getClockingPoints ' + error);
    return null;
  }
}

async function isValidQRClockingCode(clockingPoints, qrCode) {

  if (clockingPoints != null && clockingPoints.length > 0 && qrCode != null)

    for (const clockingPoint of clockingPoints) {

      if (clockingPoint != null && clockingPoint.qrData != null) {

        if (clockingPoint.qrData.contents == qrCode.contents) {

          return true;
        }
      }
    }

  return false;
}

async function saveQRCode(siteObj, barCodes, userCreated) {

  if (siteObj != null && barCodes != null) {

    let clockingPoints = await getClockingPoints(siteObj._id);

    let qrCodes = [];
    for (const barCode of barCodes) {
      let validQrCode = true;

      if (clockingPoints.length > 0) {

        validQrCode = await isValidQRClockingCode(clockingPoints, barCode);
        validQrCode = !validQrCode;
      }

      if (validQrCode) {

        await qrCodes.push(barCode);
      }
    }

    for (const qrCode of qrCodes) {
      let clockingPoint = new ClockingPoint();
      clockingPoint.siteId = siteObj._id;
      clockingPoint.userId = userCreated;
      clockingPoint.qrData = qrCode;
      clockingPoint.dateCreated = new Date();

      await create(ClockingPoint, clockingPoint);
    }
  }
}

async function getDataByFilter(Collection, query) {

  try {

    let collectionObj = await Collection.find(query);

    return collectionObj;
  } catch (error) {

    console.log('error occurred during getDataByFilter ' + error);
    return null;
  }
}

async function importClocking(metaData, clockingData, siteObj) {

  if (metaData[4] != null) {

    let userId = metaData[4];
    userId = userId.toString().replace(/\"/g, "");

    let userObj = await getUserFromUserName(userId.trim());
    let clockingReport = await populateClockingReport(metaData, clockingData, siteObj, userObj);
    await create(ClockingReport, clockingReport);
  }
}

async function populateClockingReport(metaData, clockingData, siteObj, userObj) {

  let clockingReport = new ClockingReport();

  clockingReport.userId = userObj._id;
  clockingReport.siteId = siteObj._id;
  clockingReport.planTime = metaData[3];
  clockingReport.clockingData = clockingData;
  clockingReport.dateCreated = new Date(metaData[2]);
  return clockingReport;
}

module.exports = {
  saveQRCode: saveQRCode,
  getClockingPoints: getClockingPoints,
  getDataByFilter: getDataByFilter,
  importClocking: importClocking
}