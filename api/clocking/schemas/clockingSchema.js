'use strict';

const Joi = require('joi');

const addClockingSchema = Joi.object({
  siteId: Joi.string().required(),
  clockingType: Joi.string().required(),
  latLong: Joi.object().allow('', null),
  barCode: Joi.object().allow('', null),
  remarks: Joi.string().allow('', null),
  images: Joi.array().allow('', null)
});

module.exports = addClockingSchema;