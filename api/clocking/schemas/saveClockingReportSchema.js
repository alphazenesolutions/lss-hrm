'use strict';

const Joi = require('joi');

const saveClockingSchema = Joi.object({
    siteId: Joi.string().required().error(new Error(
        "Site Id is required."
    )),
    planTime: Joi.string().required().error(new Error(
        "Plan Time is required."
    )),
    clockingData: Joi.array().required().error(new Error(
        "Clocking Data is required."
    )),
    images: Joi.array().allow('', null)
});

module.exports = saveClockingSchema;