'use strict';

const Joi = require('joi');

const clockingReportSchema = Joi.object({
  siteId: Joi.string().allow('', null),
  fromDate: Joi.date().required().error(new Error(
    "From Date is required."
  )),
  toDate: Joi.date().required().error(new Error(
    "To Date is required."
  ))
});

module.exports = clockingReportSchema;