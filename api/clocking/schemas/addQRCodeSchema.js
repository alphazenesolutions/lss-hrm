'use strict';

const Joi = require('joi');

const siteNameSchema = Joi.object({
  siteId: Joi.string().required(),
  qrCodes: Joi.array().required()
});

module.exports = siteNameSchema;