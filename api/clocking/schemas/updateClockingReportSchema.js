'use strict';

const Joi = require('joi');

const saveClockingSchema = Joi.object({
    _id: Joi.string().required().error(new Error(
        "_Id is required."
    )),
    clockingData: Joi.array().required().error(new Error(
        "Clocking Data is required."
    ))
});

module.exports = saveClockingSchema;