'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const clockingPointModel = new Schema({
  userId: { type: String, required: true },
  siteId: { type: String, required: true },
  qrData: { type: Object, required: true },
  dateCreated: { type: Date, required: true }
});

module.exports = mongoose.model('ClockingPoint', clockingPointModel);