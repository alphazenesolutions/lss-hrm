'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const clockingModel = new Schema({
  userId: { type: String, required: true },
  siteId: { type: String, required: true },
  clockingType: { type: String, required: true },
  latLong: { type: Object, required: false },
  barCode: { type: Object, required: false },
  remarks: { type: String, required: false },
  images: { type: Array, required: false },
  dateCreated: { type: Date, required: true }
});

module.exports = mongoose.model('Clocking', clockingModel);