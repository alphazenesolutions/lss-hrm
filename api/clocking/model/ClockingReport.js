'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const clockingReportModel = new Schema({
  userId: { type: String, required: true },
  updateUser: { type: String, required: false },
  siteId: { type: String, required: true },
  //site: { type: Schema.Types.ObjectId, ref: 'Sites' },
  planTime: { type: String, required: true },
  clockingData: { type: Array, required: true },
  images: { type: Array, required: false },
  dateCreated: { type: Date, required: true },
  dateUpdated: { type: Date, required: false }
});

module.exports = mongoose.model('ClockingReport', clockingReportModel);