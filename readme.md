# LSS Service

This repo goes along with Auth0's tutorial on [Lss Service code](git clone https://bitbucket.org/dumzu/live-security-services.git). It shows how to implement user creation, authentication, and access control with Hapi and MongoDB.

## Install and run the MongoDB

```bash/cmd
mongod
```

## Install and run the redis 

```windows
redis-server.exe redis.windows.conf
```

## Installation and Running the App

Clone the repo, then: 

```bash/cmd
npm install
node server.js
```

The app will be served at `localhost:4000`.

## Available Routes

please check the below swagger documentation: 

localhost:4000/documentation