'use strict';

const Hapi = require('hapi');
const HapiCron = require('hapi-cron');
const mongoose = require('mongoose');
const glob = require('glob');
const path = require('path');
const secret = require('./config');
const admin = require("firebase-admin");
const serviceAccount = require("./live-security-firebase.json");

const fs = require('fs');
/*const options = {
  key: fs.readFileSync('/opt/lss/livesensorsecurity.app/private-key.key'),
  cert: fs.readFileSync('/opt/lss/livesensorsecurity.app/f26e73e6d6d23990.crt')
};*/

const server = new Hapi.Server({
  port: 4000,
  //options: options
});

const Vision = require('vision');
const Inert = require('inert');
const HapiSwagger = require('hapi-swagger');
const Pack = require('./package');
const getUserByUserId = require('./api/users/util/userFunctions').getUserByUserId;
const dbUrl = 'mongodb://localhost:27017/live-security';

const swaggerOptions = {
  info: {
    title: 'LiveSecurity Documentation',
    version: Pack.version,
  },
};

// bring your own validation function
const validate = async function (decoded, request) {

  let bearerToken = request.headers.authorization;
  let token = bearerToken.replace('Bearer ', '');

  // do your checks to see if the person is valid
  if (decoded.id) {

    let user = await getUserByUserId(decoded.id);
    if (user == null || user.token != token) {

      return { isValid: false };

    } else {

      request.params.userId = decoded.id;
      return { isValid: true };
    }
  }
  else {
    return { isValid: false };
  }
};

const init = async () => {

  require('console-stamp')(console, { pattern: 'dd/mm/yyyy HH:MM:ss.l' });

  await server.register(require('hapi-auth-jwt2'));
  await server.register(Inert);
  await server.register([
    Inert,
    Vision,
    {
      plugin: HapiSwagger,
      options: swaggerOptions
    }
  ]);

  /*await server.register({
    plugin: require('hapi-require-https'),
    options: {}
  });*/

  server.auth.strategy('jwt', 'jwt',
    {
      key: secret,             // Never Share your secret key
      validate: validate,      // validate function defined above
      verifyOptions: {
        ignoreExpiration: true,
        algorithms: ['HS256']
      } // pick a strong algorithm
    });
  server.auth.default('jwt');

  // Look through the routes in
  // all the subdirectories of API
  // and create a new route for each
  glob.sync('api/**/routes/*.js', {
    root: __dirname
  }).forEach(file => {
    const route = require(path.join(__dirname, file));
    server.route(route);
  });

  server.route({
    method: 'GET',
    path: '/',
    config: {
      auth: false
    },
    handler: {
      file: {
        path: './api/index.html',
        confine: false
      }
    }
  });

  await server.register({
    plugin: require('hapi-cors'),
    options: {
      origins: ['*'],
      allowCredentials: 'true',
      exposeHeaders: ['content-type', 'content-length'],
      maxAge: 600,
      methods: ['GET, HEAD, PUT, PATCH, POST, DELETE, OPTIONS'],
      headers: ['Accept', 'Content-Type', 'Authorization', 'source-system', 'channel', 'timeZone']
    },
    checkOrigin: true
  });

  await server.register({
    plugin: require('hapi-require-https'),
    options: {}
  });

  await server.register({
    plugin: HapiCron,
    options: {
        jobs: [{
            name: 'overtimeNotification',
            time: '*/5 * * * *',
            timezone: 'Asia/Singapore',
            request: {
                method: 'GET',
                url: '/api/v1/overtimeNotification'
            },
            onComplete: (res) => {
                console.log('running overtime checking..');
            }
        }]
    }
  });

  await server.start();
  return server;
};

init().then(hapiServer => {

  // firebase initialize 
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://realtimetracer.firebaseio.com'
  });

  console.log('firebase initialized');

  require('./api/socket-config').init(hapiServer.listener, function () {
    console.log('listening on: http://127.0.0.1:3000');
  });

  mongoose.Promise = require('bluebird');

  // Once started, connect to Mongo through Mongoose
  mongoose.connect(dbUrl, {}, (err) => {
    if (err) {
      throw err;
    } else {
      console.log('mangoose connected ..');
    }
  });

})
  .catch(error => {
    console.log('error occurred while starting the server..' + error);
  });